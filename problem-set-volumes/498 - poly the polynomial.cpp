/*****  poly the polynomial @ 498  *****/

#include<stdio.h>
#include<string.h>
#include<math.h>
#include<stdlib.h>

char f[1010], s[1010], *ptr;
long c[1010], x[1010];
long cf, cs, i, j, k, sum;

int main(){
	while(gets(f)!=NULL){
		gets(s);

		for(cf=0, ptr=strtok(f, " "); ptr!=NULL; ptr=strtok(NULL, " "))
			c[cf++]=atol(ptr);

		for(cs=0, ptr=strtok(s, " "); ptr!=NULL; ptr=strtok(NULL, " "))
			x[cs++]=atol(ptr);

		for(i=0; i<cs; i++){
			sum=0;
			for(j=cf-1, k=0; j>=0, k<cf; j--, k++){
				sum+=c[k]*pow(x[i], j);
			}
			if(i<cs-1)	printf("%ld ", sum);
			else	printf("%ld\n", sum);
		}
	}
	return 0;
}
