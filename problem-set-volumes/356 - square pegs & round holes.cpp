/*****  356 @ Square Pegs And Round Holes  *****/

#include<stdio.h>
#include<math.h>

long n, com, seg, sum, k, bl;
double rad;

int main(){
	bl = 0;
	while(scanf("%ld", &n) == 1){
		if(bl == 1)	printf("\n");
		seg = 8 * n - 4;

		sum = 0;
		rad = n - 0.5;

		for(k = 1; k < n; k++){
			sum += floor(sqrt(pow(rad, 2) - k * k));
		}

		com = sum * 4;

		printf("In the case n = %ld, %ld cells contain segments of the circle.\n", n, seg);
		printf("There are %ld cells completely contained in the circle.\n", com);
		bl = 1;
	}
	return 0;
}
