/*****  Product of Digits @ 993  *****/

#include <stdio.h>

int Find(long n){
	long arr[100], k=-1, i;

	for(i=9; i>=2; i--){
		while(n%i==0){
			arr[++k]=i;
			n=n/i;
		}
		if(n==1)	break;
	}
	if(n>9)	printf("-1\n");
	else{
		for(i=k; i>=0; i--)	printf("%ld", arr[i]);
		printf("\n");
	}
	return 0;
}

int main(){
	long tc, n;
	scanf("%ld", &tc);
	while(tc--){
		scanf("%ld", &n);
		if(n==0)	printf("10\n");
		else if(n==1)	printf("1\n");
		else	Find(n);
	}
	return 0;
}