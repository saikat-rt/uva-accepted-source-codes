/*****  how many knights @ 696  *****/

#include<stdio.h>

long m,n;

int main(){
	while(scanf("%ld %ld",&m,&n)==2){
		if(m==0 && n==0)	break;
		else if(m==1 && n==1)
			printf("%ld knights may be placed on a %ld row %ld column board.\n",1,m,n);
		else if(m==1 || n==1)
			printf("%ld knights may be placed on a %ld row %ld column board.\n",m>n?m:n,m,n);

		/*****  less than 3 row or column is a special case  *****/

		else if(m<3 || n<3){
			long l=m>n?m:n;
			long s=(m*n)/2;
			long t=(l/4)*4;
			if(l%4==1)	t=t+2;
			else if(l%4>1)	t=t+4;
			long temp=s>t?s:t;
			printf("%ld knights may be placed on a %ld row %ld column board.\n",temp,m,n);
		}
		else
			printf("%ld knights may be placed on a %ld row %ld column board.\n",(m*n+1)/2,m,n);
	}
	return 0;
}
