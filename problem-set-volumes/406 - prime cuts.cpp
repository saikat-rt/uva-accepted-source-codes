/*****  406 - Prime Cuts  *****/

#include<stdio.h>
#include<math.h>
#define SZ 1010

char flag [SZ+1];
long prime[SZ],pc=0;
long n, c;
long stock[SZ];

void sieve(void){
	long i,j,r;
	prime[pc++]=1;
	prime[pc++]=2;
	//printf("%ld",2);

	for(i=4;i<=SZ;i+=2)	flag[i]=1;

	for(i=3;i<=SZ;i+=2){
		if(flag[i]==0){
			prime[pc++]=i;
			//printf(",%d",i);
			if(SZ/i>=i){
				r=i*2;
				for(j=i*i;j<=SZ;j+=r)
					flag[j]=1;
			}
		}
	}
}

int main(){
	sieve();
	while(scanf("%ld %ld", &n, &c)==2){
		long j=0, k, l, i, t;
		for(i=0; prime[i]<=n; i++)	stock[j++]=prime[i];
		
		//j=j-1;
		printf("%ld %ld:", n, c);
		if(j%2==0){
			if(j>=2*c){
        	    k=(j-2*c)/2;
            	l=k+2*c;
        	}
        	else{
            	k=0;
            	l=j;
         	}
      	}
      	else{
         	if(j>=(2*c-1)){
            	k=(j-2*c+1)/2;
            	l=k+2*c-1;
         	}
         	else{
            	k=0;
            	l=j;
         	}
      	}
		for(i=k; i<l; i++)	printf(" %ld", stock[i]);
		printf("\n\n");			
	}	
	return 0;
}
