/*****  Heads / Tails Probability @ 474  *****/

#include <stdio.h>
#include <math.h>


int main()
{
	double n, z, y;

	while(scanf("%lf", &n) == 1)
	{
		y = floor(log10(2) * n + 1);
		
		z = pow(2, (y/log10(2) - n));

		printf("2^-%0.0lf = %0.3lfe-%0.0lf\n", n, z, y);
	}
	return 0;
}
