/*****  uniform generator @ 408  *****/

#include<stdio.h>

long a,b;

long gcd(long a,long b)
{
	 if(b==0) 
         return a;
	 else if(a==0)
		 return b;
	 else if(a<b)
		 return gcd(a,b%a); 
     else 
		 return gcd(b,a%b);
}

int main()
{
	while(scanf("%ld %ld",&a,&b)==2)
	{
		if(gcd(a,b)==1)	printf("%10ld%10ld    Good Choice\n",a,b);
		else	printf("%10ld%10ld    Bad Choice\n",a,b);
		printf("\n");
	}
	return 0;
}
