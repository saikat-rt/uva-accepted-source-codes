/*****  Maximum Sum @ 108  *****/


#include <stdio.h>
#define Dif(i,j,k) (Mat[i+k][j] - Mat[i][j])


int N, Mat[104][104];


void Cal() {
   int i, j, k, t, MAX;

   for(i = 1; i<=N; i++){
	   for(j = 0; j<N; j++)
		  Mat[i][j] =  Mat[i][j] + Mat[i-1][j];
   }

   MAX = Mat[1][0];
   for(k = 1; k<=N; k++) {
	   for(i = 0; i<=N-k; i++){
		   for(t = 0, j = 0; j<N; j++) {
		      if(t>=0) t+= Dif(i,j,k);
			  else t = Dif(i,j,k);
			  if(t>MAX) MAX = t;
		   }
	   }
   }		 
   printf("%d\n",MAX);  
}

void Free() {
   int i, j;
   for(i = 0; i<=N; i++)
	   for(j = 0; j<=N; j++)
		   Mat[i][j] = 0;
}

int main(){
	int f=0, i, j;

	while(scanf("%d", &N)==1){
		if(f++)	Free();
		for(i=1; i<=N; i++)
			for(j=0; j<N; j++)
				scanf("%d", &Mat[i][j]);

		Cal();
	}
	return 0;
}
