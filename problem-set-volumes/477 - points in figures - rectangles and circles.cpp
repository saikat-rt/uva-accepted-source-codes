/*****  Points in Figures: Rectangles and Circles @ 477  *****/

#include<stdio.h>
#include<ctype.h>

char s, index[13];
double ulx[13], uly[13], lrx[13], lry[13], x, y;
double cx[13], cy[13], radius[13], temp, rad;
int figure, point=0;
int i, j, flag;

int main(){
	int i=1, j;
	while(scanf("%c", &s)==1  && s!='*'){
		if(s=='r'){
			index[i]='r';
			scanf("%lf%lf%lf%lf",&ulx[i],&uly[i],&lrx[i],&lry[i]);
			i++;
		}
		else if(s=='c'){
			index[i]='c';
			scanf("%lf%lf%lf", &cx[i], &cy[i], &radius[i]);
			i++;
		}
	}

	while(scanf("%lf%lf", &x,&y)==2){
		if(x==9999.9 && y==9999.9)	break;
		else{
			point++;
			flag=0;
			for(j=1; j<i; j++){
				if(index[j]=='r'){
					if((x>ulx[j] && x<lrx[j]) && (y<uly[j] && y>lry[j])){
						flag=1;
						printf("Point %d is contained in figure %d\n", point, j);
					}
				}
				else if(index[j]=='c'){
					temp=(cx[j]-x)*(cx[j]-x)+(cy[j]-y)*(cy[j]-y);
					rad=radius[j]*radius[j];
					if(temp<rad){
						flag=1;
						printf("Point %d is contained in figure %d\n", point, j);
					}
				}
			}
			if(flag==0)
				printf("Point %d is not contained in any figure\n", point);
		}
	}
	return 0;
}




