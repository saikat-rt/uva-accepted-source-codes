/*****  Adding Reversed Numbers @ 713  *****/

#include<stdio.h>
#include<string.h>
#define max 208

void reverse(char *string){
	int i, length;

	for(i = 0, length = strlen(string); i < length / 2; i++){
		string[i] ^= string[length - i - 1] ^= string[i] ^= string[length - i - 1];
	}

	return;
}

void addition(char *num1, char *num2, char *result){
	int i, j, k, sum, carry;

	for(i = strlen(num1) - 1, j = strlen(num2) - 1, k = carry = 0; i >= 0 && j >= 0; i--, j--, k++){
		sum = (num1[i] - '0') + (num2[j] - '0') + carry;
		carry = sum / 10;
		result[k] = sum % 10 + '0';
	}

	//We need to add all the rest characters of 'num1' in 'result' if there's left any digit.
	while(i >= 0){
		sum = (num1[i--] - '0') + carry;
		carry = sum / 10;
		result[k++] = sum % 10 + '0';
	}

	//We need to add all the rest characters of 'num2' in 'result' if there's left any digit.
	while(j >= 0){
		sum = (num2[j--] - '0') + carry;
		carry = sum / 10;
		result[k++] = sum % 10 + '0';
	}

	if (carry != 0) result[k++] = carry % 10 + '0';
	if (k == 0) result[k++] = '0';

	result[k] = NULL;
	//reverse(result);

	return;
}

void eliminate_leading_zeros(char *number){
	int i, j;

   //Discard leading zeros.
   for(i = 0; number[i] == '0'; i++);

   //If 'number' consists only of '0's.
   if (number[i] == NULL) --i;

   //Launch the digits other than leading zeros.
   for(j = 0; number[i] != NULL; i++, j++) number[j] = number[i];

   number[j] = NULL;

	return;
}

int main(){
	long tc;
	char fir[max], sec[max], res[max];
	scanf("%ld", &tc);
	while(tc--){
		scanf("%s %s", fir, sec);
		reverse(fir);
		reverse(sec);
		addition(fir, sec, res);
		//reverse(res);
		eliminate_leading_zeros(res);
		printf("%s\n", res); 
	}
	return 0;
}