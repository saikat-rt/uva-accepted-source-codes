/*****  fibonacci freeze @ 495  *****/

#include<stdio.h>
#include<string.h>

long n,count;
char a[8000],b[8000],final[5010][5010];
char temp[100000];

void reverse(char c[]){
	long i,len,mid;
	char temp1;
	
	len=strlen(c);
	mid=len/2;
	
	for(i=0,len--;i<mid;i++,len--){
		temp1=c[i];
		c[i]=c[len];
		c[len]=temp1;
	}
}

void Add(char a[],char b[],int p){
	long lena,lenb,carry,k,i,j,temp1,temp2;
	lena=strlen(a);
	lenb=strlen(b);
	
	carry=k=0;
	for(i=lena-1,j=lenb-1;i>=0 || j>=0;i--,j--){
		temp1=temp2=0;
		if(i>=0)
			temp1=a[i]-'0';
		if(j>=0)
			temp2=b[j]-'0';
		
		temp[k++]=(temp1+temp2+carry)%10+'0';
		carry=(temp1+temp2+carry)/10;
	}
	
	if(carry>0)
		temp[k++]=carry+'0';
	temp[k]='\0';
	reverse(temp);
	
	if(p==1){
		strcpy(a,temp); 
		strcpy(final[count++],temp);
	}
	else{
		strcpy(final[count++],temp);
		strcpy(b,temp);
	}
}


int main(){
	a[0]='0';
	a[1]='\0';
	b[0]='1';
	b[1]='\0';
	strcpy(final[0],"0");
	strcpy(final[1],"1");
	count=2;
	
	for(long i=0;i<2504;i++){
		Add(a,b,1);
		Add(a,b,2);
	}
	while(scanf("%ld",&n)==1){
		printf("The Fibonacci number for %ld is %s\n",n,final[n]);
	}
	return 0;
}
