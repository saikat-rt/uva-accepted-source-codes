/*
*   ID Codes @ 146
*   [accepted]
*/

#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int main() {
    char arr[55];

    while (gets(arr)) {
        if (!strcmp(arr, "#"))  break;

        if (next_permutation(arr, arr + strlen(arr)))   printf("%s\n", arr);
        else    printf("No Successor\n");
    }

    return 0;
}
