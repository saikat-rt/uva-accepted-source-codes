/*****   What's The Frequency, Kenneth? @ 499  *****/

#include<stdio.h>
#include<string.h>
#include<ctype.h>

long letter[55];
long len, i, j, k, max;
char str[1000];

int main(){
	//freopen("499.txt", "r", stdin);
	while(gets(str)){
		len=strlen(str);
		for(k=0; k<55; k++)	letter[k]=0;
		
		for(i=0; i<len; i++){
			if(str[i]>='A' && str[i]<='Z')	letter[str[i]-65]++;
			else if(str[i]>='a' && str[i]<='z')	letter[str[i]-71]++;
		}
		
		max=0;
		for(j=0; j<53; j++)
			if(letter[j]>max)	max=letter[j];
			
			
		for(i=0; i<26; i++)
			if(letter[i]==max)
				printf("%c", i+65);
		
			
		for(j=26; j<52; j++)
			if(letter[j]==max)
				printf("%c", j+71);
			
		printf(" %ld\n", max);
	}
	return 0;
}
