/* COUNT ON CANTOR______264 */

#include<stdio.h>
#include<math.h>

long term,row,pos,a;
double row_p;

int main(){	
	while(scanf("\n%ld",&term)==1){
		row_p=(-1+sqrt(1+8*term))/2;
		row=ceil(row_p);

		pos=term-(row*(row-1))/2;

		a=row-pos+1;

		if(row%2==0){
			printf("TERM %ld IS %ld/%ld",term,pos,a);
			printf("\n");
		}

		else if(row%2!=0){
			printf("TERM %ld IS %ld/%ld",term,a,pos);
			printf("\n");
		}
	}
	return 0;
}
