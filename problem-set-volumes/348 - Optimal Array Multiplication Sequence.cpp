/*****  Optimal Array Multiplication Sequence @ 348  *****/

#include<stdio.h>
#define max 15
#define inf 2140999999

long num;

void output(long s[][max], long i, long j){
	if(i==j)	printf("A%d", num++);
	else{  
		printf("(");  
		output(s,i,s[i][j]);  
		printf(" x ");  
		output(s,s[i][j]+1,j);  
		printf(")");  
	}
}

void mcm(long p[], long n){
	long m[max][max]={0};
	long s[max][max]={0};
	long i, j, k, l, q;

	for(l=2; l<=n; l++){
		for(i=1; i<=n-l+1; i++){
			j=i+l-1;
			m[i][j]=inf;
			for(k=i; k<j; k++){
				q = m[i][k] + m[k+1][j] + p[i-1]*p[k]*p[j];
				if(q<m[i][j]){
					m[i][j]=q;
					s[i][j]=k;
				}
			}
		}
	}
	num=1;
	output(s, 1, n);
}

int main(){
	long n, i, cse=0;
	long p[max]={0};

	while(scanf("%ld", &n)==1 && n!=0){
		cse++;
		for(i=1; i<=n; i++)	scanf("%ld %ld", &p[i-1], &p[i]);
		printf("Case %ld: ", cse);
		mcm(p, n);
		printf("\n");
	}
	return 0;
}
