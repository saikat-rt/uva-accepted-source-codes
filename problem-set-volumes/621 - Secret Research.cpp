/*****  secret research @ 621  *****/

#include<stdio.h>
#include<string.h>

long tc, len;
char s[1000000];

int main(){
	scanf("%ld", &tc);
	while(tc--){
		scanf("%s", s);
		len=strlen(s);
		if(len==1){
			if(s[0]=='4' || s[0]=='1')	
				printf("+\n");
		}
		else if(len==2){
			if(s[0]=='7' && s[1]=='8')
				printf("+\n");
		}
		else if(len>2){
			if(s[len-1]=='5' && s[len-2]=='3')
				printf("-\n");
			else if(s[0]=='9' && s[len-1]=='4')
				printf("*\n");
			else if(s[0]=='1' && s[1]=='9' && s[2]=='0')
				printf("?\n");
		}
	}
	return 0;
}
