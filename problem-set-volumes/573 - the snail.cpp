/*****  the snail @ 573  *****/

#include<stdio.h>

double height,up,down,fatig;
long day;
double success;

int main(){
	while(scanf("%lf %lf %lf %lf",&height,&up,&down,&fatig)==4){
		if(height==0)	break;
		else{
			for(day=1,success=0,fatig*=up/100;;day++){
				if(fatig==0)	break;

				success+=up;
				if(success>height)	break;

				success-=down;
				if(success<0)	break;

				up-=fatig;
				if(up<0)	up=0;
			}

			if(success>height)	printf("success on day %ld\n",day);
			else	printf("failure on day %ld\n",day);
		}
	}
	return 0;
}





