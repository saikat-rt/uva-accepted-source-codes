/********** TeX Quotes  @  272  **************/

#include<stdio.h>
#include<string.h>
#include<ctype.h>

char s[2000];
long len, count, i;

int main(){
	count = 1;
	while(gets(s)){
		len = strlen(s);

		for(i = 0; i < len; i++){
			// double quote at the beginning
			if(s[i] == '"' && count % 2 != 0){
				printf("``");
				count++;
			}
			// double quote at the end
			else if(s[i] == '"' && count % 2 == 0){
				printf("''");
				count++;
			}

			else
				printf("%c", s[i]);
		}
		printf("\n");
	}
	return 0;
}
