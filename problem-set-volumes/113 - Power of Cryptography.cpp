/*****  power of cryptography @ 113  *****/

#include<stdio.h>
#include<math.h>

long double n;
long double k,p;

int main(){
	while((scanf("%Lf %Lf",&n,&p)==2) && n>=1 && p>=1){
		k=pow(p,(1/n));
		printf("%0.0Lf\n",k);
	}
	return 0;
}

