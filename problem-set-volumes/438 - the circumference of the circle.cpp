/*****  the circumference of the circle @ 438  *****/

#include<stdio.h>
#include<math.h>
#define pi 2*acos(0)

int main()
{
	double x1,x2,x3,y1,y2,y3;
	double a,b,c,s,r,cir;

	while(scanf("%lf %lf %lf %lf %lf %lf",&x1,&y1,&x2,&y2,&x3,&y3)==6)
	{
		a=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
		b=sqrt((x2-x3)*(x2-x3)+(y2-y3)*(y2-y3));
		c=sqrt((x3-x1)*(x3-x1)+(y3-y1)*(y3-y1));

		s=(a+b+c)/2.0;
		r=(a*b*c)/(4.0*(sqrt(s*(s-a)*(s-b)*(s-c))));
		cir=2.0*pi*r;

		printf("%0.2lf\n",cir);
	}
	return 0;
}
