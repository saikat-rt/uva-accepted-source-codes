/*****  factorial frequencies @ 324  *****/

#include<stdio.h>
#include<string.h>

char f[10000];
char factorial[1010][10000];

void multiply(long k){
	long cin,sum,i;
	long len = strlen(f);
	cin=0;
	i=0;
	while(i<len){
		sum=cin+(f[i] - '0') * k;
		f[i] = (sum % 10) + '0';
		i++;
		cin = sum/10;
	}
	while(cin>0){
		f[i++] = (cin%10) + '0';
		cin/=10;
	}
	f[i]='\0';
	for(long j=0;j<i;j++)
	    factorial[k][j]=f[j];
	factorial[k][i]='\0';
}

void fac(){
	long k;
	strcpy(f,"1");
	for(k=2;k<=1000;k++)
	    multiply(k);
}

void print(long n){
	long i,c0,c1,c2,c3,c4,c5,c6,c8,c7,c9;
	long len = strlen(factorial[n]);
	printf("%ld! --\n",n);
	c0=c1=c2=c3=c4=c5=c6=c7=c8=c9=0;
	for(i=len-1;i>=0;i--){
		if(factorial[n][i]=='0')	c0++;
		else if(factorial[n][i]=='1')	c1++;
		else if(factorial[n][i]=='2')	c2++;
		else if(factorial[n][i]=='3')	c3++;
		else if(factorial[n][i]=='4')	c4++;
		else if(factorial[n][i]=='5')	c5++;
		else if(factorial[n][i]=='6')	c6++;
		else if(factorial[n][i]=='7')	c7++;
		else if(factorial[n][i]=='8')	c8++;
		else if(factorial[n][i]=='9')	c9++;	
	}
	printf("   (0)    %3ld    (1)    %3ld    (2)    %3ld    (3)    %3ld    (4)    %3ld\n"
            "   (5)    %3ld    (6)    %3ld    (7)    %3ld    (8)    %3ld    (9)    %3ld\n",c0,c1,c2,c3,c4,c5,c6,c7,c8,c9);
}
int main(){
	long n;
	factorial[0][0]='1';
	factorial[1][0]='1';
	fac();
	while(scanf("%ld",&n)==1){
		if(n==0)	break;
		else
			print(n);
	}
	return 0;
}
