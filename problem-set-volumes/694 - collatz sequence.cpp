/*****  collatz sequence @ 694 *****/

#include<stdio.h>

long long a,beg,limit,term;
long long step=0;

int main(){
	while(scanf("%lld %lld",&a,&limit)==2){
		if(a<0 && limit<0)	break;
		step++;
		beg=a;
		term=1;
		while(beg!=1){
			if(beg%2==0)
				beg/=2;
			else if(beg%2!=0)
				beg=3*beg+1;
			if(beg>limit)	break;
			term++;
		}
		printf("Case %lld: A = %lld, limit = %lld, number of terms = %lld\n",step,a,limit,term);
	}
	return 0;
}
