/*****  street numbers @ 138  *****/

#include<stdio.h>

int main(){
	printf("%10ld%10ld\n", 6, 8);
	printf("%10ld%10ld\n", 35, 49);
	printf("%10ld%10ld\n", 204, 288);
	printf("%10ld%10ld\n", 1189, 1681);
	printf("%10ld%10ld\n", 6930, 9800);
	printf("%10ld%10ld\n", 40391, 57121);
	printf("%10ld%10ld\n", 235416, 332928);
	printf("%10ld%10ld\n", 1372105, 1940449);
	printf("%10ld%10ld\n", 7997214, 11309768);
	printf("%10ld%10ld\n", 46611179, 65918161);
	return 0;
}
