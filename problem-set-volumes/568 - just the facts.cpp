/*****  568 - Just the Facts  *****/

#include <stdio.h>

long long fact;
long N, i;

int main(){
	while(scanf("%ld", &N)==1){
		fact = 1;

        for(i=1; i<=N; i++){
			fact *= i;

            while(fact % 10==0 )	fact /= 10;

            fact %= 100000;
		}
		printf("%5ld -> %I64d\n", N, fact%10);
    }
	return 0;
}