/*********  ackermann functions  @  371  *************/

#include<stdio.h> 

int main() { 
   long i, j, loop, n, cycle, max = 0, num, temp; 
   
   while(scanf("%ld %ld", &i, &j) == 2){ 
	   if(i == 0 && j == 0)	break; 
       if(i > j){ 
		   temp = i; 
           i = j; 
           j = temp; 
       } 
       max = 0;  
       for(loop = i; loop <= j; loop++){ 
		   cycle = 1; 
           n = loop; 
          
           while(n > 0){ 
			   if(n % 2 != 0) 
               n = 3 * n + 1; 
    
               else if(n % 2 == 0) 
				   n = n / 2;

			   if(n == 1)	break;
               cycle++; 
		   } 
         
		   if(max < cycle){ 
			   max = cycle; 
               num = loop; 
		   } 
       } 
       printf("Between %ld and %ld, %ld generates the longest sequence of %ld values.\n",i,j,num,max); 
   } 
   return 0; 
}