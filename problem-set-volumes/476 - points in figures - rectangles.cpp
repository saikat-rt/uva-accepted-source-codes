/*****  points in figures:rectangles @ 476  *****/

#include <stdio.h>
#include <ctype.h>

char s;
double ulx[13], uly[13], lrx[13], lry[13], x, y;
int figure, point = 0;
int i, j, flag;

int main(){
	i=1, j;
	while(scanf("%c", &s)==1  && s!='*'){
		if(s=='r'){
			scanf("%lf%lf%lf%lf",&ulx[i],&uly[i],&lrx[i],&lry[i]);
			i++;
		}
	}
	while(scanf("%lf%lf", &x, &y)==2){
		if(x==9999.9 && y==9999.9)	break;
		else{
			point++;
			flag=0;
			for(j=1; j<i; j++){
				if((x>ulx[j] && x<lrx[j]) && (y<uly[j] && y>lry[j])){
					flag=1;
					printf("Point %d is contained in figure %d\n", point, j);
				}
			}
			if(flag==0)
				printf("Point %d is not contained in any figure\n", point);
		}
		
	}
	
	
	return 0;
}
