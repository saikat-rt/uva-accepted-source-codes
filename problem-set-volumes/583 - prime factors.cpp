/*****  prime factors @ 583  *****/

#include<stdio.h>
#include<math.h>

long p;

long prime_factor(long s){
	long i, c;
	c=s;
	printf("%ld = ",s);
	if(s<0){
		printf("-1 x ");
		c=-1*s;
	}
	while(c%2==0){
		if(c!=2)
			printf("%ld x ",2);
		
		else
			printf("%ld\n",2);
		c=c/2;
	}

	i=3;
	while(i<=sqrt(c)+1){
		if(c%i==0){
			printf("%ld x ",i);
			c=c/i;
		}
		else	i=i+2;
	}
	if(c>1)	printf("%ld\n",c);
	return 0;
}

int main(){
	while(scanf("%ld",&p)==1){
		if(p==0)	break;
		else
			prime_factor(p);
	}
	return 0;
}
