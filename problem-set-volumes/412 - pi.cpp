/*
*   Pi @ 412
*   [accepted]
*/

#include <cstdio>
#include <cmath>

int gcd(int a, int b) {
    return b == 0 ? a : gcd(b, a % b);
}

int main() {
    int n;
    while (scanf("%d", &n) && n) {
        int ds[100];
        for (int i = 0; i < n; ++i) scanf("%d", &ds[i]);

        int pair = 0;
        for (int i = 0; i < n - 1; ++i)
            for (int j = i + 1; j < n; ++j)
                if (gcd(ds[i], ds[j]) == 1) ++pair;

        if (pair == 0)  printf("No estimate for this data set.\n");
        else {
            double res = sqrt(3.0 * n * (n - 1) / pair);
            printf("%.6lf\n", res);
        }
    }

    return 0;
}