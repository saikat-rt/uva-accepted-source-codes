/*****  simply syntax @ 271  *****/

#include<stdio.h>
#include<string.h>

char s[1000];
long n,len;

int main(){
	while(gets(s)){
		n=0;
		len=strlen(s);

		for(long i=len-1;i>=0;i--){
			if(s[i]>='p' && s[i]<='z')
				n=n+1;

			else if(s[i]=='C'||s[i]=='D'||s[i]=='E'||s[i]=='I'){
				if(n>=2)	n=n-1;
				else	{n=0;break;}
			}

			else if(s[i]=='N'){
				if(n<1)	{n=0;break;}
				else	n=n;
			}
		}

		if(n==1)	printf("YES\n");
		else	printf("NO\n");
	}
	return 0;
}

