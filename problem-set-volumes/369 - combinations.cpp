/*****  combinations @ 369  *****/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

double nCr(long n,long m)  
{  
	long k;  
    long i,j;  
    double c,d;  
  
    c=d=1;   
    k=(m>(n-m))?m:(n-m);   
    for(j=1,i=k+1;(i<=n);i++,j++)   
	{   
		c*=i;   
        d*=j;   
        if(!fmod(c,d) && (d!=1))  
		{  
			c/=d;   
			d=1;   
		}   
	}   
   
	return c;  
}   

int main()  
{  
	long n,m;  
	while(scanf("%ld %ld",&n,&m)==2)
	{
		if(n==0 && m==0)	break;
		else
			printf("%ld things taken %ld at a time is %0.0lf exactly.\n",n,m,nCr(n,m));
		
	}
	return 0;  
}  
