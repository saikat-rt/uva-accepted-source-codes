/*****  3n+1 problem @ 100  *****/

#include<stdio.h>

long i, j, loop, n, cycle, temp, a, b, max=0;

int main(){
	while(scanf("%ld %ld",&i,&j)==2){
		if(i==0 || j==0) break;
		else{
			a=i, b=j;
			
			if(a>b){
				temp=a;
				a=b;
				b=temp;
			}
			
			max=0;
			for(loop=a;loop<=b;loop++){
				cycle=1;
				n=loop;
				
				while(n>0){
					if(n==1) break; 
					else if(n%2!=0)	n=3*n+1;
	
					else if(n%2==0)	n=n/2;
					cycle++;
				}
				if(max<cycle)
					max=cycle;
			}
			printf("%ld %ld %ld\n",i,j,max);
		}
	}
	return 0;
}