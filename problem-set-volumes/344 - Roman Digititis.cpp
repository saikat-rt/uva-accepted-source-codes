/*****  Roman Digititis @ 344  *****/

#include<stdio.h>

int num, i, v, x, l, c;

void  single(int n){
	switch(n){
		case 0: break;
		case 1: i++; break;
		case 2: i+=2; break;
		case 3: i+=3; break;
		case 4: i++;v++; break;
		case 5: v++; break;
		case 6: v++;i++; break;
		case 7: i+=2;v++; break;
		case 8: i+=3;v++; break;
		case 9: i++;x++; break;
	}
}

void couple(int n){
    switch(n){
		case 0: break;
		case 1: x++; break;
		case 2: x+=2; break;
		case 3: x+=3; break;
		case 4: x++;l++; break;
		case 5: l++; break;
		case 6: x++;l++; break;
		case 7: x+=2;l++; break;
		case 8: x+=3;l++; break;
		case 9: x+=1;c++; break;
		case 10: c++; break;
	}
}

int main(){
	while(scanf("%d", &num)==1 && num!=0){
		i=v=x=l=c=0;
		for(int j=1; j<=num; j++){
			single(j%10);
			couple(j/10);
		}
		printf("%d: %d i, %d v, %d x, %d l, %d c\n", num, i, v, x, l, c);
	}
	return 0;
}