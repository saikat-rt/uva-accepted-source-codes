/****  Artificial Intelligence @ 537  *****/

#include <stdio.h>
#include <string.h>

long tc, i, k = 0, len, dot;
char line[500];
double di, dp, du, val;
bool fi, fp, fu;

int main(){
	//freopen("input.txt", "r", stdin);
	scanf("%ld", &tc);
	while(tc--){
		k++;
		scanf(" %[^\n]", line);
		len = strlen(line);
		di = dp = du = 0.0;
		fi = fp = fu = dot = false;
		dot = 0;
		for(i = 0; i < len; i++){
			if(line[i] == 'P' && line[i+1] == '='){
				fp = true;
				i += 2;
				val = 0.1;
				while(1){
					if(line[i] >= '0' && line[i] <= '9'){
						if(dot == 1){
							dp = dp + (line[i] - '0') * val;
							val *= 0.1;
						}
						else
							dp = dp * 10.0 + (line[i] - '0');
						i++;
					}
					else if(line[i] == '.' && (line[i+1] >= '0' && line[i+1] <= '9')){
						dot = 1;
						i++;
					}
					else if(line[i] == 'm'){dp = dp / 1000.0; i++;}
					else if(line[i] == 'k'){dp = dp * 1000.0; i++;}
					else if(line[i] == 'M'){dp = dp * 1000000.0; i++;}
					else if(line[i] == 'W'){dot = 0; break;}
				}
			}
			else if(line[i] == 'I' && line[i+1] == '='){
				fi = true;
				i += 2;
				val = 0.1;
				while(1){
					if(line[i] >= '0' && line[i] <= '9'){
						if(dot == 1){
							di = di + (line[i] - '0') * val;
							val *= 0.1;	
						}
						else
							di = di * 10.0 + (line[i] - '0');
						i++;
					}
					else if(line[i] == '.' && (line[i+1] >= '0' && line[i+1] <= '9')){
						dot = 1;
						i++;
					}
					else if(line[i] == 'm'){di = di / 1000.0; i++;}
					else if(line[i] == 'k'){di = di * 1000.0; i++;}
					else if(line[i] == 'M'){di = di * 1000000.0; i++;}
					else if(line[i] == 'A'){dot = 0; break;}
				}
			}
			else if(line[i] == 'U' && line[i+1] == '='){
				fu = true;
				i += 2;
				val = 0.1;
				while(1){
					if(line[i] >= '0' && line[i] <= '9'){
						if(dot == 1){
							du = du + (line[i] - '0') * val;
							val *= 0.1;
						}
						else
							du = du * 10.0 + (line[i] - '0');
						i++;
					}
					else if(line[i] == '.' && (line[i+1] >= '0' && line[i+1] <= '9')){
						dot = 1;
						i++;
					}
					else if(line[i] == 'm'){du = du / 1000.0; i++;}
					else if(line[i] == 'k'){du = du * 1000.0; i++;}
					else if(line[i] == 'M'){du = du * 1000000.0; i++;}
					else if(line[i] == 'V'){dot = 0; break;}
				}
			}
		}
		printf("Problem #%ld\n", k);
		if(di && du){
			dp = di * du;
			printf("P=%0.2lfW\n\n", dp);
		}
		else if(di && dp){
			du = dp / di;
			printf("U=%0.2lfV\n\n", du);
		}
		else if(du && dp){
			di = dp / du;
			printf("I=%0.2lfA\n\n", di);
		}
	}
	return 0;
}

// end of source code
