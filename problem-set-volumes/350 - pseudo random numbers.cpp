/*****  Pseudo-Random Numbers @ 350  *****/

#include<stdio.h>

long old[200000];
int n;

long cycle(){
	long i, k;
	k = old[n];
	for(i = 0; i<n; i++)
		if(k == old[i])
			return n-i;
	return 0;
}

int main(){
	long c=0, z, i, m, l;
	while(scanf("%ld %ld %ld %ld", &z, &i, &m, &l)==4 && z!=0 && i!=0 && m!=0 && l!=0){
		c++;
		n=-1;
		while(!cycle()){
	      old[++n] = l;
	      l = ((z%m*l%m)%m+i)%m;
		}
	    printf("Case %d: %d\n", c, cycle());
	}
	return 0;
}