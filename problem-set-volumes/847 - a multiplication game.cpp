/*****  a multiplication game @ 847  *****/

#include<stdio.h>

long long number, p;

int main(){
	while(scanf("%lld", &number)==1){
		p=1;
		while(1){
			p=p*9;
			if(p>=number){
				printf("Stan wins.\n");
				break;
			}			
			p=p*2;
			if(p>=number){
				printf("Ollie wins.\n");
				break;
			}			
		}	
	}	
	return 0;
}
