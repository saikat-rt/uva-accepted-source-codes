/*****  cantor fractions @ 880  *****/

#include<stdio.h>
#include<math.h>

long long term,row,pos,a;
double row_p;

int main(){
	while(scanf("%lld",&term)==1){
		row_p=(-1+sqrt(1+8*term))/2;
		row=ceil(row_p);
		
		pos=term-(row*(row-1))/2;
		a=row-pos+1;
			
		printf("%lld/%lld\n",a,pos);
	}
	return 0;
}