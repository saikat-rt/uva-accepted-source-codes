/*****  skew binary @ 575  *****/

#include<stdio.h>
#include<math.h>
#include<string.h>

char s[37];
long len, i, j, skew, temp;

int main(){
	while(gets(s)){
		if(s[0]=='0')	break;
		else{
			len=strlen(s);
			skew=0;
			for(i=0,j=len-1;i<len,j>=0;i++,j--){
				temp=(long)(s[i]-'0');
				skew+=temp*((pow(2,(j+1)))-1);
			}
			printf("%ld\n",skew);
		}
	}
	return 0;
}
			
	