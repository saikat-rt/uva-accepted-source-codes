/*****  brick wall pattern @ 900  *****/

#include<stdio.h>

long long f[1001], pat;
long i, brick;

int main(){
	while(scanf("%ld", &brick) == 1){
		if(brick == 0)	break;
		else if(brick == 1)	 printf("1\n");
		else{
			f[0] = 1;
			f[1] = 1;
			for(i = 2;i <= brick;i++){
				f[i] = f[i-1] + f[i-2];
				pat = f[i];
			}
			printf("%lld\n", pat);
		}
	}
	return 0;
}
