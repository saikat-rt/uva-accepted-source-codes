/*****  231 - Testing the CATCHER  *****/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define MX 10000

typedef struct{
	long length;
	long parent;
}cell;

cell store[MX];
long data[MX];

void initialize(cell *store,long n){
	long i;
	for(i=0;i<n;i++){
		store[i].length=1;
		store[i].parent=-1;
	}
}

long LDS(long *data,long n){
	initialize(store,n);
	long i,j;
	for(i=0;i<n-1;i++){
		for(j=i+1;j<n;j++){
			if(data[j]<=data[i]){
				if(store[i].length+1>store[j].length){
					store[j].length=store[i].length+1;
					store[j].parent=i;
				}
			}
		}
	}
	long temp=0;
	for(i=0; i<n; i++)
		if(store[i].length>temp)
			temp=store[i].length;
		
	return temp;
}

int main(){
	//freopen("231.txt", "r", stdin);
	long i,lds_len,n=0, cse=0;
	
	while(1){
		scanf("%ld", &data[n++]);
		if(data[n-1]==-1){
			cse++;
			lds_len=LDS(data, n-1);
			printf("Test #%ld:\n", cse);
			printf("  maximum possible interceptions: %ld\n", lds_len);
			scanf("%ld", &i);
			if(i==-1)	break;
			else{
				printf("\n");
				memset(data, 0, sizeof(data));
				n=0;
				data[n++]=i;
			}
		}
	}
	return 0;
}