/*****  Marvelous Mazes @ 445  *****/

#include <stdio.h>
#include <ctype.h>


int main(){
	//freopen("445.txt", "r", stdin);
	char ch;
	int sum = 0;
	int i;
	while((ch = getc(stdin))!= EOF) {
		if(isdigit(ch))	sum += ch - '0';
		else {
			if(ch =='!')	printf("\n");
			
			else if(ch =='\n')	printf("\n");
			
			else if(ch =='b')
				for(i = 0; i<sum; i ++)
					printf(" ");
			
			else
				for(i = 0; i<sum; i++)
					printf("%c",ch);
					
			sum = 0;
		}
	}
	return 0;
}