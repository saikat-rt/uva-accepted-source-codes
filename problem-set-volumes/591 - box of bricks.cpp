/*****  box of bricks @ 591  *****/

#include<stdio.h>

long stack, box[55], sum, avg, move;

int main(){
	long set=0;
	while(scanf("%ld",&stack)==1 && stack!=0){
		set++;
		sum=0;
		for(long i=0;i<stack;i++)
			scanf("%ld",&box[i]);

		for(long j=0;j<stack;j++)
			sum+=box[j];

		avg=sum/stack;

		move=0;
		for(long k=0;k<stack;k++)
			if(avg<box[k])
				move+=box[k]-avg;

		printf("Set #%ld\nThe minimum number of moves is %ld.\n\n",set,move);
	}
	return 0;
}

