/*
    Machined Surfaces @ 414
    [accepted]
*/

#include <cstdio>

int main()
{
    int n;
    while (scanf("%d", &n) == 1 && n)
    {
        char line[26];
        int min_space = 23;
        int sum = 0;
        for (int i = 0; i < n; ++i)
        {
            scanf(" %[^\n]", line);
            int j, k;
            for (j = 0; line[j] == 'X'; ++j);
            for (k = 24; k >= j && line[k] == 'X'; --k);

            sum += k - j + 1;
            if (k - j + 1 < min_space)  min_space = k - j + 1;
        }

        sum -= min_space * n;
        printf("%d\n", sum);
    }


    return 0;
}