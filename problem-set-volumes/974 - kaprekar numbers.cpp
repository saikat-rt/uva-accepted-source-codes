/*****  kaprekar numbers @ 974  *****/

#include<stdio.h>

long test,inf,sup,flag;
long kap[]={9,45,55,99,297,703,999,2223,2728,4879,4950,5050,5292,7272,7777,9999,17344,22222,38962};

int main(){
	long cse;
	bool tag=false;
	scanf("%ld",&test);

	cse=0;
	for(long l=0;l<test;l++){
		scanf("%ld %ld",&inf,&sup);
		if(tag==true)
			printf("\n");
		tag=true;
		cse++;
		printf("case #%ld\n",cse);
		flag=0;
		for(long i=0;i<20;i++){
			if(kap[i]>=inf && kap[i]<=sup){
				printf("%ld\n",kap[i]);
				flag++;
			}
		}
		if(flag==0)	printf("no kaprekar numbers\n");
	}
	return 0;
}
