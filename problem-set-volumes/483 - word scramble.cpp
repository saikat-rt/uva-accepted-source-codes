/*****  Word Scramble @ 483  *****/

#include <string>
#include <iostream>
#include <stack>
using namespace std;

int main(){
	//freopen("input.txt", "r", stdin);
   	stack<char> stk;
   	string str;

   	while (getline(cin, str)) {
		
      	for (int i  = 0; i < str.length(); i++) {
			if (str[i] != ' ') {
            	stk.push(str[i]);
        	}
        	else {
				while (!stk.empty()) {
          			cout << stk.top();
            		stk.pop();
            	}
            	if (str[i] == ' ')
               		cout << ' ';
   			}
    	}
      	while (!stk.empty()) {
			cout << stk.top();
            stk.pop();
      	}
		cout << "\n";
   	}
	return 0;
}
