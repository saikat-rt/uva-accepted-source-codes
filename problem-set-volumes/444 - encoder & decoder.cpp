/*****  encoder and decoder @ 444  *****/

#include <stdio.h>
#include <string.h>
#define MAX 253

void decode(char str[]){
	int i, len, temp;

	len = strlen(str);

	for( i = len-1; i >= 0; i--){
		temp = (int)str[i];

		while( temp > 0){
			printf("%d", temp%10);
			temp = temp / 10;
		}
	}
}

void encode(char str[]){
	int  i, len, temp;

	len = strlen(str);

	for( i = len - 1; i >= 0; ){
		if( str[i] == '1'){
			temp = ( (str[i] - 48)*100) + ((str[i-1] - 48)*10) + (str[i-2] - 48);
			printf("%c", temp);

			i = i - 3;
		}

		else {
			temp = (str[i] - 48);
			temp = (10 * temp) + (str[i-1] - 48);
			printf("%c", temp);

			i = i - 2;
		}
	}
}

int main(){
	char str[MAX];

	while(gets(str)){
		if( str[0] >= 48 && str[0] <= 57)	encode(str);
		else	decode(str);
		printf("\n");
	}
	return 0;
}
