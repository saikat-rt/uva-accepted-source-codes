/*****  482 - Permutation Arrays  *****/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define max 100000

char A[max], B[max], *ptr, s[100];
long ia[max];
char ib[20000][50];
long ca, cb, tc, tempL, i, j;
char tempD[23], c;

int main(){
//	freopen("482.txt", "r", stdin);
//	freopen("sai.txt", "w", stdout);
	gets(s);
	tc=atol(s);
	while(tc--){
		gets(s);
		gets(A);
		gets(B);
		ca=cb=0;
		
		for(ca=0, ptr=strtok(A, " "); ptr!=NULL; ptr=strtok(NULL, " "))
			ia[ca++]=atol(ptr);
		for(cb=0, ptr=strtok(B, " "); ptr!=NULL; ptr=strtok(NULL, " ")){
			strcpy(ib[cb], ptr);
			cb++;
		}
		
		for(i=1;i<ca;i++){
			for(j=0;j<ca-i;j++){
				if(ia[j]>ia[j+1]){
					tempL=ia[j];
					ia[j]=ia[j+1];
					ia[j+1]=tempL;
					
					strcpy(tempD, ib[j]);
					strcpy(ib[j], ib[j+1]);
					strcpy(ib[j+1], tempD);
				}
			}
		}
		for(i=0; i<cb; i++)	printf("%s\n", ib[i]);
		memset(ia, 0, sizeof(ia));
		memset(ib, 0, sizeof(ib));
		
		if(tc>0)	printf("\n");
	}
	return 0;
}