/*****  perfection @ 382  *****/

#include <stdio.h>
#include <math.h>

long num, sum, flag, temp;

int main(){
	flag = 1;
	while(scanf("%ld", &num) == 1){
		if(num != 0 && flag == 1){
			printf("PERFECTION OUTPUT\n");
			flag = 0;
		}

		if(num != 0){
			if(num == 1 || num == 2)	printf("%5ld  DEFICIENT\n", num);
			else{
				sum = 1;
				temp = long(sqrt(num));
				for(long i = 2; i <= temp; i++){
					if(num % i == 0){
						sum += num / i;
						if(i != num / i)
							sum += i;
					}
				}

				if(sum == num)	printf("%5ld  PERFECT\n", num);
				else if(sum > num)	printf("%5ld  ABUNDANT\n", num);
				else if(sum < num)	printf("%5ld  DEFICIENT\n", num);
			}
		}

		else if(num == 0){
			printf("END OF OUTPUT\n");
			break;
		}
	}
	return 0;
}




