/*****  Rotating Sentences @ 490  *****/


#include<stdio.h>
#include<string.h>


char str[104][104];

int main(){
	//freopen("490.txt", "r", stdin);
	int k=-1, i, j, l, max=0;
	while(gets(str[++k])){
		if(strlen(str[k])==0)	break;
	}

	for(i=0; i<k; i++){
		l=strlen(str[i]);
		if(l>max)	max=l;

		for(j = l;j<100; j++ )   str[i][j] = ' ';
		str[i][100] = NULL;
	}
	for(i = 0; i<max; i++ ){
		for(j = k-1; j>=0; j-- )
			printf("%c",str[j][i]);

		printf("\n");
	}
	return 0;
}