/*****  Strategic Defense Initiative @ 497  *****/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define MX 100000

char str[19];
long t, n;

typedef struct{
	long length;
	long parent;
}cell;

cell store[MX];
long data[MX], var;

void initialize(cell *store,long n){
	long i;
	for(i=0;i<n;i++){
		store[i].length=1;
		store[i].parent=-1;
	}
}

long LIS(long *data,long n){
	initialize(store,n);
	long i,j;
	for(i=0;i<n-1;i++){
		for(j=i+1;j<n;j++){
			if(data[j]>data[i]){
				if(store[i].length+1>store[j].length){
					store[j].length=store[i].length+1;
					store[j].parent=i;
				}
			}
		}
	}
	long temp=0;
	for(i=0; i<n; i++)
		if(store[i].length>temp){
			temp=store[i].length;
			var=i;
		}
	return temp;
	//return(store[n-1].length);
}

void reconstruct_path(long n){
	if(store[n].parent==-1){
		printf("%ld\n",data[n]);
		return;
	}
	reconstruct_path(store[n].parent);
	printf("%ld\n",data[n]);
}

int main(){
	//freopen("497.txt", "r", stdin);
	long num, lis_len;
	gets(str);
	t=atol(str);
	gets(str);
	while(t--){
		n=0;
		while(gets(str)){
			if(strlen(str)==0) break;
			num=atol(str);
			data[n++]=num;
		}
		lis_len=LIS(data,n);
		printf("Max hits: %ld\n",lis_len);
		if(lis_len!=0)	reconstruct_path(var);

		if(t>0)	printf("\n");
	} 
	return 0;
}