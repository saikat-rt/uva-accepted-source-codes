/*** LCD Display @ 706 ****/

#include <stdio.h>
#include <string.h>

int s;
char digit[10];

void start(char c){
    int i;
    if(c == '0'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '1'){
        for(i = 0; i < s + 2; i++)  printf(" ");
    }
    else if(c == '2'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '3'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '4'){
        for(i = 0; i < s + 2; i++)  printf(" ");
    }
    else if(c == '5'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '6'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '7'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '8'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '9'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
}

void middle(char c){
    int i;
    if(c == '0'){
        for(i = 0; i < s + 2; i++)  printf(" ");
    }
    else if(c == '1'){
        for(i = 0; i < s + 2; i++)  printf(" ");
    }
    else if(c == '2'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '3'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '4'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '5'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '6'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '7'){
        for(i = 0; i < s + 2; i++)  printf(" ");
    }
    else if(c == '8'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '9'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
}

void end(char c){
    int i;
    if(c == '0'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '1'){
        for(i = 0; i < s + 2; i++)  printf(" ");
    }
    else if(c == '2'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '3'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '4'){
        for(i = 0; i < s + 2; i++)  printf(" ");
    }
    else if(c == '5'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '6'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '7'){
        for(i = 0; i < s + 2; i++)  printf(" ");
    }
    else if(c == '8'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
    else if(c == '9'){
        printf(" ");
        for(i = 0; i < s; i++)  printf("-");
        printf(" ");
    }
}

void upper(char c){
    int i;
    if(c == '0'){
        printf("|");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '1'){
        printf(" ");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '2'){
        printf(" ");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '3'){
        printf(" ");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '4'){
        printf("|");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '5'){
        printf("|");
        for(i = 0; i < s; i++)  printf(" ");
        printf(" ");
    }
    else if(c == '6'){
        printf("|");
        for(i = 0; i < s; i++)  printf(" ");
        printf(" ");
    }
    else if(c == '7'){
        printf(" ");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '8'){
        printf("|");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '9'){
        printf("|");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
}

void lower(char c){
    int i;
    if(c == '0'){
        printf("|");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '1'){
        printf(" ");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '2'){
        printf("|");
        for(i = 0; i < s; i++)  printf(" ");
        printf(" ");
    }
    else if(c == '3'){
        printf(" ");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '4'){
        printf(" ");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '5'){
        printf(" ");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '6'){
        printf("|");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '7'){
        printf(" ");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '8'){
        printf("|");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }
    else if(c == '9'){
        printf(" ");
        for(i = 0; i < s; i++)  printf(" ");
        printf("|");
    }

}

int main(){
    //freopen("706.txt", "r", stdin);

    int i, j, k, len;

    while(scanf("%d %s", &s, digit) == 2){
        if(s == 0 && !strcmp(digit, "0"))   break;
        len = strlen(digit);

        for(i = 0; i < len; i++){
            start(digit[i]);
            if(i < len - 1) printf(" ");
        }
        printf("\n");
        for(k = 0; k < s; k++){
            for(i = 0; i < len; i++){
                upper(digit[i]);
                if(i < len - 1) printf(" ");
            }
            printf("\n");
        }
        for(i = 0; i < len; i++){
            middle(digit[i]);
            if(i < len - 1) printf(" ");
        }
        printf("\n");
        for(k = 0; k < s; k++){
            for(i = 0; i < len; i++){
                lower(digit[i]);
                if(i < len - 1) printf(" ");
            }
            printf("\n");
        }
        for(i = 0; i < len; i++){
            end(digit[i]);
            if(i < len - 1) printf(" ");
        }
        printf("\n\n");
    }
    return 0;
}
