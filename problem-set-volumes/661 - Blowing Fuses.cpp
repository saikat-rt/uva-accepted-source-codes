/****  Blowing Fuses @ 661  ****/

#include <stdio.h>
#define on 1
#define off 0


long toggle[23], dpower[23], temp[23];
long kase = 0, n, m, c;

void calc(){
    long i, j, k, process, max = 0, total = 0, fuse = 0;

    for(i = 1; i <= n; i++)   toggle[i] = off;

    for(j = 1; j <= n; j++)   scanf("%ld", &dpower[j]);

    for(k = 0; k < m; k++){
        scanf("%ld", &process);

        if(toggle[process] == off){
            total += dpower[process];
            toggle[process] = on;
            if(total > c){
                 fuse = 1;
                 for(i = 0; i < m - k - 1; i++) scanf("%ld", &temp[i]);
                 break;
            }
            if(total > max) max = total;
        }
        else{
            total -= dpower[process];
            toggle[process] = off;
        }
    }
    if(fuse == 1){
        printf("Sequence %ld\n", kase);
        printf("Fuse was blown.\n\n");
    }
    else{
        printf("Sequence %ld\n", kase);
        printf("Fuse was not blown.\n");
        printf("Maximal power consumption was %ld amperes.\n\n", max);
    }
}

int main(){
    while(scanf("%ld %ld %ld", &n, &m, &c) == 3){
        if(!n && !m && !c)  break;

        kase++;
        calc();
    }
    return 0;
}
