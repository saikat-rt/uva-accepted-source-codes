/*****  binomial showdown @ 530  *****/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

long long nCr(long n,long m)  {  
	long k;  
    long i,j;  
    long long c,d;  
  
    c=d=1;   
    k=(m>(n-m))?m:(n-m);   
    for(j=1,i=k+1;(i<=n);i++,j++)   {   
		c*=i;   
        d*=j;   
        if(!fmod(c,d) && (d!=1))  {  
			c/=d;   
			d=1;   
		}   
	}   
   
	return c;  
}   

int main()  {  
	long n,m;  
	while(scanf("%ld %ld",&n,&m)==2){
		if(n==0 && m==0)	break;
		else
			printf("%lld\n",nCr(n,m));
	}
	return 0;  
}  
