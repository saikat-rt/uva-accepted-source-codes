/*****  102 - Ecological Bin Packing  *****/

#include <stdio.h>

char s[][4] = {"BCG", "BGC", "CBG", "CGB", "GBC", "GCB"};
long b[3], g[3], c[3];

int main(){
   while(scanf("%ld%ld%ld%ld%ld%ld%ld%ld%ld", &b[0],&g[0],&c[0],&b[1],&g[1],&c[1],&b[2],&g[2],&c[2])==9){
      long long m, l=2200000000, i;
      m = b[1]+b[2]+c[0]+c[2]+g[0]+g[1];   
      if (l > m) { l = m; i = 0; }      
      m = b[1]+b[2]+g[0]+g[2]+c[0]+c[1];   
      if (l > m) { l = m; i = 1; }      
      m = c[1]+c[2]+b[0]+b[2]+g[0]+g[1];   
      if (l > m) { l = m; i = 2; }      
      m = c[1]+c[2]+g[0]+g[2]+b[0]+b[1];   
      if (l > m) { l = m; i = 3; }      
      m = g[1]+g[2]+b[0]+b[2]+c[0]+c[1];   
      if (l > m) { l = m; i = 4; }      
      m = g[1]+g[2]+c[0]+c[2]+b[0]+b[1];   
      if (l > m) { l = m; i = 5; }      

      printf("%s %lld\n", s[i], l);
   }   
   return 0;
}