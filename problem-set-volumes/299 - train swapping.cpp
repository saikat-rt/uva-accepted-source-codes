/***** train swapping@@299*****/


#include <stdio.h>
#define N 51

long a[N], car, length, i, j, temp, swap, test, loop;

int main(){	
	scanf("%ld", &test);
	while(test--){
		scanf("%ld", &length);
		for(car = 0; car < length; car++)
			scanf("%ld", &a[car]);
				
		swap = 0;
		for(i = 0; i < length - 1; i++){
			for(j = 0; j < (length - 1) - i; j++){
				if(a[j] > a[j+1]){
					temp = a[j];
					a[j] = a[j+1];
					a[j+1] = temp;
					swap++;
				}		
			}
		}
		printf("Optimal train swapping takes %ld swaps.\n", swap);
	}
	return 0;
}
