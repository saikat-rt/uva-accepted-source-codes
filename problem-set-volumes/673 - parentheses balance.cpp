/*
*   Parentheses Balance @ 673
*   [accepted]
*/

#include <cstdio>
#include <stack>
#include <cstring>

using namespace std;

int main() {
    stack<char> cs;
    int tc;
    char q[130];

    scanf("%d\n", &tc);
    while (tc--) {
        gets(q);
        int len = strlen(q);

        int flag = 0;

        for (int i = 0; i < len; ++i) {
            if (q[i] == '(' || q[i] == '[') cs.push(q[i]);
            else if (q[i] == ')') {
                if (cs.empty() || cs.top() != '(') {
                    flag = 1;
                    break;
                }
                cs.pop();
            }
            else if (q[i] == ']') {
                if (cs.empty() || cs.top() != '[') {
                    flag = 1;
                    break;
                }
                cs.pop();
            }
        }

        if (flag == 0 && cs.empty())   printf("Yes\n");
        else    printf("No\n");

        while (!cs.empty()) cs.pop();
    }

    return 0;
}
