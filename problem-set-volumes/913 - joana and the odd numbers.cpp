/***** joana and the odd numbers @ 913  *****/

#include<stdio.h>

long long odd, num, sum;

int main() {
	while (scanf("%lld", &odd) == 1) {
		if (odd > 1 && odd % 2 == 1) {
			num = (odd * (odd + 1) / 2) + (odd - 1) / 2;
			sum = 3 * num - 6;
			printf("%lld\n", sum);
		}
		else	break;
	}
	return 0;
}
