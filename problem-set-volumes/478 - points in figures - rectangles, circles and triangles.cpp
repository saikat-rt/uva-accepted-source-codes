/*  Points in Figures: Rectangles, Circles, and Triangles @ 478  */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<ctype.h>

char s, index[13];
double ulx[13], uly[13], lrx[13], lry[13], x, y;
double cx[13], cy[13], radius[13], temp, rad;
double ux[13], uy[13], rx[13], ry[13], lx[13], ly[13], a1, a2, a3, a;
int figure, point=0;
int i, j, flag;

int main(){
	int i=1, j;
	while(scanf("%c", &s)==1  && s!='*'){
		if(s=='r'){
			index[i]='r';
			scanf("%lf%lf%lf%lf",&ulx[i],&uly[i],&lrx[i],&lry[i]);
			i++;
		}
		else if(s=='c'){
			index[i]='c';
			scanf("%lf%lf%lf", &cx[i], &cy[i], &radius[i]);
			i++;
		}
		else if(s=='t'){
			index[i]='t';
			scanf("%lf%lf%lf%lf%lf%lf",&ux[i],&uy[i],&rx[i],&ry[i],&lx[i],&ly[i]);
			i++;
		}
	}

	while(scanf("%lf%lf", &x,&y)==2){
		if(x==9999.9 && y==9999.9)	break;
		else{
			point++;
			flag=0;
			for(j=1; j<i; j++){
				if(index[j]=='r'){
					if((x>ulx[j] && x<lrx[j]) && (y<uly[j] && y>lry[j])){
						flag=1;
						printf("Point %d is contained in figure %d\n", point, j);
					}
				}
				else if(index[j]=='c'){
					temp=(cx[j]-x)*(cx[j]-x)+(cy[j]-y)*(cy[j]-y);
					rad=radius[j]*radius[j];
					if(temp<rad){
						flag=1;
						printf("Point %d is contained in figure %d\n", point, j);
					}
				}
				else if(index[j]=='t'){
					a1=fabs((ux[j]*ry[j]-uy[j]*rx[j]+uy[j]*x-ux[j]*y+rx[j]*y-x*ry[j])/2.0);
					a2=fabs((ux[j]*ly[j]-uy[j]*lx[j]+uy[j]*x-ux[j]*y+lx[j]*y-x*ly[j])/2.0);
					a3=fabs((lx[j]*ry[j]-ly[j]*rx[j]+ly[j]*x-lx[j]*y+rx[j]*y-x*ry[j])/2.0);
					a=fabs((ux[j]*ry[j]-uy[j]*rx[j]+uy[j]*lx[j]-ux[j]*ly[j]+rx[j]*ly[j]-lx[j]*ry[j])/2.0);
					if((a1+a2+a3-a)<=0.000001){
						flag=1;
						printf("Point %d is contained in figure %d\n", point, j);
					}
				}
			}
			if(flag==0)
				printf("Point %d is not contained in any figure\n", point);
		}
	}
	return 0;
}




