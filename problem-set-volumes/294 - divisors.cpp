/**** divisors @ 294 ********/

#include <stdio.h> 
#include <math.h> 

long n,root, fst, sec, test, i, j, k, l, num, result, max; 

long divisor(long n) { 
	root=(long)sqrt(n); 
	k=0; 
    for(i=1;i<=root;i++) 
		if(n%i==0)	
			k+=2; 
    
	if(n%root==0)
		if(n/root==root) 
			k--; 

    return k; 
} 

int main() { 
   while(scanf("%ld",&test)==1) { 
	   for(l=0;l<test;l++) { 
		   scanf("%ld %ld",&fst,&sec); 
          
           max=0; 
           for(j=fst;j<=sec;j++) {
			   n=j; 
               result=divisor(n); 
               if(result>max) {
				   max=result; 
				   num=j; 
			   } 
		   } 
           printf("Between %ld and %ld, %ld has a maximum of %ld divisors.\n",fst,sec,num,max); 
       }
   }
   return 0;
}
	

	  
          
      
