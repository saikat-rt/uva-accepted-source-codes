/****  DNA Sorting @ 612  *****/

#include <stdio.h>
#include <string.h>

long tc, m, n, i, j, k, count[101], t;
char str[101][51], temp[51];

int main(){
	//freopen("input.txt", "r", stdin);
	scanf("%ld\n\n", &tc);
	while(tc--){
		scanf("%ld %ld\n", &n, &m);
		
		for(i = 0; i < m; i++)	count[i] = 0;
		for(i = 0; i < m; i++)
			scanf("%s", str[i]);
			
		for(i = 0; i < m; i++){
			for(j = 0; j < n-1; j++){
				for(k = j+1; k < n; k++){
					if(str[i][j] > str[i][k]){
						count[i]++;
					}
				}
			}
		}
		for(j = 0; j < m; j++){
			for(k = 0; k < m-1; k++){
				if(count[k] > count[k+1]){
					t = count[k];
					count[k] = count[k+1];
					count[k+1] = t;
					strcpy(temp, str[k]);
					strcpy(str[k], str[k+1]);
					strcpy(str[k+1], temp);
				}
			}
		}
		
		for(i = 0; i < m; i++)	printf("%s\n", str[i]);
		if(tc > 0)	printf("\n");
	}
	return 0;	
}

