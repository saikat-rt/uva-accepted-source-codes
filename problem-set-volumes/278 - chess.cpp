/*****  chess @ 278  *****/

#include<stdio.h>

long m, n, t;
char chess;

int main(){
	while(scanf("%ld",&t)==1){
		for(long i=0;i<t;i++){
			scanf(" %c %ld %ld",&chess,&m,&n);

			if(chess=='r'){
				if(m<n)	printf("%ld\n",m);
				else	printf("%ld\n",n);
			}

			else if(chess=='k'){
				if((m*n)%2==1)	printf("%ld\n",(m*n+1)/2); 
				else	printf("%ld\n",(m*n)/2);
			}

			else if(chess=='Q'){
				if(m<n)	printf("%ld\n",m);
				else	printf("%ld\n",n);
			}

			else if(chess=='K'){
				m=(m+1)/2;
				n=(n+1)/2;
				printf("%ld\n",m*n);
			}
		}
	}
	return 0;
}
