/*****  lunch in grid city @ 855  *****/

#include<stdio.h>
#include<stdlib.h>

long tc;
long s, a, f;
long str[50005], ave[50005];

int sai(const void *a, const void *b){
   return *(int *)a - *(int *)b;
}

int main(){
	scanf("%ld", &tc);
	while(tc--){
		scanf("%ld %ld %ld", &s, &a, &f);
		for(long i=0; i<f; i++)
			scanf("%ld %ld", &str[i], &ave[i]);

		qsort(str,f,sizeof(long),sai);
		qsort(ave,f,sizeof(long),sai);

		if(f%2==0) printf("(Street: %ld, Avenue: %ld)\n", str[f/2-1], ave[f/2-1]);
		else printf("(Street: %ld, Avenue: %ld)\n", str[f/2], ave[f/2]);
	}
	return 0;
}