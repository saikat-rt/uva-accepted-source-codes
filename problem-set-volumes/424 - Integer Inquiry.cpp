/****  Integer Inquiry @ 424  *****/

#include <stdio.h>
#include <string.h>

int result[500];
char input[110][110];

int main(){
	int k = -1, i, j, max = 0, m, n, p, l, sum, carry;
	
	while(scanf("%s", input[++k]) && strcmp(input[k], "0"));
	
	for(i = 0; i <= k; i++)
		if(strlen(input[i]) > max)
			max = strlen(input[i]);
		
	for(j = 0; j <= k; j++){
		l = strlen(input[j]);
		p = 0;
		if(l < 99){
			for(m = l - 1; m >= 0; m--){
				input[j][99 - p] = input[j][m];
				p++;
			}
				
			for(m = 0; m <= 99 - p; m++)	input[j][m] = '0';
		}			
	} 
	
	n = 0; 
	carry = 0;
	
	for(i = 99; i > 99 - max; i--){
		sum = 0;
		for(j = 0; j <= k; j++)
			if(input[j][i])
				sum += input[j][i] - 48;
			
		sum += carry;
		carry = sum / 10;
		result[n++] = sum % 10;
	}
	if(carry)	result[n++] = carry;
	for(i = n - 1; i >= 0; i--)	printf("%d", result[i]);
	printf("\n");
	return 0;
}
