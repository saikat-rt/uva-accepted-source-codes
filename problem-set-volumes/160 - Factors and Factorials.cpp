/*****  factors and factorials @ 160  *****/

#include<stdio.h>
#include<string.h>

long prime[]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97};
long count[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
long input;
long i, j, k, l, n, c, s;

int main(){
	while(scanf("%ld", &input)==1 && input!=0){
		for(i=2; i<=input; i++){
			j=0;
			n=i;
			while(n!=1){
				while(n%prime[j]==0){
					count[j]++;
					n=n/prime[j];					
				}	
				
				if(n==1)	break;
				j++;
			}			
		}
		printf("%3ld! =", input);
		c=0;
		s=0;
		for(k=0; k<25; k++){
			if(count[k]==0)	break;
			if(c<15){
				printf("%3ld", count[k]);
			}
			if(c>=15){
				if(s==0){
					printf("\n");
					for(l=1; l<=6; l++)	printf(" ");
					s=1;
				}
				printf("%3ld", count[k]);
			}	
			c++;			
		}
		printf("\n");
		memset(count,0,sizeof(count));
	}
	return 0;
}
