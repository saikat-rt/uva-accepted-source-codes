/***** continued fractions @ 834  *****/

#include<stdio.h>

long q[1001];
long l, h, i, temp;

int main(){
	while(scanf("%ld %ld",&l,&h)==2){
		i=0;
		while(l%h!=0){
			q[i]=l/h;
			temp=h;
			h=l%h;
			l=temp;
			i++;
			if(l%h==0){
				q[i]=l/h;
				break;
			}
		}

		long j;
		for(j=0;j<=i;j++){
			if(j==0)
				printf("[%ld;",q[j]);
			else if(j==i)
				printf("%ld]",q[j]);
			else
				printf("%ld,",q[j]);
		}
		printf("\n");
	}
	return 0;
}

