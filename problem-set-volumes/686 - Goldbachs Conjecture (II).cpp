/*
*   Goldbach's Conjecture (II) @ 686
*   [accepted]
*/

#include <cstdio>

#define M 65536

bool marked[M];

bool isPrime(int n)
{
    if (n < 2)  return false;
    if (n == 2) return true;
    if (n % 2 == 0) return false;
    return marked[n] == false;
}

void sieve()
{
    for (int i = 3; i * i <= M; i += 2) {
        if (marked[i] == false) {
            for (int j = i * i; j <= M; j += i + i) {
                marked[j] = true;
            }
        }
    }
}

int main()
{
    sieve();

    int n;
    while (scanf("%d", &n) == 1 && n) {
        int count = 0;
        for (int i = 2; i <= n / 2; ++i)
            if (isPrime(i) && isPrime(n - i))   ++count;

        printf("%d\n", count);
    }

    return 0;
}
