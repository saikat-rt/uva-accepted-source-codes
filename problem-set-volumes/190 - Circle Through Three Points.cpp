/*****  circle through three points @ 190  *****/

#include<stdio.h>
#include<math.h>
#include<stdlib.h>

char sign(double x){
	return (x>0)?'+':'-';
}

int main(){
	double ax,ay,bx,by,cx,cy,temp,A1,A2,B1,B2,C1,C2;
	while(scanf("%lf %lf %lf %lf %lf %lf",&ax,&ay,&bx,&by,&cx,&cy)==6){
		A1 = by - ay;
		B1 = ax - bx;

		A2 = cy - ay;
		B2 = ax - cx;

		temp=A1;
		A1=B1;
		B1=temp;

		temp=A2;
		A2=B2;
		B2=temp;

		A1=-A1;
		A2=-A2;

		C1 = (-A1*((ax+bx)/2.0))-(B1*(ay+by)/2.0);
		C2 = (-A2*((ax+cx)/2.0))-(B2*(ay+cy)/2.0);

		double x,y,r;

		y = (A1*C2 - A2*C1)/(A2*B1-A1*B2);
	    x = (B1*C2 - B2*C1)/(B2*A1-B1*A2);

		r = sqrt((ax-x)*(ax-x)+(ay-y)*(ay-y));

		printf("(x %c %.3lf)^2 + (y %c %.3lf)^2 = %.3lf^2\n",sign(-x),fabs(x),sign(-y),fabs(y),r);
		printf("x^2 + y^2 %c %.3lfx %c %.3lfy %c %.3lf = 0\n\n",sign(-2*x),fabs(-2*x),sign(-2*y),fabs(-2*y),sign(x*x+y*y-r*r),fabs(x*x+y*y-r*r));
	}
	return 0;
}




