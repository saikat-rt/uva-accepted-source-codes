/*****  clockhands @ 579  *****/

#include<stdio.h>
#include<ctype.h>

long hour, min;
char c;
double temp, angle;

int main(){
	while(scanf("%ld %c %ld",&hour,&c,&min)==3){
		if(hour==0 && min==0)	break;
		else{
			temp=hour*30-min*5.5;
			if(temp<0)
				temp*=-1;
			else
				temp;

			if(temp<180)
				angle=temp;
			else
				angle=360-temp;
			printf("%0.3lf\n",angle);
		}
	}
	return 0;
}
