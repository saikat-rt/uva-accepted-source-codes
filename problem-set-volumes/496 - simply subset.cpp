/*****  simply subset @ 496  *****/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

char A[1000], B[1000], *ptr;
long ia[1000], ib[1000];
long ca, cb, i, j, count;

int main() {
	while(gets(A)!=NULL){
		gets(B);
		
		ca=cb=0;
		for(ca=0, ptr=strtok(A, " "); ptr!=NULL; ptr=strtok(NULL, " "))
			ia[ca++]=atol(ptr);
		for(cb=0, ptr=strtok(B, " "); ptr!=NULL; ptr=strtok(NULL, " "))
			ib[cb++]=atol(ptr);
		
		count=0;
		for(i=0; i<ca; i++)
			for(j=0; j<cb; j++)
				if(ia[i]==ib[j])
					count++;
				
			
		if(count==ca && count==cb)	printf("A equals B\n");
		else if(count<ca && count==cb)	printf("B is a proper subset of A\n");
		else if(count==ca && count<cb)	printf("A is a proper subset of B\n");
		else if(count==0 && count==0)	printf("A and B are disjoint\n");
		else	printf("I'm confused!\n");
	}
	return 0;
}
