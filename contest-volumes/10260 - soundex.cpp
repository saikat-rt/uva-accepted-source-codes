/*****  soundex @ 10260  *****/

#include<stdio.h>
#include<string.h>

long code[28] = {0,1,2,3,0,1,2,0,0,2,2,4,5,5,0,1,2,6,2,3,0,1,0,2,0,2};
char s[28];
long len;
long v;

int main(){
	while(gets(s)){ 
		v=0;
		len=strlen(s);
		for(long i=0;i<len;i++){
			if(code[s[i]-'A']!=v){
				v=code[s[i]-'A'];
				if(v!=0)
					printf("%ld",v);
			}
		}
		printf("\n");
	}
	return 0;
}


