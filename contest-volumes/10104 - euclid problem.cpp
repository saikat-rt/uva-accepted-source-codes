/*****  euclid problem @ 10104  *****/

#include<stdio.h>
#include<math.h>

long x, y, c;

long gcd(long p, long q, long *x, long *y){
	long x1,y1;
	long g;

	if(q>p)	return (gcd(q,p,y,x));

	if(q==0){
		*x=1;*y=0;
		return (p);
	}

	g=gcd(q,p%q,&x1,&y1);

	*x=y1;
	*y=long(x1-floor(p/q)*y1);

	return g;
}

int main(){
	while(scanf("%ld %ld",&x,&y)==2){
		c=gcd(x,y,&x,&y);
		printf("%ld %ld %ld\n",x,y,c);
	}
	return 0;
}
