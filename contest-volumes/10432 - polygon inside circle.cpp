/***** polygon inside a circle____acm 10432 *****/

#include<stdio.h>
#include<math.h>
#define ANG 4*acos(0)

double r, n, area;

int main(){
	while(scanf("%lf %lf", &r, &n) == 2){
		if(r > 0 && r < 20000 && n > 2 && n < 20000){
			area=0.5 * n * r * r * sin(ANG / n);
			printf("%.3lf\n", area);
		}
	}
	return 0;
}
