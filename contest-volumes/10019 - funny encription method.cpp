/*****  funny encryption method @ 10019  *****/

#include<stdio.h>
#include<math.h>

long test, decimal, num_b, num_h, temp, cb, ch;
long binary[37], hexa[37];

int main(){
	while(scanf("%ld",&test)==1 && test>0){
		for(long j=0;j<test;j++){
			scanf("%ld",&decimal);
			if(decimal<=0)	break;
			else{
				long k=0;
				num_b=decimal;
				while(num_b!=0){
					temp=num_b%2;
					binary[k]=temp;
					k++;
					num_b/=2;
				}

				cb=0;
				for(long m=0;m<k;m++){
					if(binary[m]==1)
						cb++;
				}

				num_h=decimal;
				long power=0;
				long kili=0;
				while(num_h!=0){
					temp=num_h%10;
					kili+=temp*(long)pow(16,power);
					num_h/=10;
					if(num_h==0)	break;
					power++;
				}

				long l=0;
				while(kili!=0){
					temp=kili%2;
					hexa[l]=temp;
					l++;
					if(kili==0)
						break;
					kili/=2;
				}

				ch=0;
				for(long n=0;n<l;n++)
					if(hexa[n]==1)
						ch++;

				printf("%ld %ld\n",cb,ch);
			}
		}
	}
	return 0;
}
