/*
*   Big Chocolate @ 10970
*   [accepted]
*/

#include <cstdio>

int main()
{
    int row, col;
    while (scanf("%d %d", &row, &col) == 2) {
        printf("%d\n", row * col - 1);
    }

    return 0;
}