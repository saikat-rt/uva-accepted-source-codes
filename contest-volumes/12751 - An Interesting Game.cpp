/*
*   An Interesting Game @ 12751
*   [accepted]
*/

#include <stdio.h>

int main() {
    int tc, kase = 0;
    int n, k, x;
    int ntotal, ktotal;

    scanf("%d", &tc);

    while (tc--) {
        scanf("%d %d %d", &n, &k, &x);

        ntotal = (n * (n + 1)) / 2;

        ktotal = ((2 * x + k - 1) * k) / 2;

        printf("Case %d: %d\n", ++kase, ntotal - ktotal);
    }

    return 0;
}


