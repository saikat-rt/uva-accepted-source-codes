/*
*   Banglawash @ 12700
*   [accepted]
*/

#include <stdio.h>

int main() {
    int tc, kase = 0;
    scanf("%d", &tc);

    while (tc--) {
        int n;
        scanf("%d", &n);

        int cb, cw, ca, ct;
        cb = cw = ca = ct = 0;
        char ch;

        for (int i = 0; i < n; ++i) {
            scanf(" %1c", &ch);

            switch (ch) {
                case 'B':
                    cb++;
                    break;
                case 'W':
                    cw++;
                    break;
                case 'A':
                    ca++;
                    break;
                case 'T':
                    ct++;
                    break;
            }
        }

        printf("Case %d: ", ++kase);

        if (ca == n)   printf("ABANDONED\n");
        else if (cb == n || cb + ca == n)    printf("BANGLAWASH\n");
        else if (cw == n || cw + ca == n)   printf("WHITEWASH\n");
        else if (cb == cw)  printf("DRAW %d %d\n", cb, ct);
        else if (cw > cb)   printf("WWW %d - %d\n", cw, cb);
        else if (cw < cb)   printf("BANGLADESH %d - %d\n", cb, cw);
    }

    return 0;
}
