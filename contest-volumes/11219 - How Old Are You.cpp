/*****  how old are you? @ 11219  *****/

#include<stdio.h>

long tc, d1, d2, m1, m2, y1, y2, cse = 0, y;
char s1, s2;

int main(){
	scanf("%ld", &tc);
	while(tc--){
		cse++;
		scanf("%ld%c%ld%c%ld", &d1, &s1, &m1, &s2, &y1);
		scanf("%ld%c%ld%c%ld", &d2, &s1, &m2, &s2, &y2);

		if(d1 < d2)	m1 = m1 - 1;
		if(m1 < m2)	y1 = y1 - 1;

		y = y1 - y2;

		if(y < 0)	printf("Case #%ld: Invalid birth date\n", cse);
		else if(y > 130)	printf("Case #%ld: Check birth date\n", cse);
		else	printf("Case #%ld: %ld\n", cse, y);
	}
	return 0;
}
