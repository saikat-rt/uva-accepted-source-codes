/**  Phone List @ 11362  **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct{
	char c[13];
}phone;

int tc, n, i, k, flag, count, lens, lent;
phone number[10010];
char s[13], t[13], y[13];

int comp(void const *A, void const *B){
	phone *a = (phone*)A;
	phone *b = (phone*)B;
	
	return (strcmp(a->c, b->c));
}

int main(){
	//freopen("input.txt", "r", stdin);
	scanf("%d", &tc);
	
	while(tc--){
		scanf("%d", &n);
		for(i = 0; i < n; i++)	scanf(" %s", number[i].c);
		
		qsort(number, n, sizeof(phone), comp);
		
		flag = 0;
		
		for(i = 0; i < n-1; i++){
			strcpy(s, number[i].c);
			strcpy(t, number[i+1].c);
			
			lens = strlen(s);
			lent = strlen(t);
			
			count = 0;
			if(lens > lent){
				k = lens;
				lens = lent;
				lent = k;
				strcpy(y, s);
				strcpy(s, t);
				strcpy(t, y);
			}
			
			for(k = 0; k < lent; k++)
				if(count < lens)
					if(t[k] == s[count])
						count++;
					else	break;
						
			if(count == lens){
				flag = 1;
				break;
			}
		}
		if(flag == 1)	printf("NO\n");
		else	printf("YES\n");
	}
	return 0;
}

