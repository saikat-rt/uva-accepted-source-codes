/*
    GCD The Largest @ 12708
    [accepted]
*/

#include <stdio.h>

int main()
{
    int tc;
    scanf("%d", &tc);

    while (tc--)
    {
        long n;
        scanf("%ld", &n);

        printf("%ld\n", n / 2);
    }

    return 0;
}
