/*****  Credit Check @ 11743  ****/

#include <stdio.h>
#include <ctype.h>

int tc, dt, t, i, tmp1, tmp2;
char a[5][5];

int main(){
	scanf("%d", &tc);
	while(tc--){
		for(i = 0; i < 4; i++)
			scanf(" %s", a[i]);
		
		dt = t = 0;
		for(i = 3; i >= 0; i--){
			tmp1 = 2 * (a[i][2] - '0');
			tmp2 = 2 * (a[i][0] - '0');
			
			dt += (tmp1 % 10) + (tmp1 / 10);
			dt += (tmp2 % 10) + (tmp2 / 10);
			
		}
		for(i = 3; i >= 0; i--){
			tmp1 = a[i][3] - '0';
			tmp2 = a[i][1] - '0';
			
			t += tmp1 + tmp2;
		}
		
		dt += t;
		
		if(dt % 10 == 0)	printf("Valid\n");
		else	printf("Invalid\n");
	}
	return 0;
}
		
