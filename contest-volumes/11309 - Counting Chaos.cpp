/*  Counting Chaos @ 11309  */

#include <stdio.h>

int tc;
int h1, h2, m1, m2;
char c;

int main(){
	scanf("%d", &tc);
	while(tc--){
		scanf("%1d%1d%c%1d%1d", &h1, &h2, &c, &m1, &m2);
		
		for(;;){
			m2++;
			if(m2 == 10)	m1++, m2 = 0;
			if(m1 == 6)	h2++, m1 = 0;
			if(h2 == 10)	h1++, h2 = 0;
			if(h1 == 2 && h2 == 4)	h1 = 0, h2 = 0;

			// it is going to be a long one :)
			if(h1 == 0){
				if(h2 == 0){
					if(m1 == 0){
						printf("%d%d:%d%d\n", h1, h2, m1, m2);
						break;
					}
					else if(m1 == m2){
						printf("%d%d:%d%d\n", h1, h2, m1, m2);
						break;
					}
				}
				else{
					if(h2 == m2){
						printf("%d%d:%d%d\n", h1, h2, m1, m2);
						break;
					}
				}
			}
			else{
				if(h1 == m2 && h2 == m1){
					printf("%d%d:%d%d\n", h1, h2, m1, m2);
						break;
				}
			}
		}
	}
	return 0;
}