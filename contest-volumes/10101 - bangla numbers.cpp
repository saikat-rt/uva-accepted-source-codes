/*****  10101 - Bangla Numbers  *****/

#include<stdio.h>

long long num;
long long cse=0;

void unit(long long n){
	if(n!=0)
		printf(" %lld kuti", n);
	else	printf(" kuti");	
}

void shata(long long n){
	if(n>=100){
		if(n/100!=0)
			printf(" %lld shata", n/100);	
		else	printf(" shata");
	}	
	n=n%100;
	unit(n);
}

void hajar(long long n){
	if(n>=1000){
		if(n/1000!=0)
			printf(" %lld hajar", n/1000);
		else	printf(" hajar");		
	}	
	n=n%1000;
	shata(n);
}

void lakh(long long n){
	if(n>=100000){
		if(n/100000!=0)
			printf(" %lld lakh", n/100000);
		else	printf(" lakh");		
	}	
	n=n%100000;
	hajar(n);
}

void kuti(long long n){
	if(n>=10000000){
		printf(" %lld kuti", n/10000000);	
	}
	n=n%10000000;
	lakh(n);
}

int main(){
	while(scanf("%lld", &num)==1){
		cse++;
		printf("%4lld.", cse);
		if(num<100)	printf(" %lld\n", num);
		else{
			if(num>=10000000){
				kuti(num/10000000);		
			}	
			num=num%10000000;
			if(num>=100000){
				printf(" %lld lakh", num/100000);	
				num=num%100000;
			}
			if(num>=1000){
				printf(" %lld hajar", num/1000);
				num=num%1000;				
			}
			if(num>=100){
				printf(" %lld shata", num/100);
				num=num%100;				
			}
			if(num>0)	printf(" %lld", num);
			printf("\n");
		}			
	}
	return 0;
}
