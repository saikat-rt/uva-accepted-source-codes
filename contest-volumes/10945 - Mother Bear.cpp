/**********  mother bear  @  10945  ************/
		
#include<stdio.h> 
#include<string.h> 
#include<ctype.h> 

char s[1001], pal[1001], temp[1001]; 
long len, i, j, l=0, flag=0, p, q; 

int main() { 
   while(gets(s)) { 
	   if(!strcmp(s,"DONE")) break;  
       len=strlen(s); 
       for(i=0;i<len;i++) { 
		   if(s[i]>='A' && s[i]<='Z') 
			   temp[i]=s[i]+'a'-'A'; 
           else 
			   temp[i]=s[i]; 
       } 
      
	   l=0;   
       for(j=0;j<len;j++) { 
		   if(isalpha(temp[j]))  { 
			   pal[l]=temp[j]; 
               l++; 
		   } 
       } 
      
	   pal[l]='\0';
       flag=0; 
       for(p=0,q=strlen(pal)-1;p<q;p++,q--){ 
		   if(pal[p]==pal[q]) 
			   flag++; 
		   else { 
			   flag=-1; 
			   break; 
		   } 
       } 
        
       if(flag>=0) printf("You won't be eaten!\n"); 
       else   printf("Uh oh..\n"); 
   } 
   return 0; 
}
