/*
*   Draw Grid @ 11074
*   [accepted]
*/

#include <cstdio>

int main() {
    int s, t, n, kase = 0;

    while (scanf("%d %d %d", &s, &t, &n) == 3 && s && t && n) {
        int tot_star = t * (n + 1) + s * n;

        printf("Case %d:\n", ++kase);

        int g = 0;
        while (g < n + 1) {
            for (int i = 0; i < t; ++i) {
                for (int j = 0; j < tot_star; ++j)  printf("*");
                printf("\n");
            }

            ++g;
            if (g == n + 1)  break;

            for (int i = 0; i < s; ++i) {
                int w = 0;
                while (w < n + 1) {
                    for (int j = 0; j < t; ++j) printf("*");
                    ++w;
                    for (int k = 0; k < s && w < n + 1; ++k) printf(".");
                }
                printf("\n");
            }
        }
        printf("\n");
    }

    return 0;
}
