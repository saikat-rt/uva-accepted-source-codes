
#include <stdio.h>
#include <string.h>


long fibonacci[] = {1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765,10946,17711,28657,46368,75025,121393,196418,317811,514229,832040,1346269,2178309,3524578,5702887,9227465,14930352,24157817,39088169,63245986,102334155,165580141,267914296,433494437,701408733,1134903170,1836311903};
long T, N, input[123];
char cipher[110], temp[110], dcipher[110];

void init(){
    long i;
    for(i = 0; i < 110; i++)    dcipher[i] = ' ';
}

void eliminate_trailing_spaces(){
	long i;

    for(i = strlen(dcipher); i >= 2 && dcipher[i - 1] == ' '; i--) dcipher[i - 1] = dcipher[i];

	return;
}

int main(){
    //freopen("input.txt", "r", stdin);
    long i, j, k, len;
    char c;
    scanf("%ld", &T);
    while(T--){
        scanf("%ld", &N);
        for(i = 0; i < N; i++)  scanf("%ld", &input[i]);
        scanf("%c", &c);
        gets(temp);

        len = strlen(temp);
        k = -1;
        for(j = 0; j < len; j++)
            if(temp[j] >= 'A' && temp[j] <= 'Z')
                cipher[++k] = temp[j];

        init();

        for(i = 0; i < N; i++)
            for(j = 0; j < 50; j++)
                if(input[i] == fibonacci[j])
                    dcipher[j] = cipher[i];

        eliminate_trailing_spaces();

        printf("%s\n", dcipher);

        memset(temp, '\0', sizeof(char));
        memset(cipher, '\0', sizeof(char));
        memset(dcipher, '\0', sizeof(char));
        memset(input, '\0', sizeof(long));
    }
    return 0;
}
