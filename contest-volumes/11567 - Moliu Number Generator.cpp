/*****  11567  @  Moliu Number Generator  *****/

#include<stdio.h>

long long s;
long long counter;

int main(){
	while(scanf("%lld", &s)==1){
		counter=0;
		if(s==0)	printf("0\n");
		else{
			while(s!=0){
				if(s==0)	break;
				else if(s%2==0){
					s=s/2;
					counter++;
				}
				else if(s%2==1){
					if(((s+1)/2)%2==0 && s>3){
						s=s+1;
						counter++;
					}
					else{
						s=s-1;
						counter++;
					}
				}
			}
			printf("%lld\n", counter);
		}
		
	}
	return 0;
}
