/**  Simple Base Conversion @ 10473  **/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

char number[19], flag;
long i, k, p, len;

void H2D(){
	long deci = 0, c;
	for(c = 2; c < len; c++){
		if(isdigit(number[c])){
			deci = deci * 16 + (number[c] - '0');
		}
		else if(isalpha(number[c])){
			deci = deci * 16 + (long)(number[c]) - 55;
		}
	}	
	printf("%ld\n", deci);
}

void D2H(){
	char hexa[100];
	long n = -1, c, deci = 0, rem;
	for(c = 0; c < len; c++){
		deci = deci * 10 + (number[c] - '0');
	}
	while(deci != 0){
		rem = deci % 16;
		deci = deci / 16;
		if(rem >= 0 && rem <= 9)	hexa[++n] = rem + '0';
		else if(rem >=10 && rem <= 15)	hexa[++n] = (char)(rem + 55);
	}
	printf("0x");
	for(i = n; i >= 0; i--)	printf("%c", hexa[i]);
	printf("\n");
}

int main(){
	while(gets(number)){
		if(number[0] == '-')	break;
		else{
			len = strlen(number);
			flag = 'd';
			for(i = 0; i < len; i++)
				if(number[i] == 'x')
					flag = 'h';
			
			if(flag == 'h')	H2D();
			else if(flag == 'd')	D2H();
		}
	}
	return 0;
}
