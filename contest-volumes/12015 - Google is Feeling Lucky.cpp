/*
	Google is Feeling Lucky @ 12015
	[accepted]
	[runtime: 0.016]
*/

#include <stdio.h>
#include <string.h>

int main() {
	char url[11][101];
	int k[11], tc, kase = 0;

	scanf("%d", &tc);

	while(tc--) {
		int i;
		int max = -1;
		for(i = 0; i < 10; i++) {

			scanf(" %s %d", url[i], &k[i]);
			if(k[i] > max)	max = k[i];
		}

		printf("Case #%d:\n", ++kase);
		for(i = 0; i < 10; i++) {
			if(k[i] == max) {
				printf("%s\n", url[i]);
			}
		}
	}

	return 0;
}
