/*****  What's Cryptanalysis? @ 10008  *****/

#include<stdio.h>
#include<string.h>

char s[10000000];
long freq[28], res[28];
long i, j, k, u, r, p, len, line;

int main(){
	while(scanf("%ld", &line)==1){
		for(i=0; i<28; i++){ 
			freq[i]=0;  
			res[i]=0;
		}
		while(line--){
			scanf(" %[^\n]",s);
			len=strlen(s);
			for(j=0; j<len; j++){
				if(s[j]>='A' && s[j]<='Z')
					freq[s[j]-'A']++;
				else if(s[j]>='a' && s[j]<='z')
					freq[s[j]-'a']++; 	
			}
		}
		
		for(k=0;k<26; k++){ 		
			res[k]=freq[k];		
		}	
		
		for(p=1; p<26; p++){ 
			for(r=0; r<26-p; r++){
				if(res[r]<res[r+1]){
					u=res[r];
					res[r]=res[r+1];
					res[r+1]=u;
				}
			}
		}
		for(i=0;i<26;i++){
			if(res[i]){
				for(j=0;j<26;j++){
					if(freq[j]==res[i]&&freq[j]!=0) {
						printf("%c %ld\n",j+'A',res[i]);
						freq[j]=0;
					}
				}
			}
		}
	}
	return 0;
}


