/* Bafana Bafana @ 11805 */

/* Accepted */

#include <stdio.h>		


int main()
{
	int tc, kase = 0, totalPlayer, totalPass, startPlayer, endPlayer, temp;

	scanf("%ld", &tc);

	while(tc--)
	{
		kase++;
		
		scanf("%d %d %d", &totalPlayer, &startPlayer, &totalPass);

		endPlayer = (startPlayer + totalPass) % totalPlayer;	

		if(endPlayer == 0)	endPlayer = totalPlayer;	
		
		printf("Case %d: %d\n", kase, endPlayer);
	}
	return 0;
} 
				
