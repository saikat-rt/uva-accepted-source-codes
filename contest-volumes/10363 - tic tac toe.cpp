/*****  tic tac toe @ 10363  *****/

#include<stdio.h>
#include<string.h>

char g[3][4];

int win(char ch){
	int i, j;

	for ( i = 0; i < 3; i++){
		for (j = 0; j < 3 && g[i][j] == ch; j++);
		if (j == 3)
			return 1;
		for (j = 0; j < 3 && g[j][i] == ch; j++);
		if (j == 3)
			return 1;
	}
	for (i = 0; i < 3 && g[i][i] == ch; i++);
	if (i == 3)
		return 1;
	for (i = 0; i < 3 && g[i][2-i] == ch; i++);
	if (i == 3)
		return 1;
	return 0;
}

int main(){
	//freopen("10363.txt", "r", stdin);
	int i,j,t,Xn,On;
    scanf("%d",&t);
    while (t--){
		scanf(" %s %s %s",g[0],g[1],g[2]);

        Xn = On = 0;
        for (i=0;i<3;i++){
			for (j=0;j<3;j++){
				if (g[i][j] == 'X') Xn++;
				if (g[i][j] == 'O') On++;
			}
		}

		if (On > Xn || Xn > On + 1|| win('X') && win('O')|| win('O') && Xn != On|| win('X') && Xn == On)
			printf("no\n");
		else
			printf("yes\n");
	}
	return 0;
}
