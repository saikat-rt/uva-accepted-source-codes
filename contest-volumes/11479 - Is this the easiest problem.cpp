/*****  Is this the easiest problem? @ 11479  *****/

#include<stdio.h>

double a, b, c, s;
int tc, cse=0;

int main(){
	scanf("%d", &tc);
	while(tc--){
		cse++;
		scanf("%lf %lf %lf", &a, &b, &c);
		s=(a+b+c)/2;
		if(s<=a || s<=b || s<=c)
			printf("Case %d: Invalid\n", cse);
		else{
			if(a==b && b==c)	printf("Case %d: Equilateral\n", cse);
			else if(a==b || b==c || c==a)	printf("Case %d: Isosceles\n", cse);
			else if(a!=b && b!=c)	printf("Case %d: Scalene\n", cse);
		}
	}
	return 0;
}