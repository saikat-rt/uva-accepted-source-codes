/***********  the knights of the round table  @  10195  *************/ 

#include<stdio.h>
#include<math.h>

double a, b, c, r, s, area;

int main(){
	while(scanf("%lf %lf %lf",&a,&b,&c)==3){
		if(a==0.0||b==0.0||c==0.0)
			printf("The radius of the round table is: 0.000\n");
		else{
			s=(a+b+c)/2.0;
			area=sqrt(s*(s-a)*(s-b)*(s-c));
			r=area/s;
			printf("The radius of the round table is: %0.3lf\n",r);
		}
	}
	return 0;
}
