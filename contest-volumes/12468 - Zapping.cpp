/** Zapping @ 12468 **/

#include <stdio.h>

int main(){
	int c, d, bp, t;
	
	while(scanf("%d %d", &c, &d) == 2){
		if(c == -1 && d == -1)	break;
		else{
			if(c > d){
				t = c;
				c = d;
				d = t;
			}
			
			bp = d - c;
			
			if(bp > 50)	bp = 99 - bp + 1;
			
			printf("%d\n", bp);
		}
	}
	return 0;
}
