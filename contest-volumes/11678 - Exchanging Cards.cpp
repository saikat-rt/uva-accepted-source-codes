/*****  Exchanging Cards @ 11678  *****/

#include <stdio.h>


long a, b, x[10010], y[10010], p[10010], q[10010];
long i, j, k, m, count, flag;

int main(){
	while(scanf("%ld %ld", &a, &b)==2 && a!=0 && b!=0){
		count=0;
		for(i=0; i<a; i++)	scanf("%ld", &x[i]);
		for(j=0; j<b; j++)	scanf("%ld", &y[j]);

		p[0]=x[0];
		k=0;
		for(i=1; i<a; i++)
			if(x[i]!=p[k])
				p[++k]=x[i];
		
		q[0]=y[0];
		m=0;
		for(i=1; i<b; i++)
			if(y[i]!=q[m])
				q[++m]=y[i];

		if(k<m){
			for(i=0; i<=k; i++){
				for(j=0; j<=m; j++){
					if(p[i]!=q[j])
						flag=1;
					else{
						flag=0;
						break;
					}
				}
				if(flag==1)	count++;
			}
		}
		else{
			for(i=0; i<=m; i++){
				for(j=0; j<=k; j++){
					if(q[i]!=p[j])
						flag=1;
					else{
						flag=0;
						break;
					}
				}
				if(flag==1)	count++;
			}
		}
		printf("%ld\n", count);
	}
	return 0;
}