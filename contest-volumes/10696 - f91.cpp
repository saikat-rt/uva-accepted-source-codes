/********  f91  @  10696  ************/

#include<stdio.h>

long num, res;

int main(){
	while(scanf("%ld", &num) == 1){
		if(num == 0)	break;
		else if(num <= 100)	res = 91;
		else if(num > 100)	res = num - 10;
		printf("f91(%ld) = %ld\n", num, res);
	}
	return 0;
}
