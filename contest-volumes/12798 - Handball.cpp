/*
*   Handball @ 12798
*   [accepted]
*/


#include <stdio.h>

int main() {
    int nom, nop;

    while (scanf("%d %d", &nop, &nom) == 2) {
        int found_zero = 0, count = 0;
        int m;

        for (int i = 0; i < nop; ++i) {
            for (int j = 0; j < nom; ++j) {
                scanf("%d", &m);
                if (m == 0) found_zero = 1;
            }
            if (found_zero == 0)    ++count;
            else    found_zero = 0;
        }

        printf("%d\n", count);
    }

    return 0;
}

