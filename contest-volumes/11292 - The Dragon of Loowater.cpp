/*****  The Dragon of Loowater @ 11292  *****/

#include<stdio.h>
#include<stdlib.h>

long n, m, d[20003], k[20003], count, i, j, gold;

int sai(const void *a, const void *b){
	return *(int *)a - *(int *)b;
}

int main(){
	while(scanf("%ld %ld", &n, &m)==2){
		for(i=0; i<n; i++)	scanf("%ld", &d[i]);
		for(i=0; i<m; i++)	scanf("%ld", &k[i]);

		if(n==0 && m==0)	break;
		else if(n>m)	printf("Loowater is doomed!\n");
		else{
			qsort(d, n, sizeof(long), sai);
			qsort(k, m, sizeof(long), sai);
			
			count=0;
			j=0;
			gold=0;
			for(i=0; i<m; i++){
				if(k[i]>=d[j] && count<=n){
					count++;
					gold+=k[i];
					j++;
					if(count==n)	break;
				}
			}
			if(count<n)	printf("Loowater is doomed!\n");
			else	printf("%ld\n", gold);
		}
	}
	return 0;
}



