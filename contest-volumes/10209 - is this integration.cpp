/*****  is this integration @ 10209  *****/

#include<stdio.h>
#include<math.h>
#define pi 2*acos(0)
#define sroot sqrt(3)

double radi,b;
double x,y,z;

int main(){
	while(scanf("%lf",&radi)==1){
		b=radi*radi;

		y=(1.0-sroot+pi/3.0)*b;
		x=4.0*(pi/12.0-1.0+sroot/2.0)*b;
		z=4.0*(1.0-sroot/4.0-pi/6.0)*b;

		printf("%0.3lf %0.3lf %0.3lf\n",y,x,z);
	}
	return 0;
}