/**  Code Number @ 11946  **/

#include <stdio.h>
#include <string.h>

char sms[104];
int i, j, tc, len;

int main(){
	//freopen("input.txt", "r", stdin);
	scanf("%d\n", &tc);
	while(tc--){
		while(gets(sms)){
			if(strlen(sms) == 0)	break;
			else{
				len = strlen(sms);
				for(i = 0; i < len; i++){
					if(sms[i] == '3')	printf("E");
					else if(sms[i] == '0')	printf("O");
					else if(sms[i] == '1')	printf("I");
					else if(sms[i] == '4')	printf("A");
					else if(sms[i] == '9')	printf("P");
					else if(sms[i] == '8')	printf("B");
					else if(sms[i] == '5')	printf("S");
					else if(sms[i] == '7')	printf("T");
					else if(sms[i] == '2')	printf("Z");
					else if(sms[i] == '6')	printf("G");
					else	printf("%c", sms[i]);
				}
				printf("\n");
				memset(sms, 0, sizeof(sms));
			}
		}
		if(tc > 0)	printf("\n");
	}
}