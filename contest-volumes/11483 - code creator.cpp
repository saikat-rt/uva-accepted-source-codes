/*****  code creator @ 11483  *****/

#include<stdio.h>
#include<string.h>

long n, i, len, cse=0, k;
char str[104];

void part_up(){
	printf("#include<string.h>\n");
	printf("#include<stdio.h>\n");
	printf("int main()\n");
	printf("{\n");
}

void part_down(){
	printf("printf(\"\\n\");\n");
	printf("return 0;\n");
	printf("}\n");
}

int main(){
	freopen("in.txt","rt",stdin);

	while( scanf(" %ld\n",&n)==1 && n != 0){
		cse++;
		printf("Case %ld:\n", cse);
		part_up();

		for( i = 0; i < n;i++){
			gets(str);

			len = strlen(str);
			printf("printf(\"");

			for( k = 0; k < len;k++){
				if( str[k] == '"'){
					printf("\\");
					printf("\"");
				}
				else if( str[k] == '\\'){
					printf("\\");
					printf("\\");
				}
				else 
				printf("%c", str[k]);
			}
			printf("\\n\");\n");
		}
		part_down();		
	}
	return 0;
}
