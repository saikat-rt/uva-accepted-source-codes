/* I <3 Pizza @ 12626 */
/* Accepted */

#include <stdio.h>
#include <string.h>

int main() {
    char input[601];
    int tc;

    scanf("%d", &tc);

    while(tc--) {
        scanf(" %s", input);
        int len = strlen(input);
        int freq[27] = {0};
        for(int i = 0; i < len; i++) {
            int p = input[i] - 'A';
            freq[p]++;
        }

        int count = 0;
        while(1) {
            if(freq[0] >= 3 && freq[8] >= 1 && freq[6] >= 1 && freq[12] >= 1 && freq[17] >= 2 && freq[19] >= 1) {
                count++;
                freq[0] -= 3;
                freq[8] -= 1;
                freq[6] -= 1;
                freq[12] -= 1;
                freq[17] -= 2;
                freq[19] -= 1;
            }
            else    break;
        }

        printf("%d\n", count);
    }

    return 0;
}
