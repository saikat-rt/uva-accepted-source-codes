/*
*   Ternary @ 11185
*   [accepted]
*/

#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int n;
    while (1 == scanf("%d", &n) && n >= 0) {

        if (n == 0) {
            printf("0\n");
            continue;
        }

        vector<int> v;
        while (n) {
            v.push_back(n % 3);
            n /= 3;
        }

        int len = v.size();

        for (int i = len - 1; i >= 0; --i)  printf("%d", v[i]);

        printf("\n");
    }

    return 0;
}