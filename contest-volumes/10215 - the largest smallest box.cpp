/*****  the largest smallest box @ 10215  *****/

#include<stdio.h>
#include<math.h>
#define eps 1e-7

double L, W, min, max, vol1, vol2, temp, x;

int main(){
	while(scanf("%lf %lf", &L, &W)==2){
		if(W>L){
			temp=W;
			W=L;
			L=temp;
		}
		min=(L+W-sqrt(L*L+W*W-L*W))/6.0;
        max=(L+W+sqrt(L*L+W*W-L*W))/6.0;
        vol1=(L-2*min)*(W-2*min)*(min);
        vol2=(L-2*max)*(W-2*max)*(max);

        if(vol1>vol2)	x=min;
        else	x=max;

		printf("%0.3lf 0.000 %0.3lf\n", x+eps, (W*0.5)+eps);
	}
	return 0;
}