/*********** pizza cutting @ acm 10079 ********************/

#include<stdio.h>

long double cut;
long double piece;

int main(){
	while(scanf("%Lf",&cut)){
		if(cut<0) break;
		else{
			piece=1+cut*(cut+1)/2;
			printf("%.0Lf\n",piece);
		}
	}
return 0;
}



