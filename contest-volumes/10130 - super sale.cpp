/*****  Super Sale @ 10130  *****/

#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#define SZ 1010

typedef struct{
	double value,weight;
}item;

item items[SZ];
double data[SZ][SZ];

double max(double a,double b){
	return(a>b)?a:b;
}

double knapsack_0_1(item *items,long n,double mw){
	long i,w;
	for(i=0;i<=n;i++)	data[i][0]=0;
	for(w=0;w<=mw;w++)	data[0][w]=0;
	for(i=1;i<=n;i++){
		for(w=0;w<=mw;w++){
			if(items[i].weight>w)
				data[i][w]=data[i-1][w];
			else
				data[i][w]=max(data[i-1][w],data[i-1][w-(long)items[i].weight]+items[i].value);
		}
	}
	return(data[n][(long)mw]);
}

int main(){
	//freopen("10130.txt", "r", stdin);
	long n, i, j, tc, np;
	double mxweight, res;
	scanf("%ld", &tc);
	while(tc--){
		res=0;
		scanf("%ld", &n);
		for(i=1;i<=n;i++)	scanf("%lf %lf",&items[i].value,&items[i].weight);
	
		scanf("%ld", &np);
		for(j=1; j<=np; j++){
			scanf("%lf",&mxweight);
			res+=knapsack_0_1(items,n,mxweight);
		}
		printf("%0.0lf\n",res);
	}
	return 0;
}
