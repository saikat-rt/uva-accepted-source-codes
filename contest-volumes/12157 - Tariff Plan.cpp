/*
    Tariff Plan @ 12157
    [accepted]
*/

#include <stdio.h>

int main() {
    int tc, kase = 0;
    scanf("%d", &tc);

    while(tc--) {
        int n;
        scanf("%d", &n);

        int s;
        int tot_mile = 0, tot_juice = 0;

        for (int i = 0; i < n; ++i) {
            scanf("%d", &s);
            tot_mile += (s / 30 + 1) * 10;
            tot_juice += (s / 60 + 1) * 15;
        }

        if (tot_mile < tot_juice)   printf("Case %d: Mile %d\n", ++kase, tot_mile);
        else if (tot_mile > tot_juice)  printf("Case %d: Juice %d\n", ++kase, tot_juice);
        else    printf("Case %d: Mile Juice %d\n", ++kase, tot_mile);
    }

    return 0;
}
