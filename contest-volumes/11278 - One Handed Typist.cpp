/*****  11278 - One-Handed Typist  *****/

#include<stdio.h>
#include<string.h>


char line[1010];
long len, i, j, k;

int main(){
	while(gets(line)){
		len=strlen(line);
		for(i=0; i<len; i++){
			if(line[i]=='1')	printf("1");
			else if(line[i]=='2')	printf("2");
			else if(line[i]=='3')	printf("3");
			else if(line[i]=='4')	printf("q");
			else if(line[i]=='5')	printf("j");
			else if(line[i]=='6')	printf("l");
			else if(line[i]=='7')	printf("m");
			else if(line[i]=='8')	printf("f");
			else if(line[i]=='9')	printf("p");
			else if(line[i]=='0')	printf("/");
			else if(line[i]=='-')	printf("[");
			else if(line[i]=='=')	printf("]");
			else if(line[i]=='q')	printf("4");
			else if(line[i]=='w')	printf("5");
			else if(line[i]=='e')	printf("6");
			else if(line[i]=='r')	printf(".");
			else if(line[i]=='t')	printf("o");
			else if(line[i]=='y')	printf("r");
			else if(line[i]=='u')	printf("s");
			else if(line[i]=='i')	printf("u");
			else if(line[i]=='o')	printf("y");
			else if(line[i]=='p')	printf("b");
			else if(line[i]==']')	printf("=");
			else if(line[i]=='[')	printf(";");
			else if(line[i]=='a')	printf("7");
			else if(line[i]=='s')	printf("8");
			else if(line[i]=='d')	printf("9");
			else if(line[i]=='f')	printf("a");
			else if(line[i]=='g')	printf("e");
			else if(line[i]=='j')	printf("t");
			else if(line[i]=='k')	printf("d");
			else if(line[i]=='l')	printf("c");
			else if(line[i]==';')	printf("k");
			else if(line[i]=='\'')	printf("-");
			else if(line[i]=='z')	printf("0");
			else if(line[i]=='x')	printf("z");
			else if(line[i]=='c')	printf("x");
			else if(line[i]=='v')	printf(",");
			else if(line[i]=='b')	printf("i");
			else if(line[i]=='m')	printf("w");
			else if(line[i]==',')	printf("v");
			else if(line[i]=='.')	printf("g");
			else if(line[i]=='/')	printf("'");
			else if(line[i]=='$')	printf("Q");
			else if(line[i]=='%')	printf("J");
			else if(line[i]=='^')	printf("L");
			else if(line[i]=='&')	printf("M");
			else if(line[i]=='*')	printf("F");
			else if(line[i]=='(')	printf("P");
			else if(line[i]==')')	printf("?");
			else if(line[i]=='_')	printf("{");
			else if(line[i]=='+')	printf("}");
			else if(line[i]=='Q')	printf("$");
			else if(line[i]=='W')	printf("%%");
			else if(line[i]=='E')	printf("^");
			else if(line[i]=='R')	printf(">");
			else if(line[i]=='T')	printf("O");
			else if(line[i]=='Y')	printf("R");
			else if(line[i]=='U')	printf("S");
			else if(line[i]=='I')	printf("U");
			else if(line[i]=='O')	printf("Y");
			else if(line[i]=='P')	printf("B");
			else if(line[i]=='{')	printf(":");
			else if(line[i]=='}')	printf("+");
			else if(line[i]=='A')	printf("&");
			else if(line[i]=='S')	printf("*");
			else if(line[i]=='D')	printf("(");
			else if(line[i]=='F')	printf("A");
			else if(line[i]=='G')	printf("E");
			else if(line[i]=='J')	printf("T");
			else if(line[i]=='K')	printf("D");
			else if(line[i]=='L')	printf("C");
			else if(line[i]==':')	printf("K");
			else if(line[i]=='"')	printf("_");  
			else if(line[i]=='Z')	printf(")");
			else if(line[i]=='X')	printf("Z");
			else if(line[i]=='C')	printf("X");
			else if(line[i]=='V')	printf("<");
			else if(line[i]=='B')	printf("I");
			else if(line[i]=='M')	printf("W");
			else if(line[i]=='<')	printf("V");
			else if(line[i]=='>')	printf("G");
			else if(line[i]=='?')	printf("\"");
			else	printf("%c", line[i]);
		}
		printf("\n");
	}
	return 0;
}