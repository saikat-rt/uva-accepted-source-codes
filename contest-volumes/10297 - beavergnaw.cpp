/*****  beavergnaw @ 10297  *****/

#include<stdio.h>
#include<math.h>
#define pi 2*acos(0)

double D, v, d, temp;

int main(){
	while(scanf("%lf %lf",&D,&v)==2){
		if(D==0 && v==0)	break;
		else{
			/*v + pi D^3 /12 + pi d^3 / 4 - pi d^3/12 = pi* D^3 /4*/
			temp=pow(D,3)-(6.0*v)/(pi);
			d=pow(temp,(1/3.0));
			printf("%0.3lf\n",d);
		}
	}
	return 0;
}
