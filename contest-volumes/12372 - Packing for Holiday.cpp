/**  Packing for Holiday @ 12372  **/

#include <iostream>
using namespace std;

int main(){

	int t, l, w, h, kase = 0;
	
	cin >> t;
	
	while(t--){
		++kase;
		
		cin >> l >> w >> h;
		
		if(l <= 20 && w <= 20 && h <= 20)	cout << "Case " << kase << ": good" << endl;
		else	cout << "Case " << kase << ": bad" << endl;	
	}
	
	return 0;

}
