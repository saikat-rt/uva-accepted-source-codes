/*
*   Slogan Learning of Princess @ 12592
*   [runtime - 0.012 sec]
*/

#include <stdio.h>
#include <string.h>

int main () {
	int n, q, i, j;
	char line[200];
	char slogan[41][200];
	
	scanf("%d", &n);
	
	j = n * 2;
	i = -1;
	
	while (j--) {
		scanf(" %[^\n]", slogan[++i]);
	}
	
	scanf("%d", &q);
	
	while (q--) {
		scanf(" %[^\n]", line);
		
		for (i = 0; i < n * 2; i += 2) 
			if (!strcmp(line, slogan[i]))   printf("%s\n", slogan[i + 1]);
	}
	
	return 0;
}

