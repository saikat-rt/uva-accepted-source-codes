/*****  reverse and add @ 10018  *****/

#include<stdio.h>
#include<math.h>

unsigned long reverse(unsigned long digit){
	unsigned long val=0;
	for(;digit;digit/=10)
		val = val*10 + digit%10;
	
	return val;
}

unsigned long num,temp;
long test,count;

int main(){
	scanf("%ld",&test);
	while(test--){
		count=0;
		scanf("%lu",&num);
		while(1){
			temp=reverse(num);
			if(num==temp){
				printf("%ld %lu\n",count,temp);
				break;
			}
			
			else{
				num=num+temp;
				count++;
			}
		}
	}
	return 0;
}





