
#include <stdio.h>

char board[12][12];
int n, i, k, test, flag, count;
char c;

int  Minesweeper(int i, int k, int n);

int main(){	
	scanf(" %d", &test);
	while( test-- ) {
		scanf(" %d", &n);
		for( i = 0; i < n; i++)
			for( k = 0; k < n; k++)
				scanf(" %c", &board[i][k]); 
		
		flag = 0;
		
		for( i = 0; i < n; i++)
			for( k = 0; k < n; k++){
				scanf(" %c", &c);
				count = 0;

				if( c == 'x'){
					count = Minesweeper(i, k, n);
					
					if( count == -1)	flag = 1;
					else	board[i][k] = '0' + count;
				}
			}
			
		for( i = 0; i < n; i++){
			for( k = 0; k < n; k++){
				if( flag == 1 && board[i][k] == '*')
					printf("%c", board[i][k]);
				else if( flag != 1 && board[i][k] == '*')
					printf(".");
				else 
					printf("%c", board[i][k]);
			}
			printf("\n");
		}
		if( test ) printf("\n");	
	}
	return 0;
}


int Minesweeper(int i, int k, int n){
	int count = 0;
	
	if( board[i][k] == '*')		return -1;

	if( board[i][k+1] == '*' &&  k+1 < n)					
		count++;
				
	if( board[i-1][k+1] == '*' && (k+1 < n) && (i-1 >= 0))	
		count++;
	
	if( board[i-1][k] == '*' &&  i-1 >= 0)					
		count++;
				
	if( board[i-1][k-1] == '*' &&  (k-1 >= 0) && (i-1 >= 0))	
		count++;
				
	if( board[i][k-1] == '*' &&  k-1 >= 0 )					
		count++;
			
	if( board[i+1][k-1] == '*' &&  (k-1 >= 0) && ( i+1 < n))		
		count++;
			
	if( board[i+1][k] == '*' &&  i+1 < n)						
		count++;
		
	if( board[i+1][k+1] == '*' &&  (k+1 < n) && (i+1 < n))		
		count++;

	return count;
}


