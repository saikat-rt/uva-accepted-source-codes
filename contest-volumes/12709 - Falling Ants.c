/*
*   Falling Ants @ 12709
*   [accepted]
*/

#include <stdio.h>

int main() {
    int l, w, h, vol, best, t;
    while (scanf("%d", &t) == 1 && t) {
        vol = 0, best = 0;
        while (t--) {
            scanf("%d %d %d", &l, &w, &h);
            if (h > best) {
                vol = l * w * h;
                best = h;
            }
            else if (h == best && l * w * h > vol)  vol = l * w * h;
        }
        printf("%d\n", vol);
    }
    return 0;
}
