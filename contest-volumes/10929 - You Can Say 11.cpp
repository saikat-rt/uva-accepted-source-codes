/********  you can say 11  @  10929   *************/

#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>

char s[1001];
long len;
long i;
long even,odd;
long multi;
long num;

int main()
{			
	while(gets(s))
	{ 
		if(s[0]=='0' && s[1]=='\0')	break;   
		else
		{
			len=strlen(s);
			even=0;
			odd=0;
			for(i=0;i<len;i++)
			{
				num=s[i]-'0';
				if(i%2==0)
					even+=num;
				else
					odd+=num;
			}
		}

		if(odd-even>=0)
			multi=odd-even;
		else
			multi=0-(odd-even);

		if(multi%11==0)
			printf("%s is a multiple of 11.\n",s);
		else 
			printf("%s is not a multiple of 11.\n",s);

	}
	return 0;
}

