/**  The Easiest Way @ 11207  **/

#include <stdio.h>
#define max(a, b) a > b ? a : b
#define min(a, b) a < b ? a : b

long n, c, k;
double a, b, s, t, temp;

int main(){
	while(scanf("%ld", &n) == 1 && n != 0){
		c = 0;
		t = 0.0;
		while(n--){
			c++;
			scanf("%lf %lf", &a, &b);
			if(a > b){
				temp = a;
				a = b;
				b = temp;
			}
			s = max(min(a, b / 4.0), a / 2.0);
			if(s > t){
				t = s;
				k = c;
			}			
		}
		printf("%ld\n", k);
	}
	return 0;
}