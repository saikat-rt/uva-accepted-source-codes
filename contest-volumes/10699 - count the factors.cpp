/*****  count the factors @ 10699  *****/

#include<stdio.h>
#include<math.h>

long i,c,count,p[10000];

void prime_factor(long x){
	if(x==1)	printf("%ld : %ld\n",1,0);
	else if(x==2)	printf("%ld : %ld\n",2,1);
	else{
		c=x;
		count=0;
		while(c%2==0){
			p[count++]=2;
			c/=2;
		}
		
		i=3;
		while(i<=(sqrt(c)+1)){
			if(c%i==0){
				p[count++]=i;
				c/=i;
			}
			else	i=i+2;
		}
		
		if(c>1)	p[count]=c;
		
		long temp,max=0,compute=0;
		for(long t=0;t<=count;t++){
			temp=p[t];
			if(temp>max){
				compute++;
				max=temp;
			}
		}
		
		printf("%ld : %ld\n",x,compute);
	}
}

int main(){
	long num;
	while(scanf("%ld",&num)==1){
		if(num==0)	break;
		else
			prime_factor(num);
	}
	return 0;
}
