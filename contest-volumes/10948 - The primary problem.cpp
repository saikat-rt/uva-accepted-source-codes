/*
*   The Primary Problem @ 10948
*   [accepted]
*/

#include <cstdio>

#define M 1000000

bool marked[M];

bool isPrime(int n)
{
    if (n < 2)  return false;
    if (n == 2) return true;
    if (n % 2 == 0) return false;
    return marked[n] == false;
}

void sieve()
{
    for (int i = 3; i * i <= M; i += 2) {
        if (marked[i] == false) {
            for (int j = i * i; j <= M; j += i + i) {
                marked[j] = true;
            }
        }
    }
}

int main()
{
    sieve();

    int n;

    while (scanf("%d", &n) == 1 && n) {
        int flag = 0;
        printf("%d:\n", n);
        for (int i = 2; i <= n / 2; ++i) {
            if (isPrime(i) && isPrime(n - i)) {
                printf("%d+%d\n", i, n - i);
                flag = 1;
                break;
            }
        }

        if (flag == 0)  printf("NO WAY!\n");
    }

    return 0;
}
