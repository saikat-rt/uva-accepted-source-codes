/*****  Leap Year or Not a Leap Year @ 10070  *****/

#include<stdio.h> 
#include<stdlib.h> 
#include<string.h> 
#include<math.h> 
#define MAX 2010

char fir[MAX], res[MAX];

int call_div(char *number,long div,char *result){ 
	int len=strlen(number); 
	int now; 
	long extra; 
	char Res[MAX]; 
	for(now=0,extra=0;now<len;now++){ 
		extra=extra*10 + (number[now]-'0');
		Res[now]=extra / div +'0'; 
		extra%=div; 
	} 
	Res[now]='\0'; 
	for(now=0;Res[now]=='0';now++); 
	strcpy(result, &Res[now]); 
	if(strlen(result)==0) 
		strcpy(result, "0"); 
	return extra; 
} 

int main(){  
	long rem4, rem15, rem100, rem400, rem55, count=0; 
	while(gets(fir)){ 		
		count++;
		if(count>1)	printf("\n");
		rem4=call_div(fir, 4, res); 
		rem15=call_div(fir, 15, res);
		rem100=call_div(fir, 100, res);
		rem400=call_div(fir, 400, res);
		rem55=call_div(fir, 55, res);
		
		if((rem400==0 && rem15==0)||(rem4==0 && rem100!=0 && rem15==0)) {
			printf("This is leap year.\n");
			printf("This is huluculu festival year.\n");
			if(rem55==0)	printf("This is bulukulu festival year.\n");
		}
		else if((rem400==0 && rem15!=0)||(rem4==0 && rem100!=0 && rem15!=0)){
			printf("This is leap year.\n");
			if(rem55==0)	printf("This is bulukulu festival year.\n");
		}
		else if((rem4!=0 && rem15==0)||(rem4==0 && rem100==0 && rem15==0))
			printf("This is huluculu festival year.\n");
		else if((rem4==0 && rem100==0 && rem15!=0)||(rem4!=0 && rem15!=0))
			printf("This is an ordinary year.\n");		
	} 
	return 0; 
}