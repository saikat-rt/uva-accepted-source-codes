/*****  gcd lcm @ 11388  *****/

#include <stdio.h>

int main(){
	long t, gcd, lcm;
	scanf("%ld", &t);
	for(long i = 0; i < t; i++){
		scanf("%ld %ld", &gcd, &lcm);
		
		if(lcm % gcd == 0)	printf("%ld %ld\n", gcd, lcm);
		else	printf("-1\n");
	}
	return 0;
}
