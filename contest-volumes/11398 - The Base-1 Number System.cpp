/* The Base-1 Number System @ 11398 */

#include <stdio.h>

char z;
long j, count, flag, output;

int main(){
	count = 0, output = 0;

	while(scanf("%c", &z) == 1){
		if(z == '~')	break;
		else if(z == '0')	count++;
		else if(z == ' '){
			if(count == 1)	flag = 1;
			else if(count == 2)	flag = 0;
			else if(count > 2){
				count -= 2;
				for(j = 0; j < count; j++)
					output = output * 2 + flag;
			}
			count = 0;
		}
		else if(z == '#'){
			printf("%ld\n", output);
			output = 0, count = 0;
		}
	}
	return 0;
}