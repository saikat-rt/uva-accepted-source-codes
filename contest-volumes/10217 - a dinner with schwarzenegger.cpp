/*****  a dinner with schwarzenegger @ 10217  *****/

#include <stdio.h>
#include <math.h>

long day, q;
double queue;

int main(){
	while(scanf("%ld", &day) == 1){
		queue = (sqrt(1 + 4 * day) - 1) / 2.0;
		q = floor(queue) + 1;
		printf("%0.2lf %ld\n", queue, q);
	}
	return 0;
}
