/*****  satellites @ 10221  *****/

#include<stdio.h>
#include<math.h>
#include<string.h>
#define R 6440.0
#define PI 2*acos(0)

double arc, chord, r, a, k;
char str[5];

int main(){
	while(scanf("%lf %lf %s",&r,&a,str)==3){
		if(a>180)	a=360-a;
		else a;

		if(!strcmp(str,"min"))	
			k=((a*PI)/60.0)/180.0;
		else 
			k=(a*PI)/180.0;
		
		arc=k*(R+r);
		chord=2.0*(R+r)*sin(k/2.0);
		printf("%0.6lf %0.6lf\n",arc,chord);
	}
	return 0;
}


