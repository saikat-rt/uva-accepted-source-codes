/*****  R U Kidding Mr. Feynman @ 10509  *****/

#include<stdio.h>

double a, n, dx;

int main(){
	while(scanf("%lf", &n) == 1) {
    	if(n == 0)	break;
    	
    	a = 1;
    	while(1){ 
      		if(a * a * a >= n)	break;
      		a++;
    	}    

    	if(a * a * a == n)	{ printf("%.4lf\n",a); continue; }
    	a--;
    	dx = (n - a * a * a) / ( 3 * a * a);
    	printf("%.4lf\n", a + dx);
  	}
  	return 0;
}
