/*
*   Fat and Orial @ 12725
*   [accepted]
*/

#include <cstdio>

int main() {

    int tc, kase = 0;

    scanf("%d", &tc);
    while (tc--) {
        double a, b, m, n;
        scanf("%lf %lf %lf %lf", &n, &m, &a, &b);

        double total_req_point = m * (a + b);
        double curr_total_point = n * a;
        double curr_req_grade = (total_req_point - curr_total_point) / b;

        if (curr_req_grade >= 0.0 && curr_req_grade <= 10.0)
            printf("Case #%d: %.2lf\n", ++kase, curr_req_grade);
        else
            printf("Case #%d: Impossible\n", ++kase);
    }

    return 0;
}


