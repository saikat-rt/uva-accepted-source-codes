/*****  Irreducible Basic Fractions @ 10179  *****/

#include<stdio.h>
#include<math.h>
#define SZ 31630 

long flag[SZ+1];
long prime[SZ];

void sieve(){
	long i,j,r,c=0;
	prime[c++]=2;
	flag[0]=1, flag[1]=1;
	flag[2]=0, flag[3]=0;
	for(i=4;i<=SZ;i+=2)
		flag[i]=1;

	for(i=3;i<=SZ;i+=2){
		if(flag[i]==0){
			prime[c++]=i;
			if(SZ/i>=i){
				r=i*2;
				for(j=i*i;j<=SZ;j+=r)
					flag[j]=1;
			}
		}
	}
}

int main(){
   long phi,n,i,N,j;
   sieve();
   while(scanf("%ld",&N)==1 && N!=0){
      if(N==1)	printf("1\n");
	  else{
		  j=(long)sqrt(N);
		  n=N;
		  phi=N;
		  i=0;
		  while(prime[i]<=j){
			  if(N%prime[i]==0){
				  phi=(phi/prime[i])*(prime[i]-1);
				  while(n%prime[i]==0){	
					  n/=prime[i];
				  } 
			  }
			  if(n==1)	break;
			  i++;
		  }
		  if(n>1)	phi=(phi/n)*(n-1); 	
		  printf("%ld\n",phi);
	  }
   }
   return 0;
}
