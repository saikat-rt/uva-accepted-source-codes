/*****  Mirror Clock @ 11650  *****/

#include <stdio.h>

int tc, h, m, mh, mm;
char c;

int main(){
	scanf("%d", &tc);
	while(tc--){
		scanf("%d%c%d", &h, &c, &m);

		if(h==12 && m==0)	mh=12, mm=0;
		else if(h==12 && m!=0)	mh=h-1, mm=60-m;
		else if(h!=12 && m==0)	mh=12-h, mm=0;
		else if(h==11 && m!=0)	mh=h+1, mm=60-m;
		else if(h<11 && m==0)	mh=12-h, mm=0;
		else if(h<11 && m!=0)	mh=12-h-1, mm=60-m;

		if(mh<10 && mm<10)	printf("0%d%c0%d\n", mh, c, mm);
		else if(mh>9 && mm>9)	printf("%d%c%d\n", mh, c, mm);
		else if(mh<10)	printf("0%d%c%d\n", mh, c, mm);
		else if(mm<10)	printf("%d%c0%d\n", mh, c, mm);
			
	}
	return 0;
}
