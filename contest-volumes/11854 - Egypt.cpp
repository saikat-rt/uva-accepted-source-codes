/****  Egypt @ 11854  ****/


#include <stdio.h>
#include <math.h>

long a, b, c;

int main(){
	while(scanf("%ld %ld %ld", &a, &b, &c) == 3){
		if(a == 0 && b == 0 && c == 0)	break;
		
		else{
			if(a >= b && a >= c){
				a = a * a;
				b = b * b;
				c = c * c;
				if(a == b + c)	printf("right\n");
				else	printf("wrong\n");
			}
			else if(b >= a && b >= c){
				a = a * a;
				b = b * b;
				c = c * c;
				if(b == a + c)	printf("right\n");
				else	printf("wrong\n");
			}
			else if(c >= a && c >= b){
				a = a * a;
				b = b * b;
				c = c * c;
				if(c == b + a)	printf("right\n");
				else	printf("wrong\n");
			}
		}
	}
	return 0;	
}
