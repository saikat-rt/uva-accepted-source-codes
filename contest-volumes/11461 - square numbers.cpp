/*
    Square Numbers @ 11461
    [accepted]
*/

#include <cstdio>
#define MAX 318

int squares[318];

int main()
{
    int i, j, sq = 0, a, b, count;

    for (i = 1; i < MAX; i++)   squares[i] = i * i;

    while(scanf("%d %d", &a, &b) == 2 && a != 0 && b != 0)
    {
        count = 0;
        for (i = 0; squares[i] < a; i++);
        for (; squares[i] <= b; count++, i++);

        printf("%d\n", count);
    }

    return 0;
}