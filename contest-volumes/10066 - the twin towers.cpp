/*****  the twin towers @ 10066  *****/

#include<stdio.h>

long n1, n2, t1[104], t2[104], cse=0;
long c[104][104], b[104][104];

void LCS_Length(long x[], long y[], long m, long n){
	long i, j;
	
	for(i=0; i<=m; i++)	c[i][0]=0;
	for(j=0; j<=n; j++)	c[0][j]=0;

	for(i=1; i<=m; i++){
		for(j=1; j<=n; j++){
			if(x[i-1]==y[j-1]){
				c[i][j]=c[i-1][j-1]+1;
				b[i][j]=1;
			}
			else if(c[i-1][j]>=c[i][j-1]){
				c[i][j]=c[i-1][j];
				b[i][j]=2;
			}
			else{
				c[i][j]=c[i][j-1];
				b[i][j]=3;
			}
		}
	}
	printf("Number of Tiles : %ld\n", c[m][n]);
}

int main(){
	long i;
	while(scanf("%ld %ld", &n1, &n2)==2 && n1!=0 && n2!=0){
		cse++;
		
		for(i=0; i<n1; i++)	scanf("%ld", &t1[i]);
		for(i=0; i<n2; i++)	scanf("%ld", &t2[i]);

		printf("Twin Towers #%ld\n", cse);
		LCS_Length(t1, t2, n1, n2);
		printf("\n");
	}
	return 0;
}