/**  Do Your Own Homework! @ 11917  **/

#include <stdio.h>
#include <string.h>

int tc, n, d, day[100], t, kase, i, flag;
char sub[100][23], due[23];

int main(){
	scanf("%d", &tc);
	kase = 0;
	while(tc--){
		kase++;
		scanf("%d", &n);
		for(i = 0; i < n; i++)
			scanf(" %s %d", sub[i], &day[i]);
			
		scanf("%d", &d);
		scanf(" %s", due);
		
		flag = 0;
		for(i = 0; i < n; i++){
			if(!strcmp(due, sub[i])){
				flag = 1;
				t = day[i];
				break;
			}
		}
		
		if(flag == 0)
			printf("Case %d: Do your own homework!\n", kase);
		else{
			if(d >= t)	printf("Case %d: Yesss\n", kase);
			else if(d+5 >= t)	printf("Case %d: Late\n", kase);
			else	printf("Case %d: Do your own homework!\n", kase);
		}
	}
	return 0;
}

