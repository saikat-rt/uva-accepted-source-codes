/*****  Having Fun with Matrices @ 11360  *****/

#include<stdio.h>
#include<string.h>

int n, m, tc, i, j, a, b, temp, cse=0;
int mat[10][10], data[10][10];
char s[13];

int main(){
	scanf("%d", &tc);
	while(tc--){
		cse++;
		scanf("%d", &n);
		for(i=1; i<=n; i++)
			for(j=1; j<=n; j++)
				scanf("%1d", &mat[i][j]);
		
		scanf("%d", &m);
		while(m--){
			scanf("%s", s);
			if(strcmp(s,"transpose")==0){
				for(i=1; i<=n; i++)
					for(j=1; j<=n; j++)
						data[j][i]=mat[i][j];
			
			for(i=1; i<=n; i++)
				for(j=1; j<=n; j++)
					mat[i][j]=data[i][j];
			}
			else if(strcmp(s,"inc")==0){
				for(i=1; i<=n; i++)
					for(j=1; j<=n; j++){
						mat[i][j]=mat[i][j]+1;
						if(mat[i][j]==10)	mat[i][j]=0;
					}
			}
			else if(strcmp(s,"dec")==0){
				for(i=1; i<=n; i++)
					for(j=1; j<=n; j++){
						mat[i][j]=mat[i][j]-1;
						if(mat[i][j]==-1)	mat[i][j]=9;
					}
			}
			else if(strcmp(s,"row")==0){
				scanf("%d %d", &a, &b);
				for(j=1; j<=n; j++){
					temp=mat[a][j];
					mat[a][j]=mat[b][j];
					mat[b][j]=temp;
				}
			}
			else if(strcmp(s,"col")==0){
				scanf("%d %d", &a, &b);
				for(i=1; i<=n; i++){
					temp=mat[i][a];
					mat[i][a]=mat[i][b];
					mat[i][b]=temp;
				}
			}
		}
		
		printf("Case #%d\n", cse);
		for(i=1; i<=n; i++){
			for(j=1; j<=n; j++)
				printf("%d", mat[i][j]);
			printf("\n");
		}
		printf("\n");
		
	}
	return 0;
}

