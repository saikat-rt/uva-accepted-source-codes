/*****  Mischievous Children @ 10338  *****/

#include<stdio.h>
#include<string.h>

long long dataset, cse=0;
long long fact;
long long alphabet[27];
char s[23];
long long i, j, k, len;

long long factorial(long long n){
	long long x;
	if(n==0)	return 1;
	else	x=factorial(n-1);
	return x*n;
}

int main(){
	while(scanf("%lld", &dataset)==1){
		while(dataset--){
			cse++;
			for(i=0; i<27; i++)	alphabet[i]=0;
			scanf(" %[^\n]", s);
			len=strlen(s);
			for(j=0; j<len; j++){
				if(s[j]>='A' && s[j]<='Z')	alphabet[s[j]-'A']++;
				else if(s[j]>='a' && s[j]<='z')	alphabet[s[j]-'a']++;
			}
			fact=factorial(len);
			for(k=0; k<27; k++){
				if(alphabet[k]>=2)
					fact=fact/factorial(alphabet[k]);
			}
			printf("Data set %lld: %lld\n", cse, fact);
		}
	}
	return 0;
}

