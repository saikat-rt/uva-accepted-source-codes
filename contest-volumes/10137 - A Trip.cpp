/*****  A Trip @ 10137  *****/

#include <stdio.h>

long n, i, s;
double ss[1001], sum, av;

int main(){
    while(scanf("%ld", &n) == 1 && n != 0){
        sum = s = 0;
        for(i = 0; i < n; i++){
            scanf("%lf", &ss[i]);
            ss[i] *= 100;
            sum += ss[i];
        }
        av = sum / (double)n;
        s = sum = 0;
        for(i = 0; i < n; i++){
            if(av > ss[i])  s += long(av - ss[i]);
            else if(av < ss[i]) sum += long(ss[i] - av);
        }
        if(sum < s) sum = s;
        printf("$%0.2lf\n", double(sum) / 100);
    }
    return 0;
}
