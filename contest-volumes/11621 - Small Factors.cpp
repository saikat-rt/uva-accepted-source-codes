/*
*   Small Factors @ 11621
*	[accepted | runtime : 0.019]
*/

#include <cstdio>
#include <algorithm>

using namespace std;

int main()
{
    int next[5842];
    int m;
    int pos2, pos3;

    pos2 = pos3 = 0;
    next[0] = 1;

    for (int i = 1; i < 5842; i++)
    {
        next[i] = min(2 * next[pos2], 3 * next[pos3]);

        if(next[i] == 2 * next[pos2])   pos2++;
        if(next[i] == 3 * next[pos3])   pos3++;
    }

    // printf, scanf are faster than cin, cout

    while(scanf("%d", &m) == 1)
    {
        if(m == 0)  break;

        for (int i = 0; ; ++i)
        {
            if (next[i] >= m)
            {
                printf("%d\n", next[i]);
                break;
            }
        }
    }

    return 0;
}
