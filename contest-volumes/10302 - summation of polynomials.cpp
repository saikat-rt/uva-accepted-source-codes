/*****  summation of polynomials @ 10302  *****/

#include<stdio.h>

long num;
long long temp, sum;

int main(){
	while(scanf("%ld",&num)==1){
		if(num%2==0)
			temp=(num/2.0)*(num+1);
		else
			temp=num*(num+1)/2;
		
		sum=temp*temp;
		printf("%lld\n",sum);
	}
	return 0;
}

