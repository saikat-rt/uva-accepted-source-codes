/*****  optimal parking @ 11364  *****/

#include <stdio.h>
#include <stdlib.h>

int tc, store, pos[100];
int i, min_dist;

int sai(const void *a, const void *b){
	return *(int *)a-*(int *)b;
}

int main(){
	scanf("%d", &tc);
	while(tc--){
		scanf("%d", &store);
		for(i = 0; i < store; i++)
			scanf("%d", &pos[i]);
		qsort(pos, store, sizeof(int), sai);
		min_dist = 2 * (pos[store - 1] - pos[0]);
		printf("%d\n", min_dist);
	}
	return 0;
}
