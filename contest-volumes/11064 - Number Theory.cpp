/*****  number theory @ 11064  *****/

#include<stdio.h>
#include<math.h>
#define SZ 47500

bool flag[SZ+1];
long prime[SZ];

void sieve(){
	long i,j,r,c=0;
	prime[c++]=2;
	flag[0]=1, flag[1]=1;
	flag[2]=0, flag[3]=0;
	for(i=4;i<=SZ;i+=2)
		flag[i]=1;

	for(i=3;i<=SZ;i+=2){
		if(flag[i]==0){
			prime[c++]=i;
			if(SZ/i>=i){
				r=i*2;
				for(j=i*i;j<=SZ;j+=r)
					flag[j]=1;
			}
		}
	}
}

long divisor(long n) {
	long root, k, i;
	root=(long)sqrt(n); 
	k=0; 
    for(i=1;i<=root;i++) 
		if(n%i==0)	
			k+=2; 
    
	if(n%root==0)
		if(n/root==root) 
			k--; 

    return k; 
}

int main(){
   long phi,n,i,N,j,temp;
   sieve();
   while(scanf("%ld",&N)==1){
      if(N==1)	phi=1;
	  else{
		  j=(long)sqrt(N);
		  n=N;
		  phi=N;
		  i=0;
		  while(prime[i]<=j){
			  if(N%prime[i]==0){
				  phi=(phi/prime[i])*(prime[i]-1);
				  while(n%prime[i]==0){	
					  n/=prime[i];
				  } 
			  }
			  if(n==1)	break;
			  i++;
		  }
		  if(n>1)	phi=(phi/n)*(n-1); 	
	  }
	  temp=divisor(N);
	  printf("%ld\n", N-phi-temp+1);
   }
   return 0;
}



