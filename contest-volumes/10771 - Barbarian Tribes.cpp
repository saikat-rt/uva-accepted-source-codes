/*****  Barbarian Tribes @ 10771  *****/

#include<stdio.h>

int main() {
  int n, m, k;
  while(scanf("%d %d %d", &n, &m, &k) == 3) {
    if(!n && !m && !k) break;
    if(m % 2 == 1)	printf("Keka\n");
    else	printf("Gared\n");
  }
  return 0;
}
