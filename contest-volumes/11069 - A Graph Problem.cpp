/*****  A Graph Problem @ 11069  *****/

#include<stdio.h>

long N[82];


void Table(){
	long i;
	N[1]=1;
	N[2]=N[3]=2;
	for(i=4; i<77; i++)
		N[i]=N[i-2]+N[i-3];
}
int main(){
	long n;
	Table();
	while(scanf("%ld", &n)==1){
		printf("%ld\n", N[n]);
	}
	return 0;
}