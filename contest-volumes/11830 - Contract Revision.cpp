/*****  Contract Revision @ 11830  *****/

#include <stdio.h>
#include <string.h>

char c, str[105];
int i, len, flag;

int main(){
    while(scanf(" %c %s", &c, str) == 2){
        if(c == '0' && !strcmp(str, "0")) break;
        len = strlen(str);

        flag = 0;

        for(i = 0; i < len; i++)
            if(str[i] == c)
                str[i] = '0';

        for(i = 0; i < len; i++)
            if(str[i] != '0'){
                printf("%c", str[i]);
                flag = 1;
            }

        if(flag == 0)   printf("0\n");
        else    printf("\n");
    }
    return 0;
}
