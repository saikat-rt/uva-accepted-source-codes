/*****  behold my quadrangle @ 11455  *****/

#include<stdio.h>

long tc, a, b, c, d; 
long line[4];

void sort(long a[]){
	long i, j, temp;
	for(i=0; i<4; i++){
		for(j=0; j<3-i; j++){
			if(a[j]>a[j+1]){
				temp=a[j];
				a[j]=a[j+1];
				a[j+1]=temp;
			}
		}
	}
	return ;
}
int main(){
	scanf("%ld", &tc);
	while(tc--){
		for(long t=0; t<4; t++)
			scanf("%ld", &line[t]);
		
		sort(line);
		a=line[0]; b=line[1]; c=line[2]; d=line[3]; 
	
		if(a==b && b==c && c==d)	
			printf("square\n");

		else if(a==b && c==d)	
			printf("rectangle\n");

		else if(a+b+c>=d)	
			printf("quadrangle\n");

		else	
			printf("banana\n");
	}
	return 0;
}
