/*****  Ant on a Chess Board @ 10161  *****/

#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>

using namespace std;

long n, sq, dist, x, y, temp;

int main(){
	while(scanf("%ld", &n) == 1 && n != 0){
		sq = floor(sqrt(n));
		dist = n - sq * sq;
		if(dist == 0)	x = 1, y = sq;
		else if(dist <= sq + 1)	x = dist, y = sq + 1;
		else	x = sq + 1, y = 2 * sq + 2 - dist;

		if(sq % 2 == 0)	swap(x, y);

		printf("%ld %ld\n", x, y);
	}
	return 0;
}
