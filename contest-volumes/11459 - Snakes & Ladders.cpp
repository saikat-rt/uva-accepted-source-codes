/*****  Snakes & Ladders @ 11459  *****/

#include <stdio.h>

long tc, player, ladder, roll;
long position[1000001], from[1000001], to[1000001];
long i, j, k, die, flag;

int main(){
	scanf("%ld", &tc);
	while(tc--){
		scanf("%ld %ld %ld", &player, &ladder, &roll);
		for(i = 0; i < ladder; i++)	scanf("%ld %ld", &from[i], &to[i]);
		for(i = 0; i < player; i++)	position[i] = 1;
		
		flag = 0;
		for(k = 0; k < roll; k++){
			scanf("%ld", &die);
			if(flag == 1)	continue;
			position[k%player] += die;
			for(i = 0; i < ladder; i++)
				if(position[k%player] == from[i])
					position[k%player] = to[i];
			
			if(position[k%player] >= 100){
				position[k%player] = 100;
				flag = 1;
			}
		}
		for(k = 0; k < player; k++)
			printf("Position of player %ld is %ld.\n", k+1, position[k]);
	}
	return 0;
}

