/*****  hashmat the brave warrior @ 10055  *****/

#include <stdio.h>

long long hash, opp, diff;

int main(){
	while(scanf("%lld %lld", &hash, &opp) == 2){
		if(opp > hash){
			diff = opp - hash;
			printf("%lld\n", diff);
		}
		
		else{
			diff = hash - opp;
			printf("%lld\n", diff);
		}
	}
	return 0;
}
