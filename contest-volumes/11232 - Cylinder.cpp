/*
    Cylinder @ 11232
    [accepted]
*/

#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;

#define PI acos(-1.0)

int main()
{
    int w, h;
    while(scanf("%d %d", &w, &h) == 2 && w && h)
    {
        double best = 0;

        double r = w / (2 * PI);
        if (best < (PI * r * r * (h - 2 * r)))
        {
            best = PI * r * r * (h - 2 * r);
        }

        double r_max = min(w / 2.0, h / (2 * PI + 2));
        if (best < PI * r_max * r_max * w)
        {
            best = PI * r_max * r_max * w;
        }

        printf("%0.3lf\n", best);
    }

    return 0;
}
