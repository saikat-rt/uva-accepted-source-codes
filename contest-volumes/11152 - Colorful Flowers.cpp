/*********  colourful flowers  @  11152  *********/

#include<stdio.h>
#include<math.h>
#define PI 2*acos(0)

double a, b, c, rose, vio, sun;
double tri_area, peri, cir_area, cir_radi, in_area, in_radi;

int main(){	
	while(scanf("%lf %lf %lf",&a,&b,&c)==3){
		peri=(a+b+c)/2.0;
		tri_area=sqrt(peri*(peri-a)*(peri-b)*(peri-c));

		in_radi=tri_area/peri;
		in_area=PI*in_radi*in_radi;

		cir_radi=(a*b*c)/(4.0*tri_area);
		cir_area=PI*cir_radi*cir_radi;

		rose=in_area;
		vio=tri_area-in_area;
		sun=cir_area-tri_area;

		printf("%0.4lf %0.4lf %0.4lf\n",sun,vio,rose);
	}
	return 0;
}


