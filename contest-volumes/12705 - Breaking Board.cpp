/*
*   Breaking Board @ 12705
*   [accepted]
*/

#include <cstring>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int tc;
    int value[] = {2,3,3,4,4,4,5,5,5,5,6,6,6,6,6,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,10,10,10,11,11,12};
    int count_table[37];

    scanf("%d\n", &tc);

    while (tc--) {
        char line[101];
        gets(line);

        int len = strlen(line);

        for(int i = 0; i < 36; i++) count_table[i] = 0;

        for (int i = 0; i < len; ++i) {
            if (line[i] >= '0' && line[i] <= '9')   count_table[line[i]-48]++;
            else if (line[i] >= 'A' && line[i] <= 'Z')  count_table[line[i]-65+10]++;
        }

        vector<int> myvector (count_table, count_table+36);

        sort(myvector.begin(), myvector.end());

        int mincost = 0;
        int count = -1;

        for (int i = 35; i >= 0; --i) {
            if (myvector[i] != 0)   mincost += myvector[i] * value[++count];
        }

        printf("%d\n", mincost);
    }

    return 0;
}
