/*****  square sums @ 11470  *****/

#include<stdio.h>

long n, mat[13][13], i, j, k, cse=0, sum;

int main(){
	while(scanf("%ld", &n)==1 && n!=0){
		cse++;
		for(i=0; i<n; i++)
			for(j=0; j<n; j++)
				scanf("%ld", &mat[i][j]);

		printf("Case %ld:", cse);
		for(i=0; i<n/2; i++){
			sum=0;
			for(j=i; j<=n-1-i; j++){
				sum+=mat[i][j];
				sum+=mat[n-1-i][j];
			}
			for(k=i+1; k<n-1-i; k++){
				sum+=mat[k][i];
				sum+=mat[k][n-1-i];
			}
			printf(" %ld", sum);
		}
		if(n & 1)	printf(" %ld", mat[n/2][n/2]);
		printf("\n");
	}
	return 0;
}
