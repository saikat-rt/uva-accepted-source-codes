/** Flowers Flourish from France @ 12243 **/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

char s[1111], check;
int i, k, len;
bool flag;

int main(){
	while(scanf(" %[^\n]s", s)){
		if(!strcmp(s, "*"))	break;
		
		len = strlen(s);
		check = tolower(s[0]);
		flag = true;
		i = 1;
		
		while(i < len){
			if(s[i] == ' '){
				if(tolower(s[i+1]) == check){
					flag = true;
					i += 2;
				}
				else{
					flag = false;
					break;
				}
			}
			else	++i;
		}
		
		if(flag)	printf("Y\n");
		else	printf("N\n");
	}
	return 0;
}
