/*****  jolly jumpers @ 10038  *****/

#include<stdio.h>

int n, number[3007], dif[3007];
int i, temp, flag;

int main(){
	while(scanf("%d", &n)==1){
		for(i=0; i<n; i++){
			scanf("%d", &number[i]);
			dif[i]=0;
		}
		
		for(i=0; i<n-1; i++){
			temp=number[i]-number[i+1];
			if(temp<0)	temp=-1*temp;
			if(temp<n)	dif[temp]=1;
		}

		flag=1;
		for(i=1; i<n; i++){
			if(dif[i]!=1){
				flag=0;
				break;
			}
		}

		if(flag==1)	printf("Jolly\n");
		else	printf("Not jolly\n");
	}
	return 0;
}
