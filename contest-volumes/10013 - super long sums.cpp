/*****  super long sums @ 10013  *****/

#include<stdio.h>

long sum[1000010], total[1000010];
long tc, a, b, digit, temp, i, j, k;

int main(){
	scanf("%ld", &tc);

	while(tc--){
		scanf("%ld", &digit);

		for(i=0; i<digit; i++){
			scanf("%ld %ld", &a, &b);
			sum[i]=a+b;
		}

		for(j=digit-1; j>=1; j--){
			if(sum[j]>=10){
				sum[j-1]=sum[j-1]+1;
				sum[j]=sum[j]%10;
			}
		}

		for(k=0; k<digit; k++){
			printf("%ld", sum[k]);
		}
		printf("\n");

		if(tc>=1)	printf("\n");
	}
	return 0;
}