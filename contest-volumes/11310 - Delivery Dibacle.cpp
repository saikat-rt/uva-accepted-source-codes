/* Delivery Dibacle 2 @ 11310 */

#include <stdio.h>

long long way[41];
long n, tc;

void preCalc(){
	way[0] = 1;
	way[1] = 1;
	way[2] = 5;
	
	long i;
	
	for(i = 3; i <= 40; i++)
		way[i] = way[i - 1] + 4 * way[i - 2] + 2 * way[i - 3];	
}

int main(){
	preCalc();
	scanf("%ld", &tc);
	while(tc--){
		scanf("%ld", &n);
		printf("%lld\n", way[n]);
	}
	return 0;
}
