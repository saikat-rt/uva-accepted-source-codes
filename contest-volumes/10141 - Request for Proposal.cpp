/*****  Request for Proposal @ 10141  *****/


#include <stdio.h>
#include <string.h>


int main(){
	//freopen("10141.txt", "r", stdin);
	long n, p, t, kase=0, i, j, count=0, tag;
	double v, val;
	char car[140], ignore[140], out[140];
	while(scanf("%ld %ld", &n, &p)==2 && n && p){
		kase++;
		count++;
		val=0.0;
		tag=0;
		for(i=0; i<n; i++)	scanf(" %[^\n]", ignore);
		for(j=0; j<p; j++){
			scanf(" %[^\n]", car);
			scanf("%lf %ld", &v, &t);
			for(i=0; i<t; i++)	scanf(" %[^\n]", ignore);
			if(tag<t){
				val=v;
				tag=t;
				strcpy(out, car);
			}
			else if(tag==t && v<val){
				val=v;
				strcpy(out, car);
			}
		}
		if(count>1)	printf("\n");
		printf("RFP #%ld\n", kase);
		printf("%s\n", out);
	}
	return 0;
}