/*****  Polynomial Coefficients @ 10105  *****/

#include <stdio.h>


long Fac(long f){
	if(f==1)	return 1;
	
	long i, r=1;
	for(i=2; i<=f; i++)	r*=i;
	return r;
}

int main(){
	long n, k, i, j, m, sum;
	while(scanf("%ld %ld", &n, &k)==2){
		sum=1;
		for(i=0; i<k; i++){
			scanf("%ld", &j);
			sum*=Fac(j);
		}
		m=Fac(n);
		
		printf("%ld\n", m/sum); 
	}
	return 0;
}