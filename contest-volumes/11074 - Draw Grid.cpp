/*****  draw grid @ 11074  *****/

#include<stdio.h>

int s, t, n, cse;

int main(){
	//freopen("out.txt","w",stdout);
	cse=0;
	while(scanf("%d %d %d", &s, &t, &n)==3){
		if(s==0 && t==0 && n==0)	break;
		else{
			printf("Case %d:\n", ++cse);
			long temp=t*(n+1)+s*n;
			long i=0;
			while(i<n+1){
				for(long j=0; j<t; j++){
					long star=0;
					while(star<temp){
						printf("*");
						star++;
						if(star==temp)	break;
					}
					printf("\n");
				}
				i++;
				if(i==n+1)	break;
				else{
					long a=0;
					while(a<s){
						long w=0;
						while(w<n+1){
							long c=0;
							while(c<t){
								printf("*");
								c++;
								if(c==t)	break;
							}
							w++;
							if(w==n+1)	break;
							else{
								long d=0;
								while(d<s){
									printf(".");
									d++;
									if(d==s)	break;
								}
							}
						}
						printf("\n");
						a++;
						if(a==s)	break;	
					}
				}
			}
		}
		printf("\n");
	}
	return 0;
}