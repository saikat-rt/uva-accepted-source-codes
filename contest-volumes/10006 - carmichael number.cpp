/******** carmichael number @ 10006 ********/

#include <stdio.h>

long car[19] = {561, 1105, 1729, 2465, 2821, 6601, 8911, 10585, 15841, 29341, 41041, 46657, 52633, 62745, 63973};
long ist, i, flag;

int main() {
    while (scanf("%ld", &ist) == 1) {
        flag = 0;
        if (ist == 0) break;
       
        for (i = 0;i < 15; i++){
            if (car[i] == ist) {
                printf("The number %ld is a Carmichael number.\n", ist);
                flag = 1;
                break;
            }
	}
       
        if(flag == 0)	printf("%ld is normal.\n",ist);
    }
    return 0;
}
