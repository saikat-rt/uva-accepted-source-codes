/*****  11547 - Automatic Answer  *****/

#include<stdio.h>

long n, p, tc;

int main(){
	scanf("%ld", &tc);
	while(tc--){
		scanf("%ld", &n);
		
		p = (((n * 567) / 9 + 7492) * 235) / 47 - 498;
		p = p % 100;
		p = p / 10;
		if(p < 0)	p = -1 * p;
		printf("%ld\n", p);
	}
	return 0;
}
