/*****  rare easy problem @ 10633 *****/

#include <stdio.h>

unsigned long long n;
unsigned long long p;

int main(){
	while(scanf("%llu", &p) == 1){
		if(p == 0)	break;
		else{
			if(p % 9 == 0){
				n=(10 * p) / 9;
				printf("%llu %llu\n", n - 1, n);
			}

			else{
				n=(10 * p / 9);
				printf("%llu\n", n);
			}
		}
	}
	return 0;
}


