/**  Really Strange @ 10519  **/

#include <stdio.h>
#include <string.h>

//Reverse the string 'string'.
void reverse_string(char *string)
{
	int i, length;
	
	for(i = 0, length = strlen(string); i < length / 2; i++)
	{
		string[i] ^= string[length - i - 1] ^= string[i] ^= string[length - i - 1];
	}
	
	return;
}

//'result' = 'num1' * 'num2'.
//'result' is created in reverse order. So, we reverse it again by calling 'reverse_string()'
//at the last portion of this function.
void character_multiplication(char *num1, char *num2, char *result)
{
	int i, j, k, index, carry, temp, length1, length2;
	
	//If any number is 0, then the result is 0.
	if (strcmp(num1, "0") == 0 || strcmp(num2, "0") == 0)
	{
		sprintf(result, "0");
		return;
	}
	
	length1 = strlen(num1);
	length2 = strlen(num2);
	
	//Assign all elements of 'result' of length 'length1 + length2' with '0'.
	j = length1 + length2;
	for(i = 0; i < j; i++) result[i] = '0';
	
	//Take one digit from 'num2' one by one.
	for(i = length2 - 1, index = 0; i >= 0; i--, index++)
	{
		if (num2[i] == '0') continue;
		
		//Multiply each digit of 'num1' by the digit taken from 'num2'.
		for(j = length1 - 1, k = index, carry = 0; j >= 0; j--, k++)
		{
			temp = (num1[j] - '0') * (num2[i] - '0') + (result[k] - '0') + carry;
			carry = temp / 10;
			result[k] = temp % 10 + '0';
		}
		
		if (carry != 0) result[k++] = carry % 10 + '0';
	}
	
	if (k == 0) result[k++] = '0';
	result[k] = NULL;
	
	reverse_string(result);
	
	return;
}

//We are assuming 'num1' > 'num2' (if not we swap them) & performing 'num1' - 'num2'.
//If 'num1' < 'num2', then we add a '-' at the 1st of 'result' (i.e., the result is negative).
void character_subtraction(char *num1, char *num2, char *result)
{
	int  i, j, k;
	//char *swap;
	
	strcpy(result, num1);
	
	for(i = strlen(result) - 1, j = strlen(num2) - 1; j >= 0; i--, j--)
	{
		if (result[i] >= num2[j])
		{
			result[i] = result[i] - num2[j] + '0';
			continue;
		}
		
		result[i] = (result[i] + 10) - num2[j] + '0';
		for(k = i - 1; k > 0 && result[k] == '0'; k--) result[k] = '9';
		result[k] -= 1;
	}
	
	//Ignoring all the leading zeros.
	for(i = (result[0] == '-') ? 1 : 0; result[i] == '0'; i++);
	
	//If the result is 0, then previous loop will end in 'NULL'.
	if (result[i] == NULL) --i;
	
	//Launch all the digits other than leading zeros.
	for(j = (result[0] == '-') ? 1 : 0;	result[i] != NULL; j++, i++) result[j] = result[i];
	
	result[j] = NULL;
	
	return;
}

//'string' = 'string' + 'digit'.
void add_digit(char *string, int digit)
{
	int i, sum, length = strlen(string);

	for(i = length - 1; i >= 0; i--)
	{
		sum = (string[i] - '0') + digit;
		string[i] = sum % 10 + '0';
		digit = sum / 10;
	}

   //If a carry come, add it in the 1st position after shifting all the characters
	//(including the '\0') one step to the right.
	if (digit != 0)
	{
		for(i = length; i >= 0; i--) string[i + 1] = string[i];
		string[0] = digit + '0';
   }

	return;
}

int main(){
	char n[104], multi[212], sub[212];
	//int i, k, j;
	while(scanf(" %s", n) == 1){
		if(!strcmp(n, "0")){printf("1\n"); continue;}
		character_multiplication(n, n, multi);
		//printf("%s\n", multi);
		character_subtraction(multi, n, sub);
		//printf("%s\n", sub);
		add_digit(sub, 2);

		printf("%s\n", sub);
	}
	return 0;
}
