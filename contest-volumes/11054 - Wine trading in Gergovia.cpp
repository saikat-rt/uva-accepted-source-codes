/*****  Wine trading in Gergovia @ 11054  *****/

#include <stdio.h>
#include <stdlib.h>

long long n, d, k, m, i;

int main(){
	while(scanf("%lld", &n) == 1 && n != 0){
		d = k = 0;
		for(i = 0; i < n; i++){
			scanf("%lld", &m);
			k += abs(d);
			d += m;
		}
		printf("%lld\n", k);
	}
	return 0;
}
