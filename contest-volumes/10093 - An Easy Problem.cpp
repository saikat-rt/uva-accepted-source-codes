/*****  An Easy Problem @ 10093  *****/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define MAX 100000

int main(){
	char str[MAX];
	long flag,max,min_base,val,a,b,len,sum;
	while(gets(str)){
		len = strlen(str);
		if(len != 0){
			sum = 0;
			max = 0;
			for(a = 0;a < len; a++){
				if(!isalpha(str[a]) && !isdigit(str[a]))
					continue;
				if(isdigit(str[a]))
					val = str[a] - '0';
				else if(isalpha(str[a]))
					if(str[a] >= 'A' && str[a] <= 'Z')
						val=str[a] - 'A' + 10;
					else if(str[a] >= 'a' && str[a] <= 'z')
						val = str[a] - 'a' + 36;
				if(val > max)
					max = val;     
				sum += val;     
			}
			
			if(max == 0 && sum == 0)
				printf("2\n");
			else{
				min_base = max + 1;
				flag = 1;
				for(b = min_base - 1; ;b++){
					if((sum % b) == 0)	break;
					if(b >= 61){
						flag = 0;
						break;
					}
				}
				if(flag)	printf("%ld\n" ,b + 1);
				else if(!flag)	printf("such number is impossible!\n");
			}
		}
	}
	return 0;
}