/** Language Detection @ 12250 **/

#include <stdio.h>
#include <string.h>

int main(){
	char s[15];
	int kase = 0;
	
	while(gets(s)){
		++kase;
		if (!strcmp(s, "#"))	break;
		else if (!strcmp(s, "HELLO"))	printf("Case %d: ENGLISH\n", kase);
		else if (!strcmp(s, "HOLA"))	printf("Case %d: SPANISH\n", kase);
		else if (!strcmp(s, "HALLO"))	printf("Case %d: GERMAN\n", kase);
		else if (!strcmp(s, "BONJOUR"))	printf("Case %d: FRENCH\n", kase);
		else if (!strcmp(s, "CIAO"))	printf("Case %d: ITALIAN\n", kase);
		else if (!strcmp(s, "ZDRAVSTVUJTE"))	printf("Case %d: RUSSIAN\n", kase);
		else	printf("Case %d: UNKNOWN\n", kase);
	}
	return 0;
}
