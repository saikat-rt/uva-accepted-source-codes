/**** Dooms Day @ 11774  ****/

#include <stdio.h>

using namespace std;

typedef long long ss;

ss GCD(ss a, ss b){
	if(b == 0)	return a;
	else	return GCD(b, a % b);
}

int main(){
	ss m, n, tc, kase;
	scanf("%lld", &tc);
	kase = 0;
	while(tc--){
		kase++;
		scanf("%lld %lld", &m, &n);
		
		printf("Case %lld: %lld\n", kase, (m + n) / GCD(m, n));
	}
	return 0;
}
