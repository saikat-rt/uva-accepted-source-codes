/*****  mr. azad and his son @ 10490  *****/

#include<stdio.h>
#include<math.h>

long n;
long long perfect;
//_int64 perfect;

int main(){
	while(scanf("%ld",&n)==1){
		if(n==0)	break;
		else{
			if(n==11 || n==23 || n==29)
				printf("Given number is prime. But, NO perfect number is available.\n");
			else if(n==2||n==3||n==5||n==7||n==13||n==17||n==19||n==31){
				perfect=pow(2,(n-1))*(pow(2,n)-1);
				printf("Perfect: %lld!\n",perfect);
				//printf("Perfect: %I64d!\n",perfect);
			}
			else
				printf("Given number is NOT prime! NO perfect number is available.\n");
		}
	}
	return 0;
}
