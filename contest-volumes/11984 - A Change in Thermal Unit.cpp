/** A Change in Thermal Unit @ 11984 **/

#include <stdio.h>

int main(){
	int tc, kase = 0;
	double c, f, d;
	
	scanf("%d", &tc);
	while(tc--){
		kase++;
		scanf("%lf %lf", &c, &d);
		f = 1.8*c + 32.0;
		f += d;
		c = (f-32.0) * (5.0/9.0);
		printf("Case %d: %0.2lf\n", kase, c);
	}
	return 0;
}
