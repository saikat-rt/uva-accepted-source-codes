/*******  love calculator @ 10424  ********/

#include<stdio.h> 
#include<string.h> 
#include<ctype.h> 

char s1[28],s2[28]; 
long len1,len2; 
long i,j; 
long sum1,sum2; 
double res;
long digit1,digit2;
 
int main() { 
   while(gets(s1)&&gets(s2)) { 
	   len1=strlen(s1);         // length of string 
       len2=strlen(s2);

	   sum1=0;
	   for(i=0;i<len1;i++){					// calculating sum
		   if(s1[i]>='a' && s1[i]<='z') 
			   sum1+=s1[i]-'a'+1; 
		   else if(s1[i]>='A' && s1[i]<='Z') 
               sum1+=s1[i]-'A'+1; 
	   }

	   sum2=0;
	   for(j=0;j<len2;j++) { 
		   if(s2[j]>='a' && s2[j]<='z') 
			   sum2+=s2[j]-'a'+1; 
		   else if(s2[j]>='A' && s2[j]<='Z') 
               sum2+=s2[j]-'A'+1;  
	   }

	  
	   if(sum1%9==0)	digit1=9;
	   else	digit1=sum1%9;

	   if(sum2%9==0)	digit2=9;
	   else	digit2=sum2%9;

	   if(digit1<digit2)
		   res=(double)(digit1*100.00)/digit2;
	  
	   else
		   res=(double)(digit2*100.00)/digit1;

	   printf("%0.2lf %%\n",res);
   }
   return 0;
}

		

      
