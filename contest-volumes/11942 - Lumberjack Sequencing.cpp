

#include <stdio.h>

int main(){
	int tc, num[11], i;
	
	scanf("%d", &tc);
	
	printf("Lumberjacks:\n");
	
	while(tc--){
		for(i = 0; i < 10; i++)
			scanf("%d", &num[i]);
			
		int flag = 0;
			
		if(num[0] > num[9]){
			for(i = 0; i < 9; i++){
				if(num[i] < num[i + 1]){
					flag = 1;
					break;
				}
			}
		}
			
		else if(num[0] < num[9]){
			for(i = 0; i < 9; i++){
				if(num[i] > num[i + 1]){
					flag = 1;
					break;
				}
			}
		}
		
		if(flag == 0)	printf("Ordered\n");
		else	printf("Unordered\n");
	}
}
