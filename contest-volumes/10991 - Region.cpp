/*****  region @ 10991  *****/

#include<stdio.h>
#include<math.h>
#define pi 2*acos(0)

long test;
double r1, r2, r3, a1, a2, a3, s, t1, t2, t3, area, region;

int main(){
	while(scanf("%ld",&test)==1){
		for(long i=0;i<test;i++){
			scanf("%lf %lf %lf",&r1,&r2,&r3);

			a1=r1+r2;
			a2=r2+r3;
			a3=r3+r1;

			s=(a1+a2+a3)/2.0;

			area=sqrt(s*(s-a1)*(s-a2)*(s-a3));

			t1=acos((a2*a2+a3*a3-a1*a1)/(2.0*a2*a3));
			t2=acos((a3*a3+a1*a1-a2*a2)/(2.0*a3*a1));
			t3=acos((a1*a1+a2*a2-a3*a3)/(2.0*a1*a2));

			region=area-((r1*r1*t2+r2*r2*t3+r3*r3*t1)/2.0);

			printf("%lf\n",region);
		}
	}
	return 0;
}