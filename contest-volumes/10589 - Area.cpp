/*
*    Area @ 10589
*    [accepted]
*/

#include <cstdio>


double sqdist(double x, double y, double p, double q) {
    return (x - p) * (x - p) + (y - q) * (y - q);
}


int main() {
    int n, a;
    while (scanf("%d %d", &n, &a) == 2 && n) {
        int count = 0;
        double rr = a * a;
        for (int i = 0; i < n; i++) {
            double x, y;
            scanf("%lf %lf", &x, &y);

            if (sqdist(x, y, 0, 0) <= rr && sqdist(x, y, 0, a) <= rr && sqdist(x, y, a, 0) <= rr && sqdist(x, y, a, a) <= rr) {
                ++count;
            }
        }

        double area = (count * rr) / n;
        printf("%0.5lf\n", area);
    }

    return 0;
}
