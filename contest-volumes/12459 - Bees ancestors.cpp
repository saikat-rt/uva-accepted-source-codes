/**  Bees' Ancestors @ 12459 **/

#include <stdio.h>

long long fib[82];
//__int64 fib[82];

void fibonacci(){
	long long i;
	//__int64 i;
	
	fib[0] = 1;
	fib[1] = 1;
	
	for(i = 2; i < 82; i++){
		fib[i] = fib[i-1] + fib[i-2];
	}
}

int main(){
	long long gen;
	//__int64 gen;
	
	while(scanf("%lld", &gen) == 1 && gen != 0){
		fibonacci();
		printf("%lld\n", fib[gen]);
	}
	return 0;
}
