/********  11538 - Chess Queen  *************/

#include<stdio.h>

long long m, n, result, temp;

int main(){
	while(scanf("%lld %lld", &m, &n)==2 && m!=0 && n!=0){
		if(n>m){
			temp=n;
			n=m;
			m=temp;
		}
		result=m*(m-1)*n+n*(n-1)*m+(n*(n-1)*(2*n-1)/6-(n-1)*n/2)*4+2*n*(n-1)*(m-n+1);
		printf("%lld\n", result);
	}
	return 0;
}