/*****  sms typing @ 11530  *****/

#include<stdio.h>
#include<string.h>

long tc, len, i, j, cse=0, count;
char sms[104], s[1];

int main(){
	scanf("%ld", &tc);
	gets(s);
	while(tc--){
		gets(sms);
		len = strlen(sms);
		cse++;
		count = 0;
		for(j = 0; j < len; j++){
			if(sms[j]==' '||sms[j]=='a'||sms[j]=='d'||sms[j]=='g'||sms[j]=='j'||sms[j]=='m'||sms[j]=='p'||sms[j]=='t'||sms[j]=='w')
				count = count+1;
			else if(sms[j]=='b'||sms[j]=='e'||sms[j]=='h'||sms[j]=='k'||sms[j]=='n'||sms[j]=='q'||sms[j]=='u'||sms[j]=='x')
				count = count+2;
			else if(sms[j]=='c'||sms[j]=='f'||sms[j]=='i'||sms[j]=='l'||sms[j]=='o'||sms[j]=='r'||sms[j]=='v'||sms[j]=='y')
				count = count+3;
			else if(sms[j]=='s'||sms[j]=='z')
				count = count+4;			
		}
		printf("Case #%ld: %ld\n", cse, count);
	}
	return 0;
}
