/*  12019 - Doom's Day Algorithm  */

#include <iostream>

using namespace std;

string day[] = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
int start[] = {5, 1, 1, 4, 6, 2, 4, 0, 3, 5, 1, 3};

int main() {
   int test, m, d;
   cin >> test;
   while(test--) {
      cin >> m >> d;
      m--;
      d = (d - 1) + start[m]; 
      d %= 7;
      cout << day[d] << endl;
   }
   return 0;
}

