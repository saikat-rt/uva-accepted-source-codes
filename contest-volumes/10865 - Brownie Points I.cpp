/*
*   Brownie Points I @ 10865
*   [accepted]
*/

#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int n;
    while (scanf("%d", &n) == 1 && n) {
        vector<int> cx;
        vector<int> cy;
        int x, y;

        for (int i = 0; i < n; ++i) {
            scanf("%d %d", &x, &y);
            cx.push_back(x);
            cy.push_back(y);
        }

        x = cx[n / 2];
        y = cy[n / 2];

        int stan = 0;
        int ollie = 0;
        for (int i = 0; i < n; ++i) {
            if ((cx[i] > x && cy[i] > y) || (cx[i] < x && cy[i] < y))   ++stan;
            if ((cx[i] > x && cy[i] < y) || (cx[i] < x && cy[i] > y))   ++ollie;
        }

        printf("%d %d\n", stan, ollie);
    }

    return 0;
}