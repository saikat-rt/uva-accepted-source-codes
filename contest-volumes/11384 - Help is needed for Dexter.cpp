/**  Help is needed for Dexter @ 11384  **/

#include <stdio.h>

long n, k;

int main(){
	while(scanf("%ld", &n) == 1){
		k = 0;
		while(n > 0){
			k++;
			n = n - (n+1)/2;
		}
		printf("%ld\n", k);
	}
	return 0;
}