/*
    Hajj-e-Akbar @ 12577
    [accepted]
*/

#include <stdio.h>
#include <string.h>

int main()
{
    int kase = 0;
    char word[6];

    while(1) {
        scanf(" %[^\n]", word);

        if(!strcmp(word, "*"))  break;
        else if (!strcmp(word, "Hajj")) printf("Case %d: Hajj-e-Akbar\n", ++kase);
        else if (!strcmp(word, "Umrah"))    printf("Case %d: Hajj-e-Asghar\n", ++kase);
    }

    return 0;
}