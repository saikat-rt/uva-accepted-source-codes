/** Emoogle Balance @ 12267 **/

#include <stdio.h>

int kase, n, inp, z, nz, i;

int main(){
	kase = 0;
	while(scanf("%d", &n) == 1 && n != 0){
		kase++;
		z = nz = 0;
		for(i = 0; i < n; i++){
			scanf("%d", &inp);
			if(inp == 0)	z++;
			else	nz++;
		}
		
		printf("Case %d: %d\n", kase, nz-z);
	}
}
