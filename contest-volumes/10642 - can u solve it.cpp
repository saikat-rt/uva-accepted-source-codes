/*****  can you solve it? @ 10642  *****/

#include<stdio.h>

long long x1, y1, x2, y2;
long long tc, temp1, temp2, output, cse=0;

int main(){
	scanf("%lld", &tc);
	while(tc--){
		scanf("%lld %lld %lld %lld", &x1, &y1, &x2, &y2);
		
		cse++;
		temp1=(1+x1+y1)*(x1+y1)/2+x1;
		temp2=(1+x2+y2)*(x2+y2)/2+x2;

		output=temp2-temp1;

		printf("Case %lld: %lld\n", cse, output);
	}
	return 0;
}
