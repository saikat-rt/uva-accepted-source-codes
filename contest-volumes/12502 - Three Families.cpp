/* 
	Three Families @ 12502 (accepted) 

	[runtime 0.004 second]
*/

#include <stdio.h>

int main()
{
	int tc, x, y, z;

	scanf("%d", &tc);
	
	while (tc--)
	{
		scanf("%d %d %d", &x, &y, &z);
		
		z = z * (2 * x - y) / (x + y);
		
		if (z < 0)	z *= -1;
		
		printf("%d\n", z);
	}
	
	return 0;
}


