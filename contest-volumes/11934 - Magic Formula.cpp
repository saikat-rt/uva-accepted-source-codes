/**  Magic Formula @ 11934  **/

#include <stdio.h>

long a, b, c, d, L;
long long result;

int main(){
	while(scanf("%ld %ld %ld %ld %ld", &a, &b, &c, &d, &L) == 5){
		if(!a && !b && !c && !d && !L)	break;
		else{
			long count = 0;
			for(long i = 0; i <= L; i++){
				result = a*i*i + b*i + c;
				if(result % d == 0)	count++;
			}
			printf("%ld\n", count);
		}
	}
	return 0;
}