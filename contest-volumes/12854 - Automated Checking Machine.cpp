/*
*   Automated Checking Machine @ 12854
*   [accepted]
*/

#include <stdio.h>

int main() {

    int a, b, c, d, e;
    int p, q, r, s, t;
    
    while (scanf("%d %d %d %d %d", &a, &b, &c, &d, &e) == 5) {
        scanf("%d %d %d %d %d", &p, &q, &r, &s, &t);
        
        if (a!=p && b!=q && c!=r && d!=s && e!=t)   printf("Y\n");
        else    printf("N\n");
    }
    
    return 0;
}
