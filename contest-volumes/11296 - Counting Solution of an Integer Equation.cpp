/*****  counting solution of an integer equation @ 11296  *****/

#include <stdio.h>

int main(){
	long long x,n,r ;
	while(scanf("%lld",&x)==1){
		n = (x+2)/2 ;
		r = n*(n+1)/2;
		printf("%lld\n",r) ;
	}
	return 0 ;
}
