/*****  division of nlognia @ 11498  *****/

#include<stdio.h>

long k, x, y, dx, dy;

int main(){
	while(scanf("%ld", &k) == 1 && k != 0){
		scanf("%ld %ld", &dx, &dy);
		while(k--){
			scanf("%ld %ld", &x, &y);
			if(x == dx || y == dy)	printf("divisa\n");
			else if(x > dx && y > dy)
				printf("NE\n");
			else if(x < dx && y < dy)
				printf("SO\n");
			else if(x < dx && y > dy)
				printf("NO\n");
			else if(x > dx && y < dy)
				printf("SE\n");
		}
	}
	return 0;
}
