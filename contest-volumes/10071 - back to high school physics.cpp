/*****  back to high school physics @ 10071  *****/

#include<stdio.h>

long velocity, time, distance;

int main(){
	while(scanf("%ld %ld", &velocity, &time) == 2){
		if((-100<=velocity && velocity<=100) && (0<=time && time<=200)){
			distance = 2 * velocity * time;
			printf("%ld\n", distance);
		}
	}
	return 0;
}
