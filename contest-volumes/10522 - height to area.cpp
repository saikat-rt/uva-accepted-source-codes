/*****  height to area @ 10522  *****/

#include<stdio.h>
#include<math.h>

double ha, hb, hc, a, b, c, temp, area;
long n;

int main(){
	scanf("%ld",&n);

	while(scanf("%lf %lf %lf",&ha,&hb,&hc)==3){
		if(ha==0.0||hb==0.0||hc==0.0)
			printf("These are invalid inputs!\n");
		
		else{
			a=1/ha;
			b=1/hb;
			c=1/hc;

			temp=1/((a+b+c)*(-a+b+c)*(a-b+c)*(a+b-c));
			if(temp<=0)
				printf("These are invalid inputs!\n");
			else
				printf("%0.3lf\n",sqrt(temp));
		}
	}
	return 0;
}