/*****  modular fibonacci @ 10229  *****/

#include<stdio.h>
#include<math.h>

long long m, s; 

long long fibonacci(long long n){
	long long i, h, j, k, t;
	s=(long long)pow(2,m);
	i=h=1;
	j=k=0;
	while(n>0){
		if(n%2==1){
			t=((j%s)*(h%s))%s;
			j=(((i%s)*(h%s))%s+((j%s)*(k%s))%s+t%s)%s;
			i=(((i%s)*(k%s))%s+t%s)%s;
		}
		t=((h%s)*(h%s))%s;
		h=(((2%s)*(k%s)*(h%s))%s+t%s)%s;
		k=(((k%s)*(k%s))%s+t%s)%s;
		n=(long long)n/2;
	}
	return j;
}

int main(){
	long long n, res;
	while(scanf("%lld %lld", &n, &m)==2){
		res=fibonacci(n);
		printf("%lld\n", res);
	}
	return 0;
}