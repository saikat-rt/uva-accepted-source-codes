/*****  O: dah dah dah! @ 11223  ***/

#include <stdio.h>
#include <string.h>

char dah[][10] = {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--",
				"-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--..",
				"-----",".----","..---","...--","....-",
				".....","-....","--...","---..","----.",
				".-.-.-","--..--","..--..",".----.","-.-.--","-..-.","-.--.","-.--.-",".-...",
				"---...","-.-.-.","-...-",".-.-.","-....-","..--.-",".-..-.",".--.-."};

char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,?\'!/()&:;=+-_\"@";

char word[10], message[2010];

int tc, i, k, len, j, kase = 0;

int main(){
	//freopen("11223.txt", "r", stdin);
	scanf("%d", &tc);
	while(tc--){
		kase++;
		scanf(" %[^\n]", message);
		
		len = strlen(message);
		
		printf("Message #%d\n", kase);
		
		k = -1;
		for(i = 0; i < len; i++){
			if(message[i] != ' ')
				word[++k] = message[i];
			
			if(message[i] == ' ' || i == len - 1){
				for(j = 0; j < 53; j++)
					if(!strcmp(word, dah[j]))
						printf("%c", alphabet[j]); 
						
				memset(word, 0, sizeof(word));
				k = -1;
				if(message[i] == ' ' && message[i + 1] == ' '){
					printf(" ");
					i++;
				}
			}
		}
		printf("\n");
		if(tc > 0)	printf("\n");
		memset(message, 0, sizeof(message));
	}
	return 0;
}
