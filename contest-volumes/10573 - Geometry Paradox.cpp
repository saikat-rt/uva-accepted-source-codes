/*****  geometry paradox @ 10573  *****/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#define pi 2.0*acos(0.0)

char s[55], *ptr;
long tc, r[5], cr, r1, r2, t;
double area;

int main(){
	scanf("%ld", &tc);
	while(tc--){
		scanf(" %[^\n]", s);
		
		cr=0;
		for(cr=0, ptr=strtok(s, " "); ptr!=NULL; ptr=strtok(NULL, " "))
			r[cr++]=atol(ptr);
		
		if(cr==2){
			r1=r[0], r2=r[1];
			area=2.0*pi*r1*r2;
		}
		else{
			t=r[0];
			area=(pi*t*t)/8.0;
		}
		printf("%0.4lf\n", area);
	}
	return 0;
}
