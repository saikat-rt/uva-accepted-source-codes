/*****  11577 - Letter Frequency  *****/

#include<stdio.h>
#include<string.h>

long letter[26], tc;
long len, i, j, k, max;
char str[1000];

int main(){
	scanf("%ld\n", &tc);
	while(tc--){
		gets(str);
		len=strlen(str);
		for(k=0; k<26; k++)	letter[k]=0;
		
		for(i=0; i<len; i++){
			if(str[i]>='A' && str[i]<='Z')	letter[str[i]-65]++;
			else if(str[i]>='a' && str[i]<='z')	letter[str[i]-97]++;
			else	str[i];
		}
		
		max=0;
		for(j=0; j<26; j++)
			if(letter[j]>max)	max=letter[j];
			
			
		for(i=0; i<26; i++)
			if(letter[i]==max)
				printf("%c", i+97);

		printf("\n");
	}
	return 0;
}