/*****  bee @ 11000  *****/

#include<stdio.h>

long long n, m, f, i, j;
long long fib1[1000], fib2[1000];

int main(){
	while(scanf("%lld",&n)==1){
		if(n==-1)	break;
		else{
			if(n==0)	printf("0 1\n");
			else if(n==1)	printf("1 2\n");
			else{
				fib1[0]=fib2[0]=1;
				fib1[1]=fib2[1]=2;
				for(i=2;i<=n;i++){
					fib1[i]=fib1[i-1]+fib1[i-2];
					m=fib1[i];
				}
				
				for(j=2;j<=n+1;j++){
					fib2[j]=fib2[j-1]+fib2[j-2];
					f=fib2[j];
				}
				
				printf("%lld %lld\n",m-1,f-1);
			}
		}
	}
	return 0;
}
