/*****  traffic volume @ 10693  *****/

#include<stdio.h>
#include<math.h>

double l,f;
double v,vol;

int main()
{
	while(scanf("%lf %lf",&l,&f)==2)
	{
		if(l==0 && f==0)	break;
		else
		{
			v=sqrt(l*f*2.0);
			vol=(v*1800.0)/l;

			printf("%0.8lf %0.8lf\n",v,vol);
		}
	}
	return 0;
}