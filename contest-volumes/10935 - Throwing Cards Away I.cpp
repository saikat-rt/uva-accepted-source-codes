/*****  10935 @ throwing cards away I  *****/

#include<stdio.h>
#define max 280

struct q{
	int item[max];
	int front,rear;
}q;

int n, x;
int main(){
	while(scanf("%d",&n)==1){
		if(n==0)	break;
			
		else if(n==1){
			printf("Discarded cards:\n");
			printf("Remaining card: 1\n");
		}
		
		else{
			for(int i=0;i<n;i++)	
				q.item[i]=i+1;
			q.front=0;q.rear=n-1;
			printf("Discarded cards: ");
			while(q.front!=q.rear){
				x=q.item[q.front];
				if(q.front!=q.rear-1)	printf("%d, ",x);
				else	printf("%d\n",x);
				q.front++;
				q.rear++;
				q.item[q.rear]=q.item[q.front];
				q.front++;
			}
			printf("Remaining card: %d\n",q.item[q.front]);
		}
	}
	return 0;
}
