/*****  11387 - The 3-Regular Graph  *****/

#include<stdio.h>
#include<string.h>

int data[101][101];
int i,j,n,count,c;

int main(){
	while(scanf("%d",&n)==1 && n!=0){
		if(n%2==1 || n==2)
			printf("Impossible\n");
		else{
			for(i=1;i<=n;i++)
				for(j=1;j<=n;j++)
					data[i][j]=0;
				
			c=0;
			for(i=1;i<=n;i++){
				count=0;
				for(j=2;j<=n;j++){
					if(j-i==1){
						count++;
						data[i][j]=1;
						data[j][i]=1;
						c++;
					}
					else if(j-i==3 && i%2!=0){
						count++;
						data[i][j]=1;
						data[j][i]=1;
						c++;
					}
					if(count==2)	break;
				}
			}
			data[1][n-1]=1;
			data[n-1][1]=1;
			data[2][n]=1;
			data[n][2]=1;
			c+=2;
				
			printf("%d\n",c);
				
			for(i=1;i<=n;i++){
				for(j=1;j<=n;j++){
					if(data[i][j]==1){
						printf("%d %d\n",i,j);
						data[j][i]=0;
					}
				}
			}			
		}
	}
	return 0;
}