/*****  common permutation @ 10252  *****/

#include<stdio.h>
#include<string.h>
#include<ctype.h>

char s1[1010], s2[1010];
int freq1[26], freq2[26], count[26];
int i, j, k, l, p, c, len1, len2;

int main(){
	while(gets(s1)){
		gets(s2);
		for(i=0; i<26; i++){
			freq1[i]=0;
			freq2[i]=0;
			count[i]=0;
		}
		len1=strlen(s1);
		len2=strlen(s2);

		for(j=0; j<len1; j++){
			if(s1[j]>='a' && s1[j]<='z')
				freq1[s1[j]-'a']++;
			else if(s1[j]>='A' && s1[j]<='Z')
				freq1[s1[j]-'A']++;
		}
		for(j=0; j<len2; j++){
			if(s2[j]>='a' && s2[j]<='z')
				freq2[s2[j]-'a']++;
			else if(s2[j]>='A' && s2[j]<='Z')
				freq2[s2[j]-'A']++;
		}
		for(l=0; l<26; l++){
			if(freq1[l]!=0 && freq2[l]!=0){
				if(freq1[l]<freq2[l])	count[l]=freq1[l];
				else	count[l]=freq2[l];
			}
		}

		for(c=0; c<26; c++){
			if(count[c]!=0){
				for(p=0; p<count[c]; p++)
					printf("%c", c+'a');
			}
		}
		printf("\n");
	}
	return 0;
}
