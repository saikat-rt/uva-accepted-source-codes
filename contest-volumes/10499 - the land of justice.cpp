/*****  the land of justice @ 10499  *****/

#include<stdio.h>

long long n;

int main(){
	while(scanf("%lld",&n)==1){
		if(n<0)	break;
		else{
			if(n==1)	printf("0%%\n");
			else if(n!=1)	printf("%lld%%\n",25*n);
		}
	}
	return 0;
}