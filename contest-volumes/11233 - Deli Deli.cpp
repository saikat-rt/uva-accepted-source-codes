/*****  Deli Deli @ 11233  *****/

#include <stdio.h>
#include <string.h>

char ir[23][23], irr[23][23];
char str[32];
long N, L;

int main(){
	long flag, i, j, len;
	scanf("%ld %ld", &L, &N);
	for(i=0; i<L; i++)	scanf("%s %s", ir[i], irr[i]);
	while(N--){
		flag=0;
		scanf(" %s", str);
		for(i=0; i<L; i++){
			if(!strcmp(ir[i], str)){
				printf("%s\n", irr[i]);
				flag=1;
			}
		}
		if(flag==0){
			len=strlen(str);
			
			if((str[len-2]=='a'||str[len-2]=='e'||str[len-2]=='i'||str[len-2]=='o'||str[len-2]=='u')&&str[len-1]=='y'){
				printf("%s", str);
				printf("s\n");
			}
			
			else if(str[len-1]=='y'&&(str[len-2]!='a'||str[len-2]!='e'||str[len-2]!='i'||str[len-2]!='o'||str[len-2]!='u')){
				for(j=0; j<len-1; j++)	printf("%c", str[j]);
				printf("ies\n");
			}
			else if(str[len-1]=='o'||str[len-1]=='s'||str[len-1]=='x'){
				printf("%s", str);
				printf("es\n");
			}
			else if((str[len-2]=='c'&&str[len-1]=='h')||(str[len-2]=='s'&&str[len-1]=='h')){
				printf("%s", str);
				printf("es\n");
			}
			else{
				printf("%s", str);
				printf("s\n");
			}
		}
	}
	return 0;
}