/*****  longest common subsequence @ 10405  *****/

#include<stdio.h>
#include<string.h>

char str1[1010], str2[1010];
long c[1010][1010];
long len1, len2;

void LCS_Length(char s1[], char s2[]){
	long m, n, i, j;
	m=strlen(s1);
	n=strlen(s2);
	for(i=0; i<=m; i++)	c[i][0]=0;
	for(j=0; j<=n; j++)	c[0][j]=0;

	for(i=1; i<=m; i++){
		for(j=1; j<=n; j++){
			if(s1[i-1]==s2[j-1])	c[i][j]=c[i-1][j-1]+1;
			else if(c[i-1][j]>=c[i][j-1])	c[i][j]=c[i-1][j];
			else	c[i][j]=c[i][j-1];
		}
	}
	printf("%ld\n", c[m][n]);
}

int main(){
	while(gets(str1)){
		gets(str2);
		LCS_Length(str1, str2);
	}
	return 0;
}

