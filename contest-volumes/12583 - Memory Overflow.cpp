/*
*   Memory Overflow @ 12583
*   [accepted]
*/

#include <cstdio>

int main() {
    int tc, kase = 0;
    scanf("%d", &tc);

    while (tc--) {
        int n, k;
        char names[501];

        scanf("%d %d %s", &n, &k, names);

        int v[31] = {0};
        int count = 0;

        for (int i = 0; i < n; ++i) {
            if (v[names[i] - 'A'])  ++count;

            v[names[i] - 'A']++;

            if (i - k >= 0) v[names[i - k] - 'A']--;
        }

        printf("Case %d: %d\n", ++kase, count);
    }

    return 0;
}
