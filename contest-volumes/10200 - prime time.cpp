/*****  prime time @ 10200  *****/

#include<stdio.h>
#include<math.h>

long a,b,temp,total;
long prime[100000];
double percent;

long isPrime(long n){
	long root;
	root=sqrt(n);
	if(n == 0)	return 0;
	if (n == 1) return 0;         
  	if (n == 2) return 1;         
  	if (n%2 == 0) return 0;       
  	for (long i=3; i<=root; i+=2) 
    	if (n%i == 0)             
      		return 0;
  	return 1;
}

int main(){
	long i,temp;
	total=0;
	for(i=0;i<=10000;i++){
		temp=i*i+i+41;
		prime[total++]=isPrime(temp);
	}
	while(scanf("%ld %ld",&a,&b)==2){
		long count=0;
		for(long j=a;j<=b;j++)
			if(prime[j]==1)
				count++;
		
		percent=(count*100.00)/(b-a+1);
		printf("%0.2lf\n",percent+1e-8);
	}
	return 0;
}
