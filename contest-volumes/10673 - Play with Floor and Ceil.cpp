/**  Play with Floor and Ceil @ 10673  **/

#include <stdio.h>
#include <math.h>


long ex_gcd(long p,long q,long *x,long *y){
	long x1, y1;
	long g;

	if(q > p)	return (ex_gcd(q, p, y, x));

	if(q == 0){
		*x = 1;
		*y = 0;
		return (p);
	}

	g = ex_gcd(q, p%q, &x1, &y1);

	*x = y1;
	*y = long(x1 - floor(p/q) * y1);

	//return g;
}


int main(){
	long x, k, tc, a;
	long p, q;
	long long P, Q;
	scanf("%ld", &tc);
	while(tc--){
		scanf("%ld %ld", &x, &k);
		
		if(x % k == 0)	printf("0 %ld\n", k);
		else{
			a = floor(x/k);
			ex_gcd(a, a+1, &p, &q);
			P = p*x, Q = q*x;
			printf("%lld %lld\n", P, Q);
		}
	}
	return 0;
}
