/**  Integer Game @ 11489  **/

#include <stdio.h>
#include <string.h>

int tc, flag, sum, i, len, k, count, kase;
char num[1004];

int main(){
	scanf("%d", &tc);
	kase = 0;
	while(tc--){
		kase++;
		scanf(" %s", num);
		len = strlen(num);
		if(len == 1){
			printf("Case %d: S\n", kase);
			continue;
		}
		sum = 0;
		for(i = 0; i < len; i++)	sum += (num[i] - '0');
			
		//printf("sum is %d\n", sum);

		flag = 0;
		for(i = 0; i < len; i++){
			if((sum - (num[i]-'0')) % 3 == 0){
				flag = 1;
				num[i] = '0';
				break;
			}
		}
		if(flag == 0)	printf("Case %d: T\n", kase);
		else{
			count = 0;
			for(k = 0; k < len; k++)
				if(num[k] != '0' && (num[k]-'0')%3 == 0)
					count++;

			//printf("count %d\n", count);

			if(count % 2 == 0)	printf("Case %d: S\n", kase);
			else	printf("Case %d: T\n", kase);
		}
	}
	return 0;
}