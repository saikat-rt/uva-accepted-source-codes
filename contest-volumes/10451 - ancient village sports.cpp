/***** ancient_village_sports @ 10451 *****/

#include<stdio.h>
#include<math.h>
#define PI 2*acos(0)

double area_off,area_spec,area_poly,area_outcir,area_incir;
double radius_in,radius_out;
double arm_poly;
double temp1,temp2;
long cnum=0;

int main(){
	while(scanf("%lf %lf",&arm_poly,&area_poly)==2){
        cnum++;
		if(arm_poly>=3 && arm_poly<=50 && area_poly>=0 && area_poly<=30000){
			temp1=(2*area_poly)/(arm_poly*sin((2*PI)/arm_poly));
			radius_out=sqrt(temp1);
			area_outcir=PI*radius_out*radius_out;
			area_spec=area_outcir-area_poly;

			temp2=area_poly/(arm_poly*tan(PI/arm_poly));
			radius_in=sqrt(temp2);
			area_incir=PI*radius_in*radius_in;
			area_off=area_poly-area_incir;

			printf("Case %ld: %.5lf %.5lf\n",cnum,area_spec,area_off);
		}
	}
	return 0;
}
