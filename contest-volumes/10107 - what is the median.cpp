/*****  What is the Median? @ 10107  *****/

#include<stdio.h>
#include<stdlib.h>

long arr[10000];

int sai(const void *a, const void *b){
	return *(int *)a - *(int *)b;
}

int main(){
	long k=0, d;
	while(scanf("%ld", &arr[k++])==1){
		qsort(arr, k, sizeof(long), sai);
		d=k/2;
		if(k%2==0)	printf("%ld\n",(arr[d-1]+arr[d])/2);
		else printf("%ld\n", arr[d]);
	}
	return 0;
}