/***  Flip Sort @ 10327  ***/

#include <stdio.h>
#define N 1001

long a[N], p, item, i, j, temp, flip;
 
int main(){
	while(scanf("%ld", &item) == 1){
  		for(p = 0; p < item; p++)
		scanf("%ld", &a[p]);
		flip = 0;
		for(i = 0; i < item - 1; i++){
			for(j = 0; j < item - i - 1; j++){
				if(a[j] > a[j+1]){
					temp = a[j];
					a[j] = a[j+1];
					a[j+1] = temp;
					flip++;
				}	
			}
		}
		printf("Minimum exchange operations : %ld\n", flip);
	}
	return 0;
}


	
