/*****  bus driver problem @ 11389  *****/

#include<stdio.h>
#include<stdlib.h>

long n, d, r, as[104], de[104], total, i, j, k;

int sai(const void *a, const void *b){
	return *(int *)a - *(int *)b;
}

int tap(const void *a, const void *b){
	return *(int *)b - *(int *)a;
}

int main(){
	while(scanf("%ld %ld %ld", &n, &d, &r)==3){
		if(n==0 && d==0 && r==0)	break;
		else{
			total=0;
			for(i=0; i<n; i++)	scanf("%ld", &as[i]);
			for(j=0; j<n; j++)	scanf("%ld", &de[j]);

			qsort(as, n, sizeof(long), sai);
			qsort(de, n, sizeof(long), tap);

			for(k=0; k<n; k++){
				if((de[k]+as[k])>d)
					total+=de[k]+as[k]-d;
			}
			printf("%ld\n", total*r);
		}
	}
	return 0;
}
