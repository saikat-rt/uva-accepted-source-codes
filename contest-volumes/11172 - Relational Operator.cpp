/*****  relational operator @ 11172  *****/

#include<stdio.h>

long test, pre, post;

int main(){
	scanf("%ld",&test);
	while(test--){
		scanf("%ld %ld",&pre,&post);
		if(pre-post==0) printf("=\n");
		
		else if(pre-post>0) printf(">\n");
		
		else printf("<\n");		
	}
	return 0;
}

