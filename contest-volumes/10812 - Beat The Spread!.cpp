/**********  beat the spread! @10812  ***************/

#include<stdio.h>

int main()
{
	long sum,dif;
	long fst,sec;
	long test;
	long i;

	while(scanf("%ld",&test)==1)
	{
		for(i=0;i<test;i++)
		{
			while(scanf("%ld %ld",&sum,&dif)==2)
			{
				if(sum>=dif && (sum+dif)%2==0 && (sum-dif)%2==0)
				{
					fst=(sum+dif)/2;
					sec=(sum-dif)/2;

					printf("%ld %ld\n",fst,sec);
					
				}

				else	printf("impossible\n");
			}
		}
	}
	return 0;
}
