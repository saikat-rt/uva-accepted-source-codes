/*****  vacation @ 10192  *****/

#include<stdio.h>
#include<string.h>

char str1[104], str2[104];
long c[104][104];
long len1, len2, cse=0;

void LCS_Length(char s1[], char s2[]){
	long m, n, i, j;
	m=strlen(s1);
	n=strlen(s2);
	for(i=0; i<=m; i++)	c[i][0]=0;
	for(j=0; j<=n; j++)	c[0][j]=0;

	for(i=1; i<=m; i++){
		for(j=1; j<=n; j++){
			if(s1[i-1]==s2[j-1])	c[i][j]=c[i-1][j-1]+1;
			else if(c[i-1][j]>=c[i][j-1])	c[i][j]=c[i-1][j];
			else	c[i][j]=c[i][j-1];
		}
	}
	printf(" you can visit at most %ld cities.\n", c[m][n]);
}

int main(){
	while(gets(str1)){
		if(strcmp(str1, "#")==0)	break;
		else{
			cse++;
			gets(str2);
			printf("Case #%ld:", cse);
			LCS_Length(str1, str2);
		}
	}
	return 0;
}
