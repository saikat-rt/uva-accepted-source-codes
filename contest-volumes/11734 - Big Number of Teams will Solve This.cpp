/****  Big Number of Teams will Solve This @ 11734  ****/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

char str1[23], str2[23], tmps[23], c[1];
int i, j, k, len1, len2, kase = 0, tc;

int main(){
	scanf("%d", &tc);
	gets(c);
	while(tc--){
		kase++;
		
		gets(str1);
		gets(str2);
		
		len1 = strlen(str1);
		len2 = strlen(str2);
		
		
		if(!strcmp(str1, str2))	printf("Case %d: Yes\n", kase);
				
		else{
			j = 0;
			
			for(i = 0; i < len1; i++)
				if(isalpha(str1[i]))
					tmps[j++] = str1[i]; 
					
			
			if(!strcmp(str2, tmps))
				printf("Case %d: Output Format Error\n", kase);
		
			else
				printf("Case %d: Wrong Answer\n", kase);
				
			for(i = 0; i <= j; i++)	tmps[i] = '\0';
		}
	}
	return 0;
}
