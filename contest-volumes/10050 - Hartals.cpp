/*****  Hartals @ 10050  *****/

#include <stdio.h>

long tc, day[3655], pp, dd, nd;
long i, ncd, hd, wd;

int main(){
	scanf("%ld", &tc);
	while(tc--){
		hd=0, ncd=0;
		scanf("%ld", &nd);
		for(i=1; i<=nd; i++)	day[i]=1;
		for(i=6; i<=nd; i+=7)	day[i]=0;
		for(i=7; i<=nd; i+=7)	day[i]=0;
		for(i=1; i<=nd; i++)
			if(day[i])	
				hd++;
		scanf("%ld", &pp);
		while(pp--){
			scanf("%ld", &dd);
			for(i=dd; i<=nd; i+=dd)	
				day[i]=0;
		}
		for(i=1; i<=nd; i++)
			if(day[i])
				ncd++;

		wd=hd-ncd;
		printf("%ld\n", wd);
	}
	return 0;
}