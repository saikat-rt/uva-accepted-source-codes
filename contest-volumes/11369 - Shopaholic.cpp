/**  Shopaholic @ 11369  **/

#include <stdio.h>
#include <stdlib.h>

long tc, items, prices[20003];
long i, j, total;

int dsort(const void *a, const void *b){
	return *(int *)b - *(int *)a;
}

int main(){
	scanf("%ld", &tc);
	while(tc--){
		scanf("%ld", &items);
		for(i = 0; i < items; i++)
			scanf("%ld", &prices[i]);
		
		qsort(prices, items, sizeof(long), dsort);

		total = 0;
		for(j = 2; j < items; j += 3)
			total += prices[j]; 

		printf("%ld\n", total);
	}
	return 0;
}