/*****  all you need is love @ 10193  *****/

#include<stdio.h>
#include<string.h>
#include<math.h>

char s[31],t[31];
long len_s,len_t,test;
long sum_s,sum_t;
long pos,pot;
long g,pair;

long gcd(long a,long b){
	if(b==0) 
		return a;
	else if(a==0)
		return b;
	else if(a<b)
		return gcd(a,b%a); 
	else 
		return gcd(b,a%b);
}

int main(){
	while(scanf("%ld",&test)==1){
		for(long l=0;l<test;l++){
			pair++;
			scanf("%s \n%s",s,t);
			len_s=strlen(s);
			len_t=strlen(t);
			sum_s=sum_t=0;
			
			pos=0;
			for(long i=len_s-1;i>=0;i--){
				sum_s+=(s[i]-'0')*pow(2,pos);
				pos++;
			}
			
			pot=0;
			for(long j=len_t-1;j>=0;j--){
				sum_t+=(t[j]-'0')*pow(2,pot);
				pot++;
			}
			
			g=gcd(sum_s,sum_t);
			
			if(g>1)	printf("Pair #%ld: All you need is love!\n",pair);
			else	printf("Pair #%ld: Love is not all you need!\n",pair);
		 }
	}
	return 0;
}

				
				
