/*****  10679 - I Love Strings!!  *****/

#include <stdio.h>
#include <string.h>

char str1[100010], str2[100010];
long len1, len2, count, i;
long tc, ss;

int main(){
	scanf("%ld", &tc);
	while(tc--){
		scanf("%s %ld", str1, &ss); 
		len1 = strlen(str1);
		while(ss--){
			scanf(" %s", str2);
			len2 = strlen(str2);
			count=0;
		
			for(i=0; i<len1; ++i)
				if(count<len2){
					if(str1[i] == str2[count])
						count++;
					else	break;
				}
				
			if(count == len2)	printf("y\n");
			else	printf("n\n");
			memset(str2, 0, sizeof(str2));
		}
	}
	return 0;
}
