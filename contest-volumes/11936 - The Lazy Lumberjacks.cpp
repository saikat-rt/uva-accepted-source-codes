/** The Lazy Lumberjacks @ 11936  **/

#include <stdio.h>

long tc, a, b, c;

int main(){
	scanf("%ld", &tc);
	while(tc--){
		scanf("%ld %ld %ld", &a, &b, &c);
		
		if(a >= b && a >= c)
			if(b + c > a)	printf("OK\n");
			else	printf("Wrong!!\n");
		
		else if(b >= a && b >= c)
			if(a + c > b)	printf("OK\n");
			else	printf("Wrong!!\n");
			
		else if(c >= a && c >= b)
			if(a + b > c)	printf("OK\n");
			else	printf("Wrong!!\n");
	}
	return 0;
}
