/*****  Tell me the frequencies! @ 10062  *****/

#include<stdio.h>
#include<string.h>
#include<ctype.h>

char s[1010];
long freq[130], saikat[130],flag=0;
long res[130];
long len,i,t,u,k,p,r,m,j;

int main(){
	while(gets(s)){
		len=strlen(s);
		if(flag)
			printf("\n");
		for(i=0; i<130; i++){ 
			freq[i]=0; 
			saikat[i]=0; 
			res[i]=0;
		}
		for(j=0; j<len; j++){
			freq[s[j]]++; 
		}
		
		t=0;
		for(k=32;k<129; k++){
			if(freq[k]!=0){
				saikat[t]=k;
				res[t]=freq[k];
				t++;	
			}	
		}
		
		for(p=1; p<t; p++){ 
			for(r=0; r<t-p; r++){
				if(res[r]<res[r+1]){
					u=res[r];
					res[r]=res[r+1];
					res[r+1]=u;
					u=saikat[r];
					saikat[r]=saikat[r+1];
					saikat[r+1]=u;
					
				}
			}
		}

		for(m=t-1; m>=0; m--)
			printf("%ld %ld\n", saikat[m], res[m]);
		
		if(flag==0)
			flag=1;
	}
	return 0;
}
					

