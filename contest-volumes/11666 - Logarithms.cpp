/*****  Logarithms @ 11666  *****/

#include <stdio.h>
#include <math.h>

long n, L;
double x;

int main(){
	while(scanf("%ld", &n) == 1 && n != 0){
		L = ceil(log(n) - log(2));
		x = 1 - exp(log(n) - L);
		printf("%ld %.8lf\n", L, x);
	}
	return 0;
}
