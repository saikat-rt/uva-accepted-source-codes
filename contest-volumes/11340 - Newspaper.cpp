/** Newspaper @ 11340  **/

#include <stdio.h>
#include <string.h>
#define loop(i, n)  for(i = 0; i < n; i++)

long tc, k, line, c, i, j, value[104], len;
char news[10004], letters[104];
double price;

int main(){
    //freopen("test.txt", "r", stdin);
    scanf("%ld\n", &tc);
    while(tc--){
        scanf("%ld\n", &k);
        loop(i, k)  scanf("%c %ld\n", &letters[i], &value[i]);

        scanf("%ld\n", &line);
        price = 0;
        loop(i, line){
            gets(news);
            len = strlen(news);
            loop(j, k)
                loop(c, len)
                    if(letters[j] == news[c])
                        price += value[j];
        }
        printf("%.2lf$\n", price / 100);
    }
    return 0;
}
