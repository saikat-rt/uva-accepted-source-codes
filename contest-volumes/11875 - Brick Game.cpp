/*****  Brick Game @ 11875  *****/


#include <stdio.h>


int main(){
	long T, N, player[13], i, kase = 0;
	
	scanf("%ld", &T);
	
	while(T--){
		kase++;
		scanf("%ld", &N);
		
		for(i = 0; i < N; i++)	scanf("%ld", &player[i]);
		
		printf("Case %ld: %ld\n", kase, player[N/2]);
	}
	return 0;
}
