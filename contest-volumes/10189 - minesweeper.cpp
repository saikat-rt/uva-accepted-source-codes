/*****  mine sweeper @ 10189  *****/

#include<stdio.h>

int i, j, a, b, count, tc, flag=0;
char field[104][104];

int main(){
	tc=0;
	while(scanf("%d %d", &i, &j)==2 && i!=0 && j!=0){
		tc++;
		for(a=1; a<104; a++)
			for(b=1; b<104; b++)
				field[a][b]='.';

		if(flag==1)	printf("\n");
		for(a=1; a<=i; a++){
			for(b=1; b<=j; b++){
				scanf(" %c", &field[a][b]);
			}
		}
		printf("Field #%d:\n", tc);
		count=0;
		for(a=1; a<=i; a++){
			for(b=1; b<=j; b++){

				if(field[a][b]=='.'){
					if(field[a-1][b-1]=='*')	count++;
					if(field[a-1][b]=='*')		count++;
					if(field[a-1][b+1]=='*')	count++;
					if(field[a][b-1]=='*')		count++;
					if(field[a][b+1]=='*')		count++;
					if(field[a+1][b-1]=='*')	count++;
					if(field[a+1][b]=='*')		count++;
					if(field[a+1][b+1]=='*')	count++;

					field[a][b]=count+'0';
				}
				count=0;
			}

		}
		for(a=1; a<=i; a++){
			for(b=1; b<=j; b++){
				printf("%c", field[a][b]);
			}
			printf("\n");
		}
		if(flag==0)	flag=1;
	}
	return 0;
}
