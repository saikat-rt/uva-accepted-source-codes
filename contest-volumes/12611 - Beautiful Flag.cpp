/*
	Beautiful Flag - 12611
	[accepted]
*/

#include <stdio.h>

int main () {
	int tc, rad, len, wid, x, kase = 0;
	
	scanf("%d", &tc);
	
	while (tc--) {
		scanf("%d", &rad);
		
		len = 5 * rad;
		wid = (6 * len) / 10;
		
		x = (len * 45) / 100;
		
		printf("Case %d:\n", ++kase);
		printf("%d %d\n", -x, wid/2);
		printf("%d %d\n", len-x, wid/2);
		printf("%d %d\n", len-x, -wid/2);
		printf("%d %d\n", -x, -wid/2); 
	}
	
	return 0;
}
