/********  2 the 9s  @  10922   ***************/

#include<stdio.h>
#include<string.h>
#include<ctype.h>

char s[1001];
long len, i, sum, deg, temp;

int main(){
	while(gets(s)){
		len=strlen(s);
		if(s[0]=='0')	break;
		else{
			sum=0;
			for(i=0;i<len;i++)	
				sum+=s[i]-'0';
			if(sum%9==0){
				deg=1;
				while(sum!=9){	
					temp=0;
					while(sum>0){
						temp+=sum%10;
						sum=sum/10;
					}
					sum=temp;
					deg++;
				}
				printf("%s is a multiple of 9 and has 9-degree %ld.\n",s,deg);
			}
			else
			printf("%s is not a multiple of 9.\n",s);
		}
	}
	return 0;
}

