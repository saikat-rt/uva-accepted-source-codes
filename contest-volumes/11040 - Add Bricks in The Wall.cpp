/*** Add Bricks in The Wall @ 11040  ***/

#include <stdio.h>

int main(){
   int wall[9][9];
   int kase, row, col;
   scanf("%d",&kase);
  
   while(kase--){
      for(row = 0; row <= 8; row += 2)
         for(col = 0; col <= row; col += 2)
            scanf("%d", &wall[row][col]);
      
      for(row = 0; row <= 6; row += 2)
         for(col = 0;col <= row; col += 2){
            wall[row + 2][col + 1] = (wall[row][col] - wall[row + 2][col] - wall[row + 2][col + 2]) / 2;
            wall[row + 1][col] = wall[row + 2][col] + wall[row + 2][col + 1];
            wall[row + 1][col + 1]=wall[row + 2][col + 2] + wall[row + 2][col + 1];
         }
      
      for(row = 0;row <= 8;row++){
		  for(col = 0;col <= row;col++){
            printf("%d",wall[row][col]);
			if(col < row)	printf(" ");
		  }
        
         printf("\n");
      }
   }
   return 0;
}