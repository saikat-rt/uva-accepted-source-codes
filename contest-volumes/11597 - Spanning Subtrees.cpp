/*****  Spanning Subtrees @ 11597  *****/


#include <stdio.h>


int main()
{
	int n, kase = 0;

	while(scanf("%d", &n) == 1 && n != 0)
	{
		printf("Case %d: %d\n", ++kase, n / 2);
	}
	return 0;
}
