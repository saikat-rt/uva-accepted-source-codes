/*
    Zeros and Ones @ 10324
    [accepted]
*/

#include <stdio.h>

char zerones[1000005];
int sum[1000005];

int main(void)
{
    int kase = 0;

    while(gets(zerones))
    {
        printf("Case %d:\n", ++kase);
        int q;
        scanf("%d\n", &q);

        sum[0] = zerones[0] - '0';

        for(int i = 1; zerones[i]; i++)
            if(zerones[i] != zerones[i-1]) sum[i] = sum[i-1]+1;
            else sum[i] = sum[i-1];

        while(q--)
        {
            int b, e;
            scanf("%d %d\n", &b, &e);

            if(b > e) b ^= e ^= b ^= e;

            if(sum[b] == sum[e])    printf("Yes\n");
            else    printf("No\n");
        }
    }
    return 0;
}
