/*****  sort sort and sort @ 11321  *****/

#include<stdio.h>
#include<stdlib.h>

long m, n, i;

typedef struct{
	long num;
	long rem;
	long foe;
}sai;

sai data[10010];

int comp(const void *a, const void *b){
	sai *x = (sai *) a;
	sai *y = (sai *) b;
	
	if(x->rem < y->rem)	return -1;
	if(x->rem > y->rem)	return 1;
	
	if(x->foe >y->foe)	return -1;
	if(x->foe < y->foe)	return 1;
	
	if(x->foe==1 && x->foe==1){
		if(x->num > y->num)	return -1;
		if(x->num < y->num)	return 1;
	}
	
	if(x->foe==0 && x->foe==0){
		if(x->num > y->num)	return 1;
		if(x->num < y->num)	return -1;
	}
}

int main(){
//	freopen("11321.txt","r",stdin);
	while(scanf("%ld %ld", &n, &m)==2){
		printf("%ld %ld\n", n, m);

		if(n==0 && m==0)	break;

		for(i=0; i<n; i++){
			scanf("%ld", &data[i].num);

			if(data[i].num%2==0)	data[i].foe=0;
			else	data[i].foe=1;

			data[i].rem=data[i].num % m;
		}

		qsort(data, n, sizeof(sai), comp);

		for(i=0; i<n; i++)
			printf("%ld\n", data[i].num);
	}
	return 0;
}





