/*****  Another Lottery @ 11628  *****/

#include<stdio.h>

long n, m;
long mat[10010][40];

long GCD(long a, long b){
	if(b==0)	return a;
	else 	return GCD(b, a%b);
}

int main(){
	while(scanf("%ld %ld", &n, &m)==2 && n!=0 && m!=0){
		long i, j, total=0, g;
		for(i=0; i<n; i++){
			for(j=0; j<m; j++){
				scanf("%ld", &mat[i][j]);
			}
		}
		
		for(i=0; i<n; i++){
			total+=mat[i][m-1];
		}
		
		for(i=0; i<n; i++){
			g=GCD(total, mat[i][m-1]);
			printf("%ld / %ld\n", mat[i][m-1]/g, total/g);
		}
	}
	return 0;
}
