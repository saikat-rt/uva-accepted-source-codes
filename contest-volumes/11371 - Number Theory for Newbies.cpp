/*****  number theory for newbies @ 11371  *****/

#include<stdio.h>
#include<string.h>
#include<math.h>

char max[13], min[13], ini[13];
long long num;

void absort(char d[] ,long long len){
	long long i, j;
	char t;
	for(i=1; i<len; i++){
		for(j=0; j<=len-1-i; j++){
			if(d[j]>d[j+1]){
				t=d[j];
				d[j]=d[j+1];
				d[j+1]=t;
			}
		}
	}
}

void dbsort(char d[], long long len){
	long long i, j;
	char t;
	for(i=1; i<len; i++){
		for(j=0; j<=len-1-i; j++){
			if(d[j]<d[j+1]){
				t=d[j];
				d[j]=d[j+1];
				d[j+1]=t;
			}
		}
	}
}

int main(){
	long long len, i, j, k, maxi, mini;
	char t;
	while(scanf("%lld", &num)==1){
		sprintf(ini,"%lld",num);
		len=strlen(ini);
		for(i=0; i<len; i++)
			max[i]=min[i]=ini[i];

		dbsort(max,len);
		absort(min,len);
		k=0;
		if(min[0]=='0'){
			while(min[k]=='0')
				k++;
			t=min[k];
			for(j=k;j>0;j--)
				min[j]=min[j-1];
			min[j]=t;
		}
		maxi=mini=0;

		for(i=0;i<len;i++){
			maxi=(maxi*10)+max[i]-'0';
			mini=(mini*10)+min[i]-'0';
		}
		j=maxi-mini;
		printf("%lld - %lld = %lld = 9 * %lld\n",maxi,mini,j,j/9);
	}
	return 0;
}



