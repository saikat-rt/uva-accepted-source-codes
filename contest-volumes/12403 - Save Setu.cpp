/*
*	Save Setu @ 12403
*	[accepted]
*/

#include <cstdio>
#include <cstring>

int main() {
	int tc;
	scanf("%d", &tc);
	int total = 0;
	int k;
	char inpstr[10];
	while(tc--) {
		scanf(" %s", inpstr);
		if(!strcmp(inpstr, "donate")) {
			scanf("%d", &k);
			total += k;
		}
		else if(!strcmp(inpstr, "report"))	printf("%d\n", total);
	}
	return 0;
}
