/*****  gcd @ 11417  *****/

#include<stdio.h>

long n,i,j,g;
long a,b;

long gcd(long a,long b){
	 if(b==0) 
         return a;
	 else if(a==0)
		 return b;
	 else if(a<b)
		 return gcd(a,b%a); 
     else 
		 return gcd(b,a%b);
}

int main(){
	while(scanf("%ld",&n)==1){
		if(n==0)	break;
		else{
			g=0;
			for(i=1;i<n;i++)
				for(j=i+1;j<=n;j++)
					g+=gcd(i,j);

			printf("%ld\n",g);
		}
	}
	return 0;
}

