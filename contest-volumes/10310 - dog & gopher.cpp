/*****  dog and gopher @ 10310  *****/

#include<stdio.h>

int hole, flag;
double dogx, dogy, gopx, gopy, holx, holy, disd, disg;
double hx, hy;

int main(){
	while(scanf("%d %lf %lf %lf %lf", &hole, &gopx, &gopy, &dogx, &dogy)==5){
		flag=0;
		while(hole--){
			scanf("%lf %lf", &holx, &holy);
			if(flag!=1){
				disg=(gopx-holx)*(gopx-holx)+(gopy-holy)*(gopy-holy);
				disd=(dogx-holx)*(dogx-holx)+(dogy-holy)*(dogy-holy);
				if(disd>=4.0*disg){
					flag=1;
					hx=holx;
					hy=holy;
				}
			}
		}
		if(flag==1)
			printf("The gopher can escape through the hole at (%0.3lf,%0.3lf).\n", hx, hy);
		else
			printf("The gopher cannot escape.\n");
	}
	return 0;
}




