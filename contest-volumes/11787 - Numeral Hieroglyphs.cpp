/* Numeral Hieroglyphs @ 11787 */

#include <stdio.h>
#include <string.h>

long tc, i, k, m, len, icount, dcount, output;
char letter[505], set[8];
long B, U, S, P, F, T, M;
char inc[] = "BUSPFTM";	// increasing occurrence
char dec[] = "MTFPSUB";	// decreasing occurrence

int main(){
    scanf("%ld", &tc);
    while(tc--){
        scanf(" %[^\n]", letter);
        len = strlen(letter);

        k = -1;
        set[++k] = letter[0];
        // taking the subset to check order
        for(i = 1; i < len; i++)
            if(set[k] != letter[i])
                set[++k] = letter[i];

        icount = dcount = 0;
        m = 0;
        // to check if it is increasing order
        for(i = 0; i < 7; i++)
            if(inc[i] == set[m])
                icount++, m++;

        m = 0;
        // to check if it is decreasing order
        for(i = 0; i < 7; i++)
            if(dec[i] == set[m])
                dcount++, m++;

        if(icount != k+1 && dcount != k+1)  printf("error\n");
        else if(icount == k+1 || dcount == k+1){
            B = U = S = P = F = T = M = 0;
            // counting number of occurrence
            for(i = 0; i < len; i++){
                if(letter[i] == 'B')   B++;
                else if(letter[i] == 'U')  U++;
                else if(letter[i] == 'S')  S++;
                else if(letter[i] == 'P')  P++;
                else if(letter[i] == 'F')  F++;
                else if(letter[i] == 'T')  T++;
                else if(letter[i] == 'M')  M++;
            }
            if(B>9 || U>9 || S>9 || P>9 || F>9 || T>9 || M>9)
                printf("error\n");
            else{
                output = B*1 + U*10 + S*100 + P*1000 + F*10000 + T*100000 + M*1000000;
                printf("%ld\n", output);
            }
        }
        memset(set, 0, sizeof(set));
    }
    return 0;
}
