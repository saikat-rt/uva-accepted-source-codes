/*
*   Lap @ 12791
*   [accepted]
*/


#include <stdio.h>

int main() {
    int x, y;
    
    while(scanf("%d %d", &x, &y) == 2) {
        
        int lap;
        
        for (lap = 1; ; ++lap)
            if (x * lap <= y * (lap - 1))    break;
        
        printf("%d\n", lap);
    }
    
    return 0;
}
