/*****  11827 - Maximum GCD  *****/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

long kase, m[104];
char A[1000], *ptr;
long i, k, j, g, t;

long gcd(long a, long b){
    if(b == 0)
        return a;
    else
        return gcd(b, a % b);
}

int main(){
    scanf("%ld\n", &kase);
    while(kase--){
        gets(A);

        k = -1;
        for(k = -1, ptr = strtok(A, " "); ptr != NULL; ptr = strtok(NULL, " "))
			m[++k]=atol(ptr);

        g = 0;
        for(i = 0; i < k; i++){
            for(j = i + 1; j <= k; j++){

                if(m[i] >= m[j])    t = gcd(m[i], m[j]);
                else    t = gcd(m[j], m[i]);

                if(t > g)   g = t;
            }
        }
        printf("%ld\n", g);
    }
    return 0;
}
