/*****  vito's family @ 10041  *****/

#include<stdio.h>
#include<stdlib.h>

long tc;
long f, family[30200];
long median, minsum;

int sai(const void *a, const void *b){
   return *(int *)a - *(int *)b;
}

int main(){
	scanf("%ld", &tc);
	while(tc--){
		scanf("%ld", &f);
		for(long i=0; i<f; i++)
			scanf("%ld", &family[i]);

		qsort(family,f,sizeof(long),sai);
		if(f%2==0)	median=family[f/2-1];
		else	median=family[f/2];
		minsum=0;
		for(long j=0; j<f; j++)
			minsum+=abs(median-family[j]);

		printf("%ld\n", minsum);
	}
	return 0;
}