/*
*   Little Masters @ 12704
*   [accepted]
*/

#include <stdio.h>
#include <math.h>

int main() {
    int tc;

    scanf("%d", &tc);

    while (tc--) {
        double x, y, r;
        scanf("%lf %lf %lf", &x, &y, &r);

        double dist = sqrt((x * x + y * y));

        printf("%0.2lf %0.2lf\n", r - dist, r + dist);
    }

    return 0;
}

