/*
*   A Special Happy Birthday Song @ 12554
*   [accepted]
*/

#include <cstdio>


int main() {
    char song[][10] = {"Happy", "birthday", "to", "you",
                        "Happy", "birthday", "to", "you",
                        "Happy", "birthday", "to", "Rujia",
                        "Happy", "birthday", "to", "you"};

    char names[101][101];

    int n;

    scanf("%d", &n);

    for (int i = 0; i < n; ++i) scanf(" %s", names[i]);

    if (n <= 16) {
        for (int i = 0; i < 16; ++i)
            printf("%s: %s\n", names[i % n], song[i]);
    }
    else {
        int count = 0;
        for (int i = 0; ; ++i) {
            printf("%s: %s\n", names[i % n], song[i % 16]);
            if (count >= n && i % 16 == 15) break;
            ++count;
        }
    }

    return 0;
}
