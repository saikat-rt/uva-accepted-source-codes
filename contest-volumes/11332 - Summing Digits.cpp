/******* summing digits  @ 11332  ***********/

#include<stdio.h>

long num, sum;

int main(){
	while(scanf("%ld",&num)==1){
		if(num==0)	break;

		else if(num%9==0)
			sum=9;
		else
			sum=num%9;
		printf("%ld\n",sum);
	}
	return 0;
}

