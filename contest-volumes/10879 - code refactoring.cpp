/*****  code refactoring @ 10879  *****/

#include<stdio.h>
#include<math.h>

long test,num,count,root,flag,cse;

int main(){
	scanf("%ld",&test);
	cse=0;
	while(test--){
		cse++;
		scanf("%ld",&num);
		root=sqrt(num);
		count=0;
		flag=0;
		printf("Case #%ld: %ld = ",cse,num);
		for(long i=2;i<=root;i++){
			if(flag==0){
				if(num%i==0){
					if(count==0){
						printf("%ld * %ld = ",i,num/i);
						count++;
					}
					else if(count==1){
						printf("%ld * %ld\n",i,num/i);
						flag=1;
					}
				}
			}
		}
	}
	return 0;
}
