/*_______________ odd sum _____________@10783_______*/

#include<stdio.h>

long test,pre,post,sum,i,j,c=0;

int main(){
	while(scanf("%ld",&test)==1){
		for(i=0;i<test;i++){	
			while(scanf("%ld %ld",&pre,&post)==2){
				c++;
				sum=0;
				
				for(j=pre;j<=post;j++)
					if(j%2!=0) 
						sum+=j;
				
				printf("Case %ld: %ld\n",c,sum);
			}
		}
	}
	return 0;
}