/*****  11847 - Cut the Silver Bar ****/

#include <stdio.h>
#include <math.h>

int main(){
	int n, cut;
	while(scanf("%d", &n) == 1 && n != 0){

		cut = log2(n);

		printf("%d\n", cut);
	}
	return 0;
}
