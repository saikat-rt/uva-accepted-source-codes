/*
    Factoring Large Numbers @ 10392
    [accepted]
*/

#include <stdio.h>
#include <math.h>

bool status[10000002];

void sieve()
{
    int N = 10000000;
    int sq = sqrt(N);
    for (int i = 4; i <= N; i += 2) status[i] = 1;
    for (int i = 3; i <= sq; i += 2)
    {
        if (status[i] == 0)
        {
            for (int j = i * i; j <= N; j += i) status[j] = 1;
        }
    }
    status[1] = 1;
}

int main()
{
    sieve();

    long long num;
    while (scanf("%lld", &num) == 1 && num >= 0)
    {
        int dv = 2;

        while (dv <= 1000000)
        {
            if (status[dv] == 0 && num % dv == 0)
            {
                while (num % dv == 0)
                {
                    printf("    %d\n", dv);
                    num /= dv;
                }
            }
            else    dv++;
        }

        if (num > 1)    printf("    %lld\n", num);
        printf("\n");
    }

    return 0;
}
