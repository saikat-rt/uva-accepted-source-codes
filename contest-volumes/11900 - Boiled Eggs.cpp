/*****  Boiled Eggs @ 11900  *****/

#include <stdio.h>

int tc, n, p, q, egg[32], kase, i, k, count;

int main(){
    scanf("%d", &tc);
    kase = 0;
    while(tc--){
        kase++;
        scanf("%d %d %d", &n, &p, &q);
        for(i = 0; i < n; i++)  scanf("%d", &egg[i]);

        count = 0;

        for(k = 0; k < n; k++){
            q = q - egg[k];
            if(q >= 0 && k < p) count++;
            else if(q <= 0 || k == p-1) break;
        }
        printf("Case %d: %d\n", kase, count);
    }
    return 0;
}
