/*
*   Robot Instructions @ 12503
*   [accepted]
*/

#include <stdio.h>
#include <string.h>

int main() {
    int tc;
    scanf("%d", &tc);
    while(tc--) {
        int noi;
        scanf("%d", &noi);

        char mem[101][13];
        char ins[13];
        int p = 0;
        for (int i = 1; i <= noi; ++i) {
            scanf(" %s", ins);

            if (!strcmp(ins, "LEFT")) {
                --p;
                strcpy(mem[i], ins);
            }
            else if (!strcmp(ins, "RIGHT")) {
                ++p;
                strcpy(mem[i], ins);
            }
            else {
                char as[3];
                int z;
                scanf(" %s %d", as, &z);

                strcpy(mem[i], mem[z]);
                if (!strcmp(mem[i], "LEFT"))    --p;
                else    ++p;
            }
        }

        printf("%d\n", p);
    }

    return 0;
}