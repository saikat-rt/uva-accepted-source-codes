/*****  GOURMET GAMES @ 11313  *****/

#include<stdio.h>

long tc, m, n, show, d, r, flag;

int main(){
	scanf("%ld", &tc);
	while(tc--){
		flag=0;
		show=0;
		scanf("%ld %ld", &n, &m);
		while(n>=m){
			d=n/m;
			r=n%m;
			if(n==m){
				flag=1;
				show+=1;
				break;
			}
			else if(d+r>=m){
				flag=1;
				show+=d;
				n=d+r;
				if(n<m)	break;
			}
			else{
				flag=0;
				break;
			}
		}
		if(flag==0)	printf("cannot do this\n");
		else	printf("%ld\n", show);
	}
	return 0;
}



