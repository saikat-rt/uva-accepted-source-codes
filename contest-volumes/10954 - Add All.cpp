/** Add All @ 10954 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SWAP(x,y) {int t; t = x; x = y; y = t;}

void createheap(int[],int num);
void heapsort(int[],int num);
void addheap(int number[],int addnum,int num);

int main(void) {
    int number[5000+1],ans[5000+1]={0};
    int i,num,m,count[3],k,answer=0;

    scanf("%d",&num);
    while(num!=0){
      for(i=1;i<=num;i++){
         scanf("%d",&number[i]);
      }

      createheap(number,num);

      m=num;
      k=1;
      while(m>1){

         SWAP(number[1], number[m]);
         count[0]=number[m];
         m--;
         heapsort(number,m);

         SWAP(number[1], number[m]);
         count[1]=number[m];
         m--;
         heapsort(number,m);

         count[2]=count[0]+count[1];
         ans[k]=count[2];
         k++;

         m++;
         addheap(number,count[2],m);
      }

      i=1;
      while(ans[i]!=0){
         answer+=ans[i];
         i++;
      }
      printf("%d\n", answer);
      answer=0;
	  memset(ans,0,sizeof(ans));
      scanf("%d",&num);
   }
    return 0;
}

void createheap(int number[],int num) {
    int i, s, p;
    int heap[5000+1] = {0};

    for(i = 1; i <= num; i++) {
        heap[i] = number[i];
        s = i;
        p = i / 2;
        while(s >= 2 && heap[p] > heap[s]) {
            SWAP(heap[p], heap[s]);
            s = p;
            p = s / 2;
        }
    }

    for(i = 1; i <= num; i++)
        number[i] = heap[i];

}

void addheap(int number[],int addnum,int num) {
   int s, p;

   number[num]=addnum;
   s = num;
   p = num / 2;
   while(s >= 2 && number[p] > number[s]) {
      SWAP(number[p], number[s]);
      s = p;
      p = s / 2;
   }
}

void heapsort(int number[],int num) {
    int p, s;

   p = 1;
   s = 2 * p;
   while(s <= num) {
      if(s < num && number[s+1] < number[s])
         s++;
      if(number[p] <= number[s])
         break;
      SWAP(number[p], number[s]);
      p = s;
      s = 2 * p;
   }
}
