/*****  11877 - The Coco-Cola Store  *****/

#include <stdio.h>

int n, total;

int main(){
    while(scanf("%d", &n) == 1 && n != 0){

        total = 0;
        while(n >= 3){
            total += n / 3;

            n = n / 3 + n % 3;

            if(n + 1 == 3)  n++;
        }
        printf("%d\n", total);
    }
    return 0;
}
