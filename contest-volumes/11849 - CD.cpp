/**  11849 @ CD  **/

#include <stdio.h>

long jack, jill;
long jackcd[1000001];
long i, k, count, t, beg, end, mid;

int main(){
	while(scanf("%ld %ld", &jack, &jill) == 2 && jack && jill){
		for(i = 0; i < jack; i++)	scanf("%ld", &jackcd[i]);
		count = 0;
		for(i = 0; i < jill; i++){
			scanf("%ld", &t);
			beg = 0, end = jack-1;
			mid = (beg+end)/2;
			while(beg <= end){
				if(t == jackcd[mid]){
					count++;
					break;
				}
				else if(t < jackcd[mid])	end = mid - 1;
				else	beg = mid + 1;
				mid = (beg+end)/2;
			}
			beg = mid;
		}
		printf("%ld\n", count);
	}
	return 0;
}
