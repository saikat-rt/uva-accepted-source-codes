/**  Boxes of Chocolates @ 10489  **/


#include <stdio.h>

long tc, buddy, box, i, k, line, div[101], kittu, choco;

int main(){
	scanf("%ld", &tc);
	while(tc--){
		scanf("%ld %ld", &buddy, &box);

		for(i = 0; i < box; i++){
			scanf("%ld", &line);
			div[i] = 1;
			for(k = 0; k < line; k++){
				scanf("%ld", &choco);
				div[i] = (div[i] * choco) % buddy;
			}
		}
		
		kittu = 0;
		for(i = 0; i < box; i++)	kittu += div[i];
		
		printf("%ld\n", kittu % buddy);
	}
	return 0;
}
