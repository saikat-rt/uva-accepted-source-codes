/*****  Back to Intermediate Math @ 10773  *****/

#include<stdio.h>
#include<math.h>

long tc, cse=0;
double u, v, d, temp, time1, time2;

int main(){
	scanf("%ld", &tc);
	while(tc--){
		scanf("%lf %lf %lf", &d, &v, &u);
		cse++;
		if(v>=u || v==0|| u==0)	printf("Case %ld: can't determine\n", cse);
		else{
			temp=sqrt(u*u-v*v);
			
			time1=d/temp;
			
			time2=d/u;
			
			printf("Case %ld: %0.3lf\n", cse, time1-time2);
		}
	}
	return 0;
}

