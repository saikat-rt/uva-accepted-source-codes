/**********  the grazing cow  @  10678  *********/

#include<stdio.h>
#include<math.h>
#define PI 2*acos(0)

long test, i;
long rope, dist, minor, area, focus, major;

int main(){
	while(scanf("%ld",&test)==1){
		for(i=0;i<test;i++){
			scanf("%lf %lf",&dist,&rope);
	
			major=rope/2.0;
			focus=dist/2.0;
			minor=sqrt(major*major-focus*focus);
			area=PI*major*minor;
			printf("%0.3lf\n",area);
		}
	}
	return 0;
}
