/*****  Decoding @ 11541  *****/


#include <stdio.h>
#include <string.h>

long tc;
char str[1000], c;

int main(){
	long len, i, j, num, flag, kase = 0;
	scanf("%ld", &tc);
	while(tc--){
		kase++;
		num = 0, flag = 0;
		scanf(" %s", str);
		len = strlen(str);
		printf("Case %ld: ", kase);
		for(i = 0; i < len; i++){
			if(str[i] >= 'A' && str[i] <= 'Z'){
				if(flag == 1){
					for(j = 0; j < num; j++)
						printf("%c", c);
					num = 0;
				}
				c = str[i];
			}
			else if(str[i] >= '0' && str[i] <= '9'){
				num = num * 10 + str[i] - '0';
				flag = 1;
				if(i == len - 1)
					for(j = 0; j < num; j++)
						printf("%c", c);
			}
		}
		printf("\n");
	}
	return 0;
}
