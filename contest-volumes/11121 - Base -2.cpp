/*****  Base -2 @ 11121  *****/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

long tc, num;
long ans[100];

int main(){
	long i, j, kase=0;
	scanf("%ld", &tc);
	while(tc--){
		kase++;
		scanf("%ld", &num);
		
		if(num==0)	printf("Case #%ld: 0\n", kase);
		else{
			i=-1;
			while(num!=0){
				ans[++i]=abs(num%2);
			
				if(num<0 && num%-2==-1)
					num=num/-2+1;
				else
					num=num/-2;
			}
			printf("Case #%ld: ", kase);
			for(j=i;j>=0;j--)	printf("%ld",ans[j]);
			printf("\n");
		}
	}
	return 0;
}