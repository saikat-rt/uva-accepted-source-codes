/*
*   Dangerous Dive @ 12650
*   [accepted]
*   [runtime: 0.016]
*/

# include <stdio.h>

int main() {

	int n, r, list[10002];

	while(scanf("%d %d", &n, &r) == 2) {
		int i, k;
		for (i = 1; i <= n; ++i)	list[i] = 1;
		for (i = 0; i < r; ++i) {
			scanf("%d", &k);
			list[k] = 0;
		}	

		if (n == r)	printf("*\n");
		else {
			for (i = 1; i <= n; ++i)
				if (list[i] != 0)	printf("%d ", i);
				
			printf("\n");
		}
	}

	return 0;
}
