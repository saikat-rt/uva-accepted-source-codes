/*****  Etruscan Warriors Never Play Chess @ 11614  *****/

#include<stdio.h>
#include<math.h>

long long n, test, result;

int main(){
	scanf("%lld", &test);
	while(test--){
		scanf("%lld", &n);
		result=floor((sqrt(1+8*n)-1)/2);
		printf("%lld\n", result);
	}
	return 0;
}
