
/*  Image Coding @ 11588  */

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

char mat[23][23];
long r, c, m, n, tc;
long i, j, k, l, cse=0, result, count, max;

int sai(const void *a, const void *b){
   return *(int *)b - *(int *)a;
}

int main(){
	scanf("%ld", &tc);
	while(tc--){
		cse++;
		scanf("%ld %ld %ld %ld", &r, &c, &m, &n);

		for(i=0; i<r; i++)
			scanf(" %s", mat[i]);

		long letter[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

		for(i=0; i<r; i++)
			for(j=0; j<c; j++)
				letter[mat[i][j]-65]++;
			
		qsort(letter,26,sizeof(long),sai);

		max=letter[0];
		count=1;

		for(k=1; k<26; k++)
			if(letter[k]==max)
				count++;

		if(count==(r*c))
			result=max*m+((r*c)-max)*n;
		else
			result=count*max*m+(r*c-max*count)*n;

		printf("Case %ld: %ld\n", cse, result);
	}
	return 0;
}