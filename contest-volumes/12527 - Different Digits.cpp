/*
	12527 - Different Digits (accepted)
	[runtime 0.028 second]
*/

#include <stdio.h>

int main()
{
	int m, n, count, bits, number, digit;
	bool duplicate;
	
	while(scanf("%d %d", &m, &n) == 2)
	{
		count = 0;
		
		while(m++ <= n)
		{
			number = m;
			bits = 0;
			duplicate = false;
			while(number != 0 && !duplicate)
			{
				digit = number % 10;
				if(bits & (1 << digit))	duplicate = true;
				
				bits |= (1 << digit);
				number /= 10;
			}
				
			if(!duplicate)	count++;
		}	
			
		printf("%d\n", count);
	}
	
	return 0;
}

