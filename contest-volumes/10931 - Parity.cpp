/***** parity @ 10931  *****/

#include<stdio.h>
#include<string.h>
#include<ctype.h>

long num;
long s[1001];

int main()
{
	while(scanf("%ld",&num)==1)
	{
		if(num==0)	break;
		else
		{
			long temp=num;
			long i=0;
			while(temp!=0)
			{
				s[i]=temp%2;
				temp/=2;
				i++;
			}

			long par=0;
			for(long j=0;j<i;j++)
			{
				if(s[j]==1)
					par+=1;
			}

			char bin[32];
			long l=0;
			long a=num;
			long p;
			while(a!=0)
			{
				p=a%2;
				bin[l]=(p)+'0';
				a=a/2;
				l++;
				if(a==0)
				{
					bin[l]='\0';
					break;
				}
			}
			
			long len;
			long m,n;
			char fbin[32];
			len=strlen(bin);
			fbin[len]='\0';
			for(m=len-1,n=0;m>=0,n<len;m--,n++)
				fbin[n]=bin[m];

			printf("The parity of %s is %ld (mod 2).\n",fbin,par);
		}
	}
	return 0;
}


