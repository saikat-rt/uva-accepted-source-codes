/*****  medians @ 10347  *****/

#include<stdio.h>
#include<math.h>

double m1, m2, m3, area, s, temp;

int main(){
	while(scanf("%lf %lf %lf",&m1,&m2,&m3)==3){
		s=(m1+m2+m3)/2.0;
		temp=s*(s-m1)*(s-m2)*(s-m3);
		area=(4.0/3.0)*sqrt(temp);
		if(area>0)
			printf("%0.3lf\n",area);
		else
			printf("-1.000\n");
	}
	return 0;
}

