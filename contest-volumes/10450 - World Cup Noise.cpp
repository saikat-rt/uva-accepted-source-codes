/*****  world cup noise @ 10450  *****/

#include<stdio.h>

long long tc;
long long f, s, temp;
long long fib[100];

int main(){
	scanf("%lld", &tc);
	s=0;
	while(tc--){
		s++;
		scanf("%lld", &f);
		fib[0]=1;
		fib[1]=2;
		if(f==0)	printf("Scenario #%lld:\n%lld\n\n", s, fib[0]);
		else if(f==1)	printf("Scenario #%lld:\n%lld\n\n", s, fib[1]);
		else{
			for(long long i=2; i<=f; i++){
				fib[i]=fib[i-1]+fib[i-2];
				temp=fib[i];
			}
			printf("Scenario #%lld:\n%lld\n\n", s, temp);
		}
	}
	return 0;
}