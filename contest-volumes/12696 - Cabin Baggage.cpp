/*
*   Cabin Baggage @ 12696
*   [accepted]
*/

#include <stdio.h>

int main() {
    double l, w, d, m;
    int t, count = 0;
    scanf("%d", &t);
    
    while (t--) {
        scanf("%lf %lf %lf %lf", &l, &w, &d, &m);
        double sum = l + w + d;
   
        if (m > 7.0)    printf("0\n");
        else if((l > 56.0 || w > 45.0 || d > 25.0) && sum > 125.0)   printf("0\n");
        else if((l > 56.0 || w > 45.0 || d > 25.0) && sum <= 125.0) {
           printf("1\n");
           ++count;
        }
        else {
            printf("1\n");
            ++count;
        }
    }
    
    printf("%d\n", count);
    
    return 0;
}
    

