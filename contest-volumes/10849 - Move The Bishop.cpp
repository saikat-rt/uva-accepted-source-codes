/*****  move the bishop @ 10849  *****/

#include<stdio.h>
#include<stdlib.h>

long x1,y1,x2,y2;
long t,line,n;

int main(){
	scanf("%ld",&t);
	for(long i=0;i<t;i++){
		scanf("%ld %ld",&line,&n);
		for(long j=0;j<line;j++){
			scanf("%ld %ld %ld %ld",&x1,&y1,&x2,&y2);
			if(x1>n || x2>n || y1>n || y2>n)
				printf("no move\n");
			else if(x1==x2 && y1==y2)
				printf("0\n");
			else if(x1+y1==x2+y2)
				printf("1\n");
			else if(abs(x1-x2)==abs(y1-y2))
				printf("1\n");
			else if(((x1%2==0 && y1%2!=0)||(x1%2!=0 && y1%2==0)) && ((x2%2==0 && y2%2!=0)||(x2%2!=0 && y2%2==0)))
				printf("2\n");
			else if(((x1%2==0 && y1%2==0)||(x1%2!=0 && y1%2!=0)) && ((x2%2==0 && y2%2==0)||(x2%2!=0 && y2%2!=0)))
				printf("2\n");
			else
				printf("no move\n");
		}
	}
	return 0;
}