/*****  age sort @ 11462  *****/

#include<stdio.h>

long n;
long AgeCounter[101];
long age[2000010];

int main(){
	while(scanf("%ld", &n)==1 && n!=0){
		for(long p=0; p<101; p++)
			AgeCounter[p]=0;

		for(long i=0; i<n; i++){
			scanf("%ld", &age[i]);
			AgeCounter[age[i]]++;
		}
		
		long space=1;
		for(long s=0; s<101; s++){
			if(AgeCounter[s]!=0){
				for(long k=0; k<AgeCounter[s]; k++){
					if(space<n){
						printf("%ld ", s);
						space++;
					}
					else if(space==n)
						printf("%ld\n",s);
				}
			}
		}
	}
	return 0;
}
