/**  Closest Sums @ 10487  **/

#include <stdio.h>
#include <stdlib.h>

long n, p, m, input[1010], diff, i, k, sum, q, kase = 0, max, output;

int QuickSort(const void *a, const void *b){
	return *(int *)a - *(int *)b;
}
int main(){
	//freopen("10487.txt", "r", stdin);
	while(scanf("%ld", &n) == 1 && n != 0){
		kase++;
		for(i = 0; i < n; i++)	scanf("%ld", &input[i]);
		
		qsort(input, n, sizeof(long), QuickSort);
		
		scanf("%ld", &m);
		printf("Case %ld:\n", kase);

		for(p = 0; p < m; p++){
			scanf("%ld", &q);
			max = 999999999;
			long flag = 0;
						
			for(i = 0; i < n - 1; i++){
				for(k = i + 1; k < n; k++){
					sum = input[i] + input [k];
					diff = abs(sum - q);
					if(diff < max)	max = diff, output = sum;
				}
			}
			printf("Closest sum to %ld is %ld.\n", q, output);
		}
	}
}
