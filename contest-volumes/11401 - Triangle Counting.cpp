/*****  triangle counting @ 11401  *****/

#include<stdio.h>

long long n, r, b, i;
long long s[1000001];

int main(){
	s[3] = 0;
	s[4] = 1;
	for(i=5; i<=1000000; i++){
		b = i/2+1;
		r = i*(i-1)-(i+1)*(i-b)-b*(b-1);
		s[i] = s[i-1] + r;
	}
	
	while(scanf("%lld", &n)==1 && n>=3){
		printf("%lld\n", s[n]);
	}
	return 0;
}