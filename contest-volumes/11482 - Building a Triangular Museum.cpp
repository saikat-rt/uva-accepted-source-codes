/*
*   Building a Triangular Museum @ 11482
*   [accepted]
*/

#include <cstdio>

int main() {
    int m, n, kase = 0;
    while (scanf("%d %d", &m, &n) == 2 && m && n) {
        printf("Triangular Museum %d\n", ++kase);

        int level = n * m;

        for (int i = 1; i <= level; ++i) {
            for (int k = 1; k <= level - i; ++k)    printf(" ");

            int b = (i / m) + 1;
            if (i % m == 0) --b;
            for (int r = 1; r <= b; ++r) {
                printf("/");

                if (i % m != 0)
                    for (int p = 1; p <= 2 * (i % m - 1); ++p)  printf(" ");
                else
                    for (int p = 1; p <= 2 * (m - 1); ++p)  printf("_");

                printf("\\");

                if (r < b && i % m != 0)
                    for (int p = 1; p <= 2 * (m - i % m); ++p)  printf(" ");
            }
            printf("\n");
        }
        printf("\n");
    }

    return 0;
}
