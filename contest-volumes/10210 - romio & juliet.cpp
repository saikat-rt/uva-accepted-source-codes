/*****  romio and juliet @ 10210  *****/

#include<stdio.h>
#include<math.h>
#define PI 2*acos(0)

double x1, x2, p1, p2, m, n, ab, temp1, temp2, dis;

int main(){
	while(scanf("%lf %lf %lf %lf %lf %lf",&x1,&p1,&x2,&p2,&m,&n)==6){
		ab=sqrt((x1-x2)*(x1-x2)+(p1-p2)*(p1-p2));
		temp1=ab/tan(m*PI/180);
		temp2=ab/tan(n*PI/180);
		dis=temp1+temp2;
		printf("%0.3lf\n",dis);
	}
	return 0;
}

