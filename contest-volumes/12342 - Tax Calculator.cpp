/*
*   Tax Calculator @ 12342
*   [accepted]
*/

#include <cstdio>
#include <cmath>

int main() {
    int tc, kase = 0;
    scanf("%d", &tc);

    while (tc--) {
        long k;
        scanf("%ld", &k);

        if (k <= 180000)    printf("Case %d: 0\n", ++kase);
        else {
            k -= 180000;

            double tax = 0.0;

            if (k >= 300000) {
                tax += 30000;
                k -= 300000;
            }
            else {
                tax += k * 0.1;
                k = 0;
            }

            if (k >= 400000) {
                tax += 60000;
                k -= 400000;
            }
            else {
                tax += k * 0.15;
                k = 0;
            }

            if (k >= 300000) {
                tax += 60000;
                k -= 300000;
            }
            else {
                tax += k * 0.2;
                k = 0;
            }

            tax += k * 0.25;

            if (tax < 2000)  tax = 2000;

            printf("Case %d: %.0lf\n", ++kase, ceil(tax));
        }
    }

    return 0;
}
