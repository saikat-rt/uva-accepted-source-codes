/**  One-Two-Three @ 12289  **/

#include <stdio.h>
#include <string.h>

char s[6];
int tc, i, k, len;

int main(){
	scanf("%d", &tc);
	while(tc--){
		scanf(" %s", s);
		len = strlen(s);
		
		if(len == 5)	printf("3\n");
		else{
			if((s[0]=='o' && s[1]=='n')||(s[1]=='n' && s[2]=='e')||(s[0]=='o' && s[2]=='e')){
				printf("1\n");
			}
			else{
				printf("2\n");
			}
		}
	}
	return 0;
}
