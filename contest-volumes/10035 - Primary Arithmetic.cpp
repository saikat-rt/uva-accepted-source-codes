/** Primary Arithmetic @ 10035 **/

#include <stdio.h>

int main(){
	long a, b;
	int count, carry, i, sum;
	
	while(scanf("%ld %ld", &a, &b) == 2){
		if(a == 0 && b == 0)	break;
		else{
			count = 0;
			carry = 0;
			while(1){
				sum = (a % 10) + (b % 10) + carry;
				
				if(sum >= 10){
					carry = 1;
					++count;
				}
				else{
					carry = 0;
				}
				
				a = a / 10;
				b = b / 10;
				
				if(a == 0 && b == 0)	break;
			}
			
			if(count == 0)	printf("No carry operation.\n");
			else if(count == 1)	printf("1 carry operation.\n");
			else	printf("%d carry operations.\n", count);
		}
	}
	return 0;
}
