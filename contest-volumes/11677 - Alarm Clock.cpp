/*****  Alarm Clock @ 11677  *****/

#include <stdio.h>


int main(){
	int h1, m1, h2, m2, m;

	while(scanf("%d %d %d %d", &h1, &m1, &h2, &m2) == 4){
		if(h1 == 0 && m1 == 0 && h2 == 0 && m2 == 0)	break;
		else{
			if(h1 == 0)	h1 = 24;
			if(h2 == 0)	h2 = 24;

			m = (h2 - h1) * 60 + (m2 - m1);
			
			if(m < 0)	m = 1440 + m;
			else if(m == 0)	m = 24 * 60;

			printf("%d\n", m);
		}
	}
	return 0;
}
