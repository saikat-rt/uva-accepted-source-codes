/*
*	Nice Licence Plates - 12611
*	[accepted]
*/

#include <stdio.h>

int main () {

	int tc, num, sum;
	char a, b, c, x;

	scanf("%d", &tc);

	while (tc--) {
		scanf(" %c%c%c%c%d", &a, &b, &c, &x, &num);

		sum = (a - 65) * 26 * 26 + (b - 65) * 26 + c - 65;
		sum = sum - num;
		if (sum < 0)	sum *= -1;
		if (sum <= 100)	printf("nice\n");
		else	printf("not nice\n");
	}

	return 0;
}

