/*****  11824 - A Minimum Land Price  *****/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define budget 5000000

long long tc, baht[41], i, j, k;

int de(const void *a, const void *b){
	return *(int *)b - *(int *)a;
}

int main(){
    scanf("%lld", &tc);
    while(tc--){
        k = -1;
        while(scanf("%lld", &baht[++k]) == 1 && baht[k] != 0);

        qsort(baht, k + 1, sizeof(long long), de);

        long long total = 0, flag = 0;

        for(i = 0; i <= k; i++){
            total += 2 * pow(baht[i], i + 1);
            if(total > budget){
                flag = 1;
                break;
            }
        }
        if(flag == 1)   printf("Too expensive\n");
        else    printf("%lld\n", total);
    }
    return 0;
}
