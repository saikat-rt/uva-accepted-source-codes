/*****  smith numbers @ 10042  *****/

#include<stdio.h>
#include<math.h>

long long num,temp,test,sum,plus;
long long add[10000];

long prime_test(long n){
	if (n == 1) return 0;         
  	if (n == 2) return 1;         
  	if (n%2 == 0) return 0;       
  	for (int i=3; i*i<=n; i+=2)   
    	if (n%i == 0)               
      return 0;
  	return 1;
}
		
long sum_factor(long x){
	long i,c,s;
	c=x;
	s=0;
	while(c%2==0){
		add[s++]=2;
		c=c/2;
	}
	i=3;
	while(i<=(sqrt(c)+1)){
		if(c%i==0){
			add[s++]=i;
			c=c/i;
		}
		else	i=i+2;
	}
	if(c>1)	add[s]=c;
		
	plus=0;
	for(long j=0;j<=s;j++){
		//temp=add[j];
		while(add[j]!=0){
			plus+=add[j]%10;
			add[j]=add[j]/10;
		}
	}
	return plus;
}
			
		
int main(){
	scanf("%lld",&test);
	for(long i=0;i<test;i++){
		scanf("%lld",&num);
		for(long long k=num+1; ;k++){
			if(prime_test(k)==0){
				temp=k;
				sum=0;
				while(temp!=0){
					sum+=temp%10;
					temp=temp/10;
				}
				if(sum==sum_factor(k)){
					printf("%lld\n",k);
					break;
				}
			}
		}
	}
	return 0;
}
