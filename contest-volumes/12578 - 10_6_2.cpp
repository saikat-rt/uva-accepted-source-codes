/*
*	10:6:2 @ 12578
*	[runtime - 0.008 sec]
*/

#include <stdio.h>
#include <math.h>
#define pi acos(-1.0)

int main()
{
	int tc;
	double length, width, radius, red_area, green_area;

	scanf("%d", &tc);

	while(tc--)
	{
		scanf("%lf", &length);

		width = 0.6 * length;
		radius = 0.2 * length;

		red_area = pi * radius * radius;
		green_area = width * length - red_area;

		printf("%.2lf %.2lf\n", red_area, green_area);
	}

	return 0;
}

