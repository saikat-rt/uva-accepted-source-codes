/*
*   Mobile SMS @ 12896
*   [accepted]
*/

#include <stdio.h>

int main() {

    char keypad[][5] = {" ", ".,?\"", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    
    int tc;
    scanf("%d", &tc);
    
    while (tc--) {
        int len;
        scanf("%d", &len);
        
        int key[101];
        for (int i = 0; i < len; ++i)   scanf("%d", &key[i]);
        
        int pos;
        for (int i = 0; i < len; ++i) {
            scanf("%d", &pos);
            printf("%c", keypad[key[i]][--pos]);
        }
        
        printf("\n");
    }
    return 0;
}
