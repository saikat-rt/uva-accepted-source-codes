/*****  11608 - No Problem  *****/

#include<stdio.h>

long s;
long pre[13], next[13];
long total, cse=0;

int main(){
	while(scanf("%ld", &s)==1 && s>-1){
		pre[0]=s;
		cse++;
		for(long i=1; i<=12; i++)	scanf("%ld", &pre[i]);
		for(long j=0; j<12; j++)	scanf("%ld", &next[j]);

		total=0;
		printf("Case %ld:\n", cse);
		for(long k=0; k<12; k++){
			if(pre[k]+total>=next[k]){
				printf("No problem! :D\n");
				total+=pre[k]-next[k];
			}
			else if(pre[k]+total<next[k]){
				printf("No problem. :(\n");
				total+=pre[k];
			}
		}
	}
	return 0;
}