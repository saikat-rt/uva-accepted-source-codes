/*
*    Draw Grid @ 11074
*    [accepted]
*/

#include <cstdio>
#include <cstring>

int main() {
    int s, t, n, kase = 0;
    char star_line[823];
    char grid_line[823];

    while (scanf("%d %d %d", &s, &t, &n) == 3 && s && t && n) {
        int tot_star = t * (n + 1) + s * n;
        memset(star_line, '*', tot_star);

        int w = 0;
        int c = -1;
        while (w < n + 1) {
            for (int j = 0; j < t; ++j) grid_line[++c] = '*';
            ++w;
            for (int k = 0; k < s && w < n + 1; ++k) grid_line[++c] = '.';
        }
        printf("Case %d:\n", ++kase);

        int g = 0;
        while (g < n + 1) {
            for (int i = 0; i < t; ++i)	printf("%s\n", star_line);
            if (++g == n + 1)  break;
            for (int i = 0; i < s; ++i)	printf("%s\n", grid_line);
        }
        printf("\n");

        memset(star_line, 0, sizeof(star_line));
        memset(grid_line, 0, sizeof(star_line));
    }

    return 0;
}