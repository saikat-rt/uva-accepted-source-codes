/*****  largest prime divisor @ 11466  *****/

#include<stdio.h>
#include<math.h>

long long d;
long long prime[100000];

long long factor(long long n){
	long long i, c, t;
	if(n<0)	c=-1*n;
	else	c=n;
	t=0;
	while(c%2==0){
		prime[t++]=2;
		c=c/2;
	}

	i=3;
	while(i<=sqrt(c)+1){
		if(c%i==0){
			prime[t++]=i;
			c=c/i;
		}
		else	i=i+2;
	}

	if(c>1)	prime[t]=c;
	else	t=t-1;
	if(t>0){
		long long p, max;
		max=0;
		for(p=0; p<=t; p++)
				if(prime[p]>max)
					max=prime[p];
		if(max>prime[0])
			printf("%lld\n", max);
		else	printf("-1\n");
	}
	else	printf("-1\n");
	return 0;
}


int main(){
	while(scanf("%lld", &d)==1 && d!=0){
		factor(d);
	}
	return 0;
}


