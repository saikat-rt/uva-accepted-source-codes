/*
*	Start Grid @ 12488
*	[accepted | runtime : 0.012]
*/

#include <cstdio>
#include <algorithm>
using namespace std;

int main() {

	int n, a[25], b[25];

	while(scanf("%d", &n) == 1) {

		for(int i = 0; i < n; i++)	scanf("%d", &a[i]);
		for(int j = 0; j < n; j++)	scanf("%d", &b[j]);

		int ans = 0;

		for(int i = 0; i < n; i++) {

			int pos = 0;

			for(int j = 0; j < n; j++) {
				if(a[j] == b[i]) {
					pos = j;
					break;
				}
			}

			for(int j = pos; j > i; j--)	swap(a[j], a[j-1]);

			ans += max(0, pos-i);
		}

		printf("%d\n", ans);
	}

	return 0;
}

