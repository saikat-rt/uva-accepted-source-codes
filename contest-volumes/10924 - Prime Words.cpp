/*****  prime words @ 10924  *****/

#include<stdio.h>
#include<math.h>
#include<string.h>

char s[23];
long sum, len, p, res;

long isPrime(long sum) { 
	if(sum==1) return 1; 
    if(sum==2) return 1; 
    if(sum%2==0) return 0; 
    for(int i=3;i*i<=sum;i+=2)  
        if(sum%i==0) return 0; 
   
    return 1;
}


int main(){
	while(gets(s)){
		len=strlen(s);
		sum=0;
		for(p=0;p<len;p++){
			if(s[p]>='a'&&s[p]<='z')
				sum+=s[p]-'a'+1;

			else if(s[p]>='A'&&s[p]<='Z')
				sum+=s[p]-'A'+27;
		}
		res=isPrime(sum);

		if(res==1)
			printf("It is a prime word.\n");
		else if(res==0)
			printf("It is not a prime word.\n");
	}
	return 0;
}