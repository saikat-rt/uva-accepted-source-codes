/*****  Optical Reader @ 11839  ****/

#include <stdio.h>

int tc, i, n[6], count, min;

int main(){
    while(scanf("%d", &tc) == 1 && tc != 0){
        while(tc--){
            count = 0;
            min = -1;
            for(i = 0; i < 5; i++){
                scanf("%d", &n[i]);
                if(n[i] <= 127){
                    count++;
                    if(n[i] > min)
                        min = n[i];
                }
            }
            if(count == 0 || count > 1)
                printf("*\n");
            else
                for(i = 0; i < 5; i++)
                    if(n[i] == min)
                        printf("%c\n", i + 65);
        }
    }
    return 0;
}
