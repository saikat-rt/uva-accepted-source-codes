/*****  
		Hello World @ 11636
*****/


#include <stdio.h>

int main(){
	//freopen("11636.txt","r",stdin);
	long num, kase=0, count, n;
	while(scanf("%ld", &num)==1 && num>0){
		count=n=1;
		kase++;
		if(num==1)	printf("Case %ld: 0\n", kase);
		else{
			while(1){
				n*=2;
				if(n>=num)
					break;
				else
					count++;
			}
			printf("Case %ld: %ld\n", kase, count);
		}
	}
	return 0;
}