/*****  power et al @ 10515  *****/

#include<stdio.h>
#include<string.h>
#include<math.h>

char m[110], n[110];
long lm, ln, temp, i, j;

int main(){
	while(scanf("%s %s", m, n)==2){
		if(m[0]=='0' && n[0]=='0')	break;
		else if(m[0]=='0')	printf("0\n");
		else if(n[0]=='0')	printf("1\n");
		else{
			lm=strlen(m);
			ln=strlen(n);
			i=m[lm-1]-'0';
			if(ln==1 && n[ln-1]!='0'){	
				j=n[ln-1]-'0';
				if(j%4==0)	j=4;
				else	j=j%4;
			}
			else if(ln>=2){
				if(n[ln-1]=='0' && n[ln-2]=='0')	j=4;
				else{
					j=(n[ln-1]-'0')+(n[ln-2]-'0')*10;
					if(j%4==0)	j=4;
					else	j=j%4;
				}
			}
			temp=(long)pow(i, j);
			printf("%ld\n", temp%10);
		}
	}
	return 0;
}
